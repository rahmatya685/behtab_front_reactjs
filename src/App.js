import React, { Component } from 'react'
import { render } from 'react-dom'
import { BrowserRouter as Router, Route, Link } from 'react-router-dom'
import '../node_modules/bootstrap/dist/css/bootstrap.min.css';
import globalTranslations from "./translations/global.json";
import { renderToStaticMarkup } from "react-dom/server";
import { LocalizeProvider } from "react-localize-redux";

import Login from './pages/login'
import whoops404 from './components/whoops404'
import MainPage from '../pages/MainPage'
import Cookies from 'universal-cookie';

import { SnackbarProvider } from 'notistack';

class App extends Component {


    constructor(props) {
        super(props)

        const cookies = new Cookies();
        const lang = cookies.get('languageCode') || "en";


        this.props.initialize({
            languages: [
                { name: "English", code: "en" },
                { name: "Farsi", code: "fa" }
            ],
            translation: globalTranslations,
            options: {
                renderToStaticMarkup,
                renderInnerHtml: true,
                defaultLanguage: lang
            }
        });
    }


    render() {
        // if (token === undefined) {
        //     currentRoute = <Route path="/login" component={Login}></Route>
        // } else {
        //     currentRoute = <Route path="/" component={dashboard}></Route>
        // }

        return (
            <SnackbarProvider maxSnack={3}>
                <LocalizeProvider>
                    <Router >
                        <div>
                            <Route path="/login" component={Login}></Route>
                            <Route path="/map" component={MapView}></Route>
                            <Route exact path="/" component={dashboard}></Route>
                        </div>
                    </Router>
                </LocalizeProvider>
            </SnackbarProvider> 
        )
    }
}
export default App;