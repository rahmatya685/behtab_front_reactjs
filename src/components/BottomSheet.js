import React, {   Component, Fragment } from 'react'

import SwipeableBottomSheet from 'react-swipeable-bottom-sheet';
import CloseIcon from '@material-ui/icons/Close'
import FullscreenIcon from '@material-ui/icons/Fullscreen'
import FullscreenExit from '@material-ui/icons/FullscreenExit'
import KeyboardArrowUpIcon from '@material-ui/icons/KeyboardArrowUp'
import KeyboardArrowDownIcon from '@material-ui/icons/KeyboardArrowDown'

import { withStyles } from '@material-ui/core/styles';
import { withLocalize, Translate } from "react-localize-redux";
import IconButton from '@material-ui/core/IconButton';
import PropTypes from 'prop-types';

const bottonSheetOverflowHeight = 40;

const styles = theme => ({

    bottomSheetTitle: {
        backgroundColor: '#3f51b5',
        boxSizing: 'border-box',
        color: 'white',
        minHeight: `${bottonSheetOverflowHeight}px`,
        fontSize: '16px',
        textAlign: 'center',
        padding: '5px',

    },
    bottomSheetBotton: {
        color: 'white',
        float: 'right',
        padding: '5px',
    }
});

class BottomSheet extends Component {


    constructor(props) {
        super(props)

        this.onBottomSheetChanged = this.onBottomSheetChanged.bind(this);
    }

    state = {
        toggleBottomView: false,
        fullScreenBtoomView: false,
    };

    onBottomSheetToggled(toggle) {
        this.setState({
            toggleBottomView: toggle
        })
    }

    onBottomSheetFullScreenChanged(isFullScreen) {
        this.setState({
            fullScreenBtoomView: isFullScreen
        })
    }
    onBottomSheetChanged(opened) {
        this.props.onBottomSheetChanged(opened)
    }

    render() {

        const { fullScreenBtoomView, toggleBottomView } = this.state

        const { bottomViewTitle, classes } = this.props

        let toggleBtnIcon = <KeyboardArrowUpIcon />
        if (toggleBottomView) {
            toggleBtnIcon = <KeyboardArrowDownIcon />
        }

        let fullScreenIcon = <FullscreenIcon />

        if (fullScreenBtoomView) {
            fullScreenIcon = <FullscreenExit />;
        }
        return (
            <SwipeableBottomSheet
                style={{
                    zIndex: 1300,
                }}
                overflowHeight={bottonSheetOverflowHeight}
                open={toggleBottomView}
                defaultOpen={true}
                overlay={false}
                onChange={(opened) => this.onBottomSheetToggled(opened)}
                fullScreen={this.state.fullScreenBtoomView}
            >
                <div className={classes.bottomSheetTitle}  >
                    <label  ><Translate id={bottomViewTitle} /></label>
                    <IconButton onClick={() => this.onBottomSheetChanged(false)} className={classes.bottomSheetBotton} aria-label="close"   >
                        <CloseIcon />
                    </IconButton>
                    <IconButton className={classes.bottomSheetBotton} onClick={() => this.onBottomSheetFullScreenChanged(!this.state.fullScreenBtoomView)} aria-label="fullscreen">
                        {fullScreenIcon}
                    </IconButton>
                    <IconButton onClick={() => this.onBottomSheetToggled(!this.state.toggleBottomView)} className={classes.bottomSheetBotton} aria-label="togglee"   >
                        {toggleBtnIcon}
                    </IconButton>
                </div>
                 {this.props.children} 
            </SwipeableBottomSheet>
        )
    }
}
BottomSheet.propTypes = {
    classes: PropTypes.object.isRequired,
    theme: PropTypes.object.isRequired,
};
 
export default withStyles(styles, { withTheme: true })(withLocalize(BottomSheet));
