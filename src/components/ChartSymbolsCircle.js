import React, { Component } from 'react';

const ChartSymbolsCircle = (props) => {
    const {
        cx, cy, stroke, payload, value,fill
    } = props;

    return (
        <svg x={cx - 5} y={cy - 5} height="10" width="10">
            <circle cx="5" cy="5" r="5" fill={fill} />
        </svg>
    );
};
export default ChartSymbolsCircle;

