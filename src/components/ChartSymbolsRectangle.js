

import React, { Component } from 'react';

const ChartSymbolsRectangle = (props) => {
    const {
        cx, cy, stroke, payload, value,
    } = props;

    return (
        <svg x={cx - 5} y={cy - 5} height="10" width="10">
            <polygon points="0 0, 10 0,10 10, 0 10" class="triangle" fill="#3f51b5" />
        </svg>
    );
};

export default ChartSymbolsRectangle;
