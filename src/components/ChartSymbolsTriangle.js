
import React, { Component } from 'react';

const ChartSymbolsTriangle = (props) => {
    const {
        cx, cy, stroke, payload, value,
    } = props;

    return (
        <svg x={cx - 7.5} y={cy - 7.5} height="15" width="15">
            <polygon points="7.5 0, 15 15, 0 15" class="triangle" fill="#ffc107" />
        </svg>
    );
};
export default ChartSymbolsTriangle;