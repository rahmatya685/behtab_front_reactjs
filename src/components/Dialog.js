import React, { Component } from 'react';
import { withStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import MuiDialogTitle from '@material-ui/core/DialogTitle';
import MuiDialogContent from '@material-ui/core/DialogContent';
import MuiDialogActions from '@material-ui/core/DialogActions';
import IconButton from '@material-ui/core/IconButton';
import CloseIcon from '@material-ui/icons/Close';
import Typography from '@material-ui/core/Typography';
import PropTypes from 'prop-types';
import { Translate, withLocalize } from 'react-localize-redux';
import Slide from '@material-ui/core/Slide';

const Transition = React.forwardRef(function Transition(props, ref) {
    return <Slide direction="up" ref={ref} {...props} />;
});

const DialogTitle = withStyles(theme => ({
    root: {
        borderBottom: `1px solid ${theme.palette.divider}`,
        margin: 0,
        padding: theme.spacing.unit * 2,
    },
    closeButton: {
        position: 'absolute',
        right: theme.spacing.unit,
        top: theme.spacing.unit,
        color: theme.palette.grey[500],
    },
}))(props => {
    const { children, classes, onClose } = props;
    return (
        <MuiDialogTitle disableTypography className={classes.root}>
            <Typography variant="h6">{children}</Typography>
            {onClose ? (
                <IconButton aria-label="Close" className={classes.closeButton} onClick={onClose}>
                    <CloseIcon />
                </IconButton>
            ) : null}
        </MuiDialogTitle>
    );
});

const DialogContent = withStyles(theme => ({
    root: {
        margin: 0,
        padding: theme.spacing.unit * 2,
    },
}))(MuiDialogContent);

const DialogActions = withStyles(theme => ({
    root: {
        borderTop: `1px solid ${theme.palette.divider}`,
        margin: 0,
        padding: theme.spacing.unit,
    },
}))(MuiDialogActions);

class CustomizedDialog extends Component {

    constructor(props) {
        super(props)

        this.handleClose = this.handleClose.bind(this);
        this.handleOkClicked = this.handleOkClicked.bind(this);
    }

    handleClose = () => {
        this.props.onDialogClosed()
    };
    handleOkClicked = () => {
        this.props.handleOkClicked()
    }

    render() {
        return (
            <div>

                <Dialog
                    onClose={this.handleClose}
                    aria-labelledby="customized-dialog-title"
                    open={true}
                    TransitionComponent={Transition}
                    fullScreen={this.props.fullScreen === undefined ? false : this.props.fullScreen}
                >
                    <DialogTitle id="customized-dialog-title" onClose={this.handleClose}>
                        {
                            this.props.titleId=== undefined ? this.props.customTitle : <Translate id={this.props.titleId}></Translate>
                        }
                         
                    </DialogTitle>
                    <DialogContent>
                        {this.props.children}
                    </DialogContent>
                    {
                        this.props.hideActions === undefined ?
                            <DialogActions>
                                <Button ref={this.props.btnOkRef} onClick={this.handleOkClicked} disabled={this.props.saveBtnEnable} color="primary"><Translate id={this.props.okBtnTitleId} /> </Button>
                            </DialogActions>
                            : null
                    }

                </Dialog>
            </div>
        );
    }
}

// CustomizedDialog.prototype = {
//     okBtnTitleId: PropTypes.string.isRequired,
//     titleId: PropTypes.string.isRequired,
//     onDialogClosed: PropTypes.func.isRequired,
//     handleOkClicked: PropTypes.func.isRequired


// }

export default withLocalize(CustomizedDialog);
