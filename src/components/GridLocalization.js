import { Translate, withLocalize } from 'react-localize-redux';
import React, { Component } from 'react';


const gridLocalization = {
    pagination: {
        labelDisplayedRows: '{from}-{to}  {count}', // {from}-{to} of {count}
        labelRowsPerPage: <Translate id="RowsPerPage" />, // Rows per page:
        firstAriaLabel: <Translate id="FirstPage" />, // First Page
        firstTooltip: <Translate id="FirstPage" />, // First Page
        previousAriaLabel: <Translate id="PreviousPage" />, // Previous Page
        previousTooltip: <Translate id="PreviousPage" />, // Previous Page
        nextAriaLabel: <Translate id="NextPage" />, // Next Page
        nextTooltip: <Translate id="NextPage" />, // Next Page
        lastAriaLabel: <Translate id="LastPage" />, // Last Page
        lastTooltip: <Translate id="LastPage" />, // Last Page
        labelRowsSelect: ""
    },
    toolbar: {
        nRowsSelected: '{0}', // {0} row(s) selected
        showColumnsTitle: 'نمایش', // Show Columns
        showColumnsAriaLabel: <Translate id="showColumnsAriaLabel" />, // Show Columns
        exportTitle: <Translate id="exportTitle" />, // Export
        exportAriaLabel: <Translate id="exportTitle" />, // Export
        exportName: 'CSV export', // Export as CSV
        searchTooltip: <Translate id="exportTitle" />, // Search
        addRemoveColumns:<Translate id="addRemoveColumns" />, // Search
    },
    header: {
        actions: <Translate id="GridActions" />, // Actions
    },
    body: {
        addTooltip:<Translate id="Add" />,
        emptyDataSourceMessage: <Translate id="EmptyDataSourceMessage" />, // No records to display
        editRow: {
            deleteText: <Translate id="DeleteAlertMsg" />
        },
        filterRow: {
            filterTooltip: 'Filter', // Filter
        },
        deleteTooltip: <Translate id="DeleteAction" />,
        editRow: {
            cancelTooltip: <Translate id="Cancel" />,
            saveTooltip: <Translate id="Save" />,
        },
        editTooltip:<Translate id="EditAction" />,
    }

}

export default withLocalize(gridLocalization)