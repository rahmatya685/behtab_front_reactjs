import React, { Component } from 'react';
import MaterialTable, { MTableToolbar } from 'material-table';
import { Translate, withLocalize } from 'react-localize-redux';
import globalTranslations from "../translations/global.json"; 
import { withStyles } from '@material-ui/core/styles';
import PropTypes from 'prop-types';
import Cookies from 'universal-cookie';
import Card from '@material-ui/core/Card';
import Typography from '@material-ui/core/Typography';
import CardContent from '@material-ui/core/CardContent';
import { withSnackbar } from 'notistack';
import gridLocalization from "../components/GridLocalization"
import ButtonGroup from '@material-ui/core/ButtonGroup';
import Button from '@material-ui/core/Button';


const styles = theme => ({
    extendedIcon: {
        marginRight: theme.spacing.unit,
    },
    paper: {
    },
});

class HospitalRiskInfoGrid extends Component {
    constructor(props) {
        super(props)

        this.props.addTranslation(globalTranslations);

        this.state = {
            height: 0, width: 0, buildings: Array.of(), isLoading: false,
            healthcareLocationType: "Hospital",
            RiskInfoHospital: null,
            RiskInfoHealthHome: null
        };

        this.updateWindowDimensions = this.updateWindowDimensions.bind(this);
        this.getToken = this.getToken.bind(this);
        this.onDialogClosed = this.onDialogClosed.bind(this);
        this.handleBtnHealthcareLocationType = this.handleBtnHealthcareLocationType.bind(this);
        this.handleResultRiskInfoHospital = this.handleResultRiskInfoHospital.bind(this);
        this.doRequest = this.doRequest.bind(this);
        this.query = undefined;
        this.tableRef = undefined;
    }
    handleBtnHealthcareLocationType(event) {
        this.setState({
            healthcareLocationType: event.currentTarget.value
        })
    }
    getToken() {
        const cookies = new Cookies();
        return cookies.get('token') || ''
    }

    handleResultRiskInfoHospital(resp) {

        const riskHospital = Array.of();
        const riskHealthHome = Array.of();

        resp.forEach(element => {
            if (element.type === 1) {
                riskHospital.push(element)
            }
            if (element.type === 2) {
                riskHealthHome.push(element)
            }
        });

        this.setState({
            RiskInfoHospital: riskHospital,
            RiskInfoHealthHome: riskHealthHome
        })
    }

    componentWillMount() {
        this.doRequest(`/protected/hospitals/analytical`, "GET", this.handleResultRiskInfoHospital);
    }

    doRequest(url, method, func) {
        try {
            fetch(`${process.env.BASE_URL}${url}`, {
                method: method,
                headers: {
                    "Content-Type": "application/json",
                    "Authorization": this.getToken()
                },
            }).then(data => {

                if (data.ok) {

                    data.json().then((resp) => {

                        if (resp === "User not found") {
                            this.props.enqueueSnackbar(<Translate id="UserNotfound" />, {
                                variant: 'error',
                            });
                        } else {
                            func(resp)
                        }
                    });
                } else {
                    if (data.status === 500) {
                        this.props.enqueueSnackbar(<Translate id="ServerError" />, {
                            variant: 'error',
                        });
                    } else if (data.status === 401) {
                        this.props.enqueueSnackbar(<Translate id="UserValidationError" />, {
                            variant: 'error',
                        });
                    } else {
                        this.props.enqueueSnackbar(data.toString(), {
                            variant: 'error',
                        });
                    }
                }

            })
                .catch(error => this.props.enqueueSnackbar(error.toString(), {
                    variant: 'error'
                }));
        } catch (e) {
            this.props.enqueueSnackbar(e.toString(), {
                variant: 'error'
            })
        }
    }


    componentDidMount() {
        this.updateWindowDimensions();
        window.addEventListener('resize', this.updateWindowDimensions);
    }

    componentWillUnmount() {
        window.removeEventListener('resize', this.updateWindowDimensions);
    }

    updateWindowDimensions() {
        this.setState({ width: window.innerWidth, height: window.innerHeight });
    }

    onDialogClosed() {
        this.props.onDialogClosed()
    }


    render() {

        const { classes, theme, activeLanguage } = this.props;

        const { width, healthcareLocationType, RiskInfoHospital, RiskInfoHealthHome } = this.state;

        const cookies = new Cookies();
        const lang = cookies.get('languageCode') || "en";

        let name = lang === "en" ? "name" : ""
        let LevelOfImportanceField = lang === "en" ? "Level Of Importance" : "سطح اهمیت"
        let HazardLevels = lang === "en" ? "Hazard Levels" : "سطح خطر"
        let riskIndexStructural = lang === "en" ? "Structural Risk Index" : "شاخص ریسک سازه ای"
        let riskIndexNonStructural = lang === "en" ? "Non-Structural Risk Index" : "شاخص ریسک غیرسازه ای"
        let riskIndexOrganizational = lang === "en" ? "Organizational Risk Index" : "شاخص ریسک سازمانی"
        let prioritizationIndex = lang === "en" ? "Overall Risk Index" : "شاخص ریسک کلی"
  
        const provinceName = lang === "en" ? "Province Name" : "نام استان"

        const provinceFieldName = lang === "en" ? 'provinceEname' : 'provinceName'
        const hazardLevels = lang === "en" ? 'hazardLevels_en' : 'hazardLevels_fa'
        const levelOfImportance = lang === "en" ? 'levelOfImportance_en' : 'levelOfImportance_fa'

        let tableData = healthcareLocationType === "Hospital" ? RiskInfoHospital : RiskInfoHealthHome

        return (


            <div  >
                <MaterialTable style={{ width: width - 175 }} className={classes.paper} ref="table"
                    columns={[
                        {
                            title: name, field: 'name', searchable: true, editable: false,
                            headerStyle: {
                                fontFamily: '"Vazir", sans-serif'
                            },
                            cellStyle: {
                                fontFamily: '"Vazir", sans-serif'
                            }
                        },
                        {
                            title: provinceName, field: provinceFieldName, searchable: true, editable: false,
                            headerStyle: {
                                fontFamily: '"Vazir", sans-serif'
                            },
                            cellStyle: {
                                fontFamily: '"Vazir", sans-serif'
                            }
                        },
                        {
                            title: HazardLevels, field: hazardLevels, searchable: true, editable: false,
                            headerStyle: {
                                fontFamily: '"Vazir", sans-serif'
                            },
                            cellStyle: {
                                fontFamily: '"Vazir", sans-serif'
                            }
                        },
                        {
                            title: LevelOfImportanceField, field: levelOfImportance, searchable: true, editable: false,
                            headerStyle: {
                                fontFamily: '"Vazir", sans-serif'
                            },
                            cellStyle: {
                                fontFamily: '"Vazir", sans-serif'
                            }
                        },
                        {
                            title: riskIndexStructural, field: 'riskIndexStructural', searchable: true, editable: false,
                            headerStyle: {
                                fontFamily: '"Vazir", sans-serif'
                            },
                            cellStyle: {
                                fontFamily: '"Vazir", sans-serif'
                            }
                        },
                        {
                            title: riskIndexNonStructural, field: 'riskIndexNonStructural', searchable: true, editable: false,
                            headerStyle: {
                                fontFamily: '"Vazir", sans-serif'
                            },
                            cellStyle: {
                                fontFamily: '"Vazir", sans-serif'
                            }
                        },
                        {
                            title: riskIndexOrganizational, field: 'riskIndexOrganizational', searchable: true, editable: false,
                            headerStyle: {
                                fontFamily: '"Vazir", sans-serif'
                            },
                            cellStyle: {
                                fontFamily: '"Vazir", sans-serif'
                            }
                        },
                        {
                            title: prioritizationIndex, field: 'prioritizationIndex', searchable: true, editable: false,
                            headerStyle: {
                                fontFamily: '"Vazir", sans-serif'
                            },
                            cellStyle: {
                                fontFamily: '"Vazir", sans-serif'
                            }
                        },

                    ]}
                    data={tableData == null ? [] : tableData}
                    title={<Translate id="HospitalRiskInfo"></Translate>}
                    options={{
                        paging: false,
                        // maxBodyHeight: this.state.height/3,
                        toolbar: true,
                        columnsButton: true,
                        exportButton: true,
                        exportAllData: true,
                        actionsColumnIndex: -1,
                        showTitle: true,
                        search: true,
                        maxBodyHeight: 400,
                        filtering: false,
                        doubleHorizontalScroll: true,
                        rowStyle: {
                            fontFamily: '"Vazir", sans-serif'
                        },
                        actionsCellStyle: {
                            fontFamily: '"Vazir", sans-serif'
                        },
                        filterCellStyle: {
                            fontFamily: '"Vazir", sans-serif'
                        },
                        headerStyle: {
                            fontFamily: '"Vazir", sans-serif'
                        }
                    }}
                    onChangeRowsPerPage={pageSize => console.log(pageSize)}
                    onChangePage={page => console.log(page)}
                    tableRef={table => this.tableRef = table}
                    localization={gridLocalization}
                    components={{
                        Toolbar: props => (
                            <div>
                                <MTableToolbar {...props} />
                                <div style={{ padding: '0px 10px' }}>
                                    <ButtonGroup color="primary" aria-label="outlined primary button group">
                                        <Button value="Hospital" variant={healthcareLocationType === "Hospital" ? "contained" : "outlined"} onClick={this.handleBtnHealthcareLocationType} ><Translate id="Hospital" /></Button>
                                        <Button value="HealthHome" variant={healthcareLocationType === "HealthHome" ? "contained" : "outlined"} onClick={this.handleBtnHealthcareLocationType}  ><Translate id="HealthHome" /></Button>
                                    </ButtonGroup>
                                </div>
                            </div>
                        ),
                    }}
                />
            </div>

        )
    }
}



HospitalRiskInfoGrid.propTypes = {
    classes: PropTypes.object.isRequired,
    theme: PropTypes.object.isRequired,
};
export default withStyles(styles, { withTheme: true })(withLocalize(withSnackbar(HospitalRiskInfoGrid)));

