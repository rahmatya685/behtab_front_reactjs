import React, { Component } from 'react';
import MaterialTable from 'material-table'
import { Translate, withLocalize } from 'react-localize-redux';
import globalTranslations from "../translations/global.json";
import { withStyles } from '@material-ui/core/styles';
import PropTypes from 'prop-types';
import Cookies from 'universal-cookie';
import { withSnackbar } from 'notistack';
import Fab from '@material-ui/core/Fab'


import BarChart from '@material-ui/icons/BarChart'
import AttachmentIcon from '@material-ui/icons/Attachment'
import ListAltIcon from '@material-ui/icons/ListAlt'
import CloudDownloadIcon from '@material-ui/icons/CloudDownload'
import AccountBalance from '@material-ui/icons/AccountBalance'
import Assignment from '@material-ui/icons/Assignment'


import classNames from 'classnames';
import gridLocalization from "../components/GridLocalization"
import download from 'downloadjs';
import HospitalAnalyticalReportDetailDialog from '../pages/HospitalAnalyticalReportDetailDialog'
import BuildingsGrid from '../pages/BuildingsGrid';
import AttachmentsDialog from '../pages/AttachmentsDialog';
import HospitalEdit from '../pages/HospitalEdit.js';
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';

import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';


const styles = theme => ({
  fabRefreshGrid: {
    position: 'relative',
    top: theme.spacing.unit * 5,
    left: theme.spacing.unit,
    zIndex: 11
  },
  extendedIcon: {
    marginRight: theme.spacing.unit * 4,
  },
});



const serviceTypes =
{
  0: 'ServiceType_General'
  ,
  1: 'ServiceType_Single_Specialty'
  ,
  2: 'ServiceType_Treasury'
};

const organizationalAffiliationTypes =
{
  0: 'organizationalAffiliation_Governmental'
  ,
  1: 'organizationalAffiliation_Private_Sector'
  ,
  2: 'organizationalAffiliation_Academic'
  ,
  3: 'organizationalAffiliation_Charity'
  ,
  4:'organizationalAffiliation_Military'
  ,
  5: 'organizationalAffiliation_Social_Security_Organization'
}
  ;

class HospitalsGrid extends Component {
  constructor(props) {
    super(props)
    this.props.addTranslation(globalTranslations);

    this.state = {
      height: 0,
      showAnalyticalReportDialog: false,
      hospital4Report: null,
      showBuildingsGrid: false,
      hospital4BuildingsGrid: null,
      showAttachments: false,
      hospital4AttachmentDialog: null,
      editingHospital: null,
      evaluatorsKeyValues: {},
      surveyorsKeyValues: {},
      evaluators: [],
      surveyors: [],
      showDownloadMenuItems: false,
      anchorDownloadMenu: null,
      Hospital4MenuAttachments: null
    };
    this.updateWindowDimensions = this.updateWindowDimensions.bind(this);
    this.getToken = this.getToken.bind(this);
    this.showAnalyticalReportDialog = this.showAnalyticalReportDialog.bind(this);
    this.onHospitalRerportDialogClosed = this.onHospitalRerportDialogClosed.bind(this);
    this.query = this.props.searchKey;
    this.tableRef = undefined;
    this.call4Report = this.call4Report.bind(this);
    this.call4AnalyticalReport = this.call4AnalyticalReport.bind(this);
    this.onBuildingsGridDialogClosed = this.onBuildingsGridDialogClosed.bind(this);
    this.showBuildingsGrid = this.showBuildingsGrid.bind(this);
    this.showAttachmentsDialog = this.showAttachmentsDialog.bind(this);
    this.closeAttachmentsDialog = this.closeAttachmentsDialog.bind(this);
    this.getUsers = this.getUsers.bind(this);
    this.onBtnHospitalEditClicked = this.onBtnHospitalEditClicked.bind(this);
    this.showDownloadMenu = this.showDownloadMenu.bind(this);
    this.handleDownloadMenuClose = this.handleDownloadMenuClose.bind(this);
    this.handleDownloadMenuItemClick = this.handleDownloadMenuItemClick.bind(this);
  }

  componentDidMount() {
    this.updateWindowDimensions();
    window.addEventListener('resize', this.updateWindowDimensions);
    this.getUsers('evaluators')
    this.getUsers('surveyors')
  }

  componentWillUnmount() {
    window.removeEventListener('resize', this.updateWindowDimensions);
  }

  updateWindowDimensions() {
    this.setState({ height: window.innerHeight });
  }

  getToken() {
    const cookies = new Cookies();
    return cookies.get('token') || ''
  }

  getUsers(type) {
    const cookies = new Cookies();
    let token = cookies.get('token') || ''

    try {
      fetch(`${process.env.BASE_URL}/protected/users/${type}`, {
        method: "POST", // *GET, POST, PUT, DELETE, etc.   
        headers: {
          "Content-Type": "application/json",
          "Authorization": token
        },
      })
        .then(data => {
          data.json().then((resp) => {

            if (resp === "User not found") {
              this.props.enqueueSnackbar(<Translate id="UserNotfound" />, {
                variant: 'error',
              });
            } else {
              let users = []
              resp.forEach(item => {
                // users.push(item)
                const item_id = item.id;
                const value = item.name;
                users[item_id] = value;
              })

              this.setState({
                [type]: resp,
                [`${type}KeyValues`]: users
              })
            }
          }) .catch(error => this.props.enqueueSnackbar(error.toString(), {
            variant: 'error'
          }));
        })
        .catch(error => this.props.enqueueSnackbar(error.toString(), {
          variant: 'error'
        }));
    } catch (e) {
      this.props.enqueueSnackbar(e.toString(), {
        variant: 'error'
      })
    }
  }

  getData = (query, resolve, reject) => {

    if (this.query === undefined) {
      this.query = query
      this.query = undefined
    }


    let url = `${process.env.BASE_URL}/protected/hospitals?`
    url += "size=" + query.pageSize
    url += "&page=" + (query.page)
    url += "&search=" + (query.search)

     fetch(url, {
      method: "GET",
      mode: "cors",
      headers: {
        "Authorization": this.getToken(),
        "content-type": "application/json",
      }
    })
      .then(response => response.clone().json())
      .then(result => {

        if (result.content.length === 0) {
          if (result.totalElements !== 0) {
            query.page = query.page - 1
            this.getData(query, resolve, reject)
          } else {
            resolve({
              data: result.content,
              page: result.number,
              totalCount: result.totalElements,
            })
          }

        } else {
          resolve({
            data: result.content,
            page: result.number,
            totalCount: result.totalElements,
          })
        }
      }).catch(e => {
        console.log(e)
      });

  }


  refreshData() {
    this.tableRef.onQueryChange(this.query)
  }
  showProgress = () => {
    this.tableRef.setState({ isLoading: true })
  }
  hideProgress = () => {
    this.tableRef.setState({ isLoading: false })
  }



  call4AnalyticalReport(hospital) {

    this.showProgress()

    let url = `${process.env.BASE_URL}/protected/hospitals/generateAnalyticalReport/${hospital.id}`

    this.call4Report(url);


  }
  callExportExcel(hospital) {

    this.showProgress()

    let url = `${process.env.BASE_URL}/protected/hospitals/generateReport/${hospital.id}`

    this.call4Report(url);
  }

  call4Report(url) {
    fetch(url, {
      method: "POST",
      headers: {
        "Authorization": this.getToken(),
        "content-type": "application/json"
      }
    }).then(resp => resp.clone().text()).then(jsResult => {
      if (jsResult === "NO_EVALUATION_FOUND") {
        this.hideProgress()
        this.props.enqueueSnackbar(<Translate id="NO_EVALUATION_FOUND" />, {
          variant: 'info',
        })

      } else {
        this.checkIfExcelFileIsReady(jsResult)
      }
    }).catch(error => {
      this.hideProgress()
      this.props.enqueueSnackbar(error.toString(), {
        variant: 'error',
      })
    })
  }

  checkIfExcelFileIsReady(fileName) {

    let url = `${process.env.BASE_URL}/hospitals/checkIfReportFileIsReady/${fileName}`
    setTimeout(() => {
      fetch(url, {
        method: "POST",
        headers: {
          "Authorization": this.getToken(),
          "content-type": "application/json"
        }
      }).then(resp => resp.clone().text()).then(fileStatus => {
        if (fileStatus === "READY") {
          this.downlaodReportFile(fileName)
        } else if (fileStatus === "ERROR_GENERATING_REPORT_FILE") {
          this.hideProgress()
          this.props.enqueueSnackbar(<Translate id="ERROR_GENERATING_REPORT_FILE" />, {
            variant: 'error',
          })
        } else {
          this.checkIfExcelFileIsReady(fileName)
        }
      }).catch(error => {
        this.hideProgress()
        this.props.enqueueSnackbar(error.toString(), {
          variant: 'error',
        })
      })
    }, 300);
  }

  downlaodReportFile(fileName) {
    const url = `${process.env.BASE_URL}/hospitals/downloadreport/${fileName}`


    fetch(url).then(resp => resp.blob())
      .then(blob => {
        download(blob, fileName, 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet')
        this.hideProgress()
      }).catch(() => {
        this.hideProgress()
      });
  }
  showDownloadMenu(event, hospital) {
    this.setState({
      showDownloadMenuItems: true,
      anchorDownloadMenu: event.currentTarget,
      Hospital4MenuAttachments: hospital
    })
  }
  handleDownloadMenuItemClick(docType) {
    this.getHospitalAttachments(this.state.Hospital4MenuAttachments, docType)
    this.handleDownloadMenuClose()
  }
  getHospitalAttachments(hospital, docType) {

    const url = `${process.env.BASE_URL}/protected/attachments/get`


    var formData = new FormData();
    formData.append('type', 'HospitalDocs');
    formData.append('id', hospital.id.toString());

    this.showProgress()

    fetch(url, {
      method: "POST",
      headers: {
        "Authorization": this.getToken()
      },
      body: formData
    })
      .then(response => {
        if (response.ok) {
          response.json().then((resp) => {


            let downloadLink = undefined;
            let fileName = undefined;

            resp.forEach(item => {
              if (item.comment === docType) {
                downloadLink = `${process.env.BASE_URL}/downloadFile/${item.fileName}`
                fileName = item.fileName
              }
            })

            if (downloadLink == undefined) {
              this.hideProgress()
              this.props.enqueueSnackbar(<Translate id="AttachmentNotFound" />, {
                variant: 'info',
              });

            } else {

              fetch(downloadLink).then(resp => resp.blob())

                .then(blob => {

                  download(blob, fileName, 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet')

                  this.hideProgress()

                }).catch(() => {
                  this.hideProgress()
                });
            }


          });
        } else {
          this.hideProgress()
          if (response.status === 500) {
            this.props.enqueueSnackbar(<Translate id="ServerError" />, {
              variant: 'error',
            });
          } else if (response.status === 401) {
            this.props.enqueueSnackbar(<Translate id="UserValidationError" />, {
              variant: 'error',
            });
          } else {
            this.props.enqueueSnackbar(response.toString(), {
              variant: 'error',
            });
          }

        }

      })
      .catch(error => {
        this.hideProgress()
        this.props.enqueueSnackbar(error.toString(), {
          variant: 'error',

        })
      });
  }

  handleDownloadMenuClose() {

    this.setState({
      showDownloadMenuItems: false,
      anchorDownloadMenu: null
    })
  }

  showBuildingsGrid(hospital) {
    this.setState({
      showBuildingsGrid: true,
      hospital4BuildingsGrid: hospital
    })
  }
  onBuildingsGridDialogClosed() {
    this.setState({
      showBuildingsGrid: false,
      hospital4BuildingsGrid: null
    })
  }


  showAnalyticalReportDialog(hospital) {
    this.setState({
      hospital4Report: hospital,
      showAnalyticalReportDialog: true
    })
  }

  onHospitalRerportDialogClosed() {
    this.setState({
      showAnalyticalReportDialog: false,
      hospital4Report: null
    })
  }


  showAttachmentsDialog(hospital) {
    this.setState({
      hospital4AttachmentDialog: hospital,
      showAttachments: true
    })
  }
  closeAttachmentsDialog() {
    this.setState({
      hospital4AttachmentDialog: null,
      showAttachments: false,
      Hospital4MenuAttachments: null
    })
  }

  onBtnHospitalEditClicked(hospital) {
    this.setState({
      editingHospital: hospital
    })
  }


  doRuquest(resolve, reject, url, oldData) {
    fetch(url, {
      method: "POST",
      headers: {
        "Authorization": this.getToken(),
        "content-type": "application/json",
      },
      body: JSON.stringify(oldData)
    })
      .then(response => {
        if (response.ok) {
          response.json().then((resp) => {
            this.props.enqueueSnackbar(<Translate id="OperationDoneSuccessfully" />, {
              variant: 'success',
            });
            resolve()
          });
        } else {
          if (response.status === 500) {
            this.props.enqueueSnackbar(<Translate id="ServerError" />, {
              variant: 'error',
            });
          } else if (response.status === 401) {
            this.props.enqueueSnackbar(<Translate id="UserValidationError" />, {
              variant: 'error',
            });
          } else {
            this.props.enqueueSnackbar(response.toString(), {
              variant: 'error',
            });
          }
          reject()
        }

      })
      .catch(error => {
        reject()
        this.props.enqueueSnackbar(error.toString(), {
          variant: 'error',

        })
      });
  }
  render() {

    console.log('render hostpital grid');
    const { classes, theme } = this.props;

    const { showAnalyticalReportDialog, hospital4Report, hospital4BuildingsGrid, showBuildingsGrid, hospital4AttachmentDialog, showAttachments, editingHospital, surveyorsKeyValues, evaluatorsKeyValues, showDownloadMenuItems, anchorDownloadMenu } = this.state


    let name = 'name';
    const address = 'address';
    const phone = 'phone';
    const evaluatorUser = 'evaluatorUser';
    const surveyUser = 'surveyUser';
    const webSite = 'webSite';
    const numTotalApprovedBeds = 'numTotalApprovedBeds';
    const bedOccupancyNormalRate = 'bedOccupancyNormalRate';
    const clinicalStaffCount = 'clinicalStaffCount';
    const nonClinicalStaffCount = 'nonClinicalStaffCount';
    const organizationalAffiliation = 'organizationalAffiliation';
    const serviceType = 'serviceType';

    return (

      <div>
        {/* <Fab variant="extended" size="small" color="secondary" aria-label="Add" className={classNames(classes.fabRefreshGrid)} onClick={() => this.refreshData()} >
          <RefreshIcon className={classes.extendedIcon} />
          <Translate id="RefreshHospitalGrid" />
        </Fab> */}
        <MaterialTable
          columns={[
            {
              title: name, field: 'name',
              headerStyle: {
                fontFamily: '"Vazir", sans-serif'
              },
              cellStyle: {
                fontFamily: '"Vazir", sans-serif'
              }
            },
            {
              title: address, field: 'address',
              headerStyle: {
                fontFamily: '"Vazir", sans-serif'
              },
              cellStyle: {
                fontFamily: '"Vazir", sans-serif'
              }
            },
            {
              title: phone, field: 'phone',
              headerStyle: {
                fontFamily: '"Vazir", sans-serif'
              },
              cellStyle: {
                fontFamily: '"Vazir", sans-serif'
              },
              hidden: true
            },
            {
              title: evaluatorUser, field: 'evaluatorUserId', type: 'numeric',
              lookup: evaluatorsKeyValues,
              headerStyle: {
                fontFamily: '"Vazir", sans-serif'
              },
              cellStyle: {
                fontFamily: '"Vazir", sans-serif'
              }
            },
            {
              title: surveyUser, field: 'surveyorUserId', type: 'numeric',
              lookup: surveyorsKeyValues,
              headerStyle: {
                fontFamily: '"Vazir", sans-serif'
              },
              cellStyle: {
                fontFamily: '"Vazir", sans-serif'
              }
            },
            {
              title: webSite, field: 'webSite',
              headerStyle: {
                fontFamily: '"Vazir", sans-serif'
              },
              cellStyle: {
                fontFamily: '"Vazir", sans-serif'
              },
              hidden: true
            },
            {
              title: numTotalApprovedBeds, field: 'numTotalApprovedBeds', type: 'numeric',
              headerStyle: {
                fontFamily: '"Vazir", sans-serif'
              },
              cellStyle: {
                fontFamily: '"Vazir", sans-serif'
              },
              hidden: true
            },
            {
              title: bedOccupancyNormalRate, field: 'bedOccupancyNormalRate', type: 'numeric',
              headerStyle: {
                fontFamily: '"Vazir", sans-serif'
              },
              cellStyle: {
                fontFamily: '"Vazir", sans-serif'
              }
              , hidden: true
            },
            {
              title: clinicalStaffCount, field: 'clinicalStaffCount', type: 'numeric',
              headerStyle: {
                fontFamily: '"Vazir", sans-serif'
              },
              cellStyle: {
                fontFamily: '"Vazir", sans-serif'
              },
              hidden: true
            },
            {
              title: nonClinicalStaffCount, field: 'nonClinicalStaffCount', type: 'numeric',
              headerStyle: {
                fontFamily: '"Vazir", sans-serif'
              },
              cellStyle: {
                fontFamily: '"Vazir", sans-serif'
              },
              hidden: true
            },
            {
              title: organizationalAffiliation, field: 'organizationalAffiliation', type: 'numeric',
              lookup: organizationalAffiliationTypes,
              headerStyle: {
                fontFamily: '"Vazir", sans-serif'
              },
              cellStyle: {
                fontFamily: '"Vazir", sans-serif'
              }
            },
            {
              title: serviceType, field: 'serviceType', type: 'numeric',
              lookup: serviceTypes,
              headerStyle: {
                fontFamily: '"Vazir", sans-serif'
              },
              cellStyle: {
                fontFamily: '"Vazir", sans-serif'
              }
            }
          ]}
          data={query =>
            new Promise((resolve, reject) => {
              this.getData(query, resolve, reject)
            })
          }
          title='Hospitals' 
          options={{
            paging: true,
            maxBodyHeight: this.state.height - 250,
            toolbar: true,
            actionsColumnIndex: -1,
            showTitle: false,
            search: true,
            filtering: false,
            columnsButton: true,
            addRowPosition: "first",
            headerStyle: {
              fontFamily: '"Vazir", sans-serif'
            },
            doubleHorizontalScroll: true,
            actionsCellStyle: {
              fontFamily: '"Vazir", sans-serif'
            },
            rowStyle: {
              fontFamily: '"Vazir", sans-serif'
            },
            searchFieldStyle: {
              fontFamily: '"Vazir", sans-serif'
            },
            rowStyle: {
              fontFamily: '"Vazir", sans-serif'
            }
          }}
          actions={[
            {
              icon: CloudDownloadIcon,
              tooltip: 'downloadDocs',
              onClick: (event, data) => {
                this.showDownloadMenu(event, data)
              }
            },
            {
              icon: AccountBalance,
              tooltip: 'buildings',
              onClick: (event, data) => {
                this.showBuildingsGrid(data)
              }
            },
            {
              icon: Assignment,
              tooltip: 'InsertCommentsForHospital',
              onClick: (event, data) => {
                this.showAnalyticalReportDialog(data)
              }
            },
            {
              icon: ListAltIcon,
              tooltip: 'ExportHospitalExcellReport',
              onClick: (event, data) => {
                this.callExportExcel(data)
              }
            }, {
              icon: BarChart,
              tooltip: 'ExportHospitalAnalyticalReport',
              onClick: (event, data) => {
                this.call4AnalyticalReport(data)
              }
            },

            {
              icon: AttachmentIcon,
              tooltip: 'AttachmentManage',
              onClick: (event, data) => {
                this.showAttachmentsDialog(data)
              }
            }
            // ,
            // {
            //   icon: 'edit',
            //   tooltip: <Translate id="EditAction" />,
            //   onClick: (event, data) => {
            //     this.onBtnHospitalEditClicked(data)
            //   }
            // }
          ]}
          tableRef={table => this.tableRef = table}
          editable={{
            onRowDelete: oldData =>
              new Promise((resolve, reject) => {
                let url = `${process.env.BASE_URL}/protected/hospitals/delete/${oldData.id}`
                fetch(url, {
                  method: "POST",  // *GET, POST, PUT, DELETE, etc.  
                  headers: {
                    "Authorization": this.getToken(),
                    "content-type": "application/json",
                  },
                })
                  .then(response => {
                    if (response.ok) {
                      const resJson = response.json()
                      if (resJson != undefined) {
                        resJson.then((resp) => {
                          if (resp === true) {
                            this.props.enqueueSnackbar("OperationDoneSuccessfully", {
                              variant: 'success',
                            });

                            resolve()
                          } else {
                            if (resp.message === "User not found") {
                              this.props.enqueueSnackbar("UserNotfound", {
                                variant: 'error',
                              });
                            } else if (resp.localizedMessage === "Action not allowed") {
                              this.props.enqueueSnackbar("ActionNotAllowed", {
                                variant: 'error',
                              });
                            } else {
                              this.props.enqueueSnackbar(resp.toString(), {
                                variant: 'error',
                              });
                            }
                            reject()
                          }
                        });
                      }
                    } else {
                      if (response.status === 500) {
                        this.props.enqueueSnackbar(<Translate id="ServerError" />, {
                          variant: 'error',
                        });
                      } else if (response.status === 401) {
                        this.props.enqueueSnackbar(<Translate id="UserValidationError" />, {
                          variant: 'error',
                        });
                      } else {
                        this.props.enqueueSnackbar(response.toString(), {
                          variant: 'error',
                        });
                      }
                      reject()
                    }

                  })
                  .catch(error => { 
                    this.props.enqueueSnackbar(error.toString(), {
                      variant: 'error',
                    })
                  });
              }),
            onRowUpdate: oldData =>
              new Promise((resolve, reject) => {
                let url = `${process.env.BASE_URL}/protected/hospitals/update`;
                this.state.evaluators.forEach(item => {
                  if (item.id === parseInt(oldData.evaluatorUserId))
                    oldData.evaluatorUser = item
                })
                this.state.surveyors.forEach(item => {
                  if (item.id === parseInt(oldData.surveyorUserId))
                    oldData.surveyorUser = item
                }) 
                this.doRuquest(resolve, reject, url, oldData)
              }),

          }}
          localization={gridLocalization}

        >

        </MaterialTable>

        {
          showAnalyticalReportDialog ? <HospitalAnalyticalReportDetailDialog hospital={hospital4Report} onDialogClosed={this.onHospitalRerportDialogClosed} handleOkClicked={this.refreshData}></HospitalAnalyticalReportDetailDialog> : null
        }
        {
          showBuildingsGrid ? <BuildingsGrid hospital={hospital4BuildingsGrid} onDialogClosed={this.onBuildingsGridDialogClosed}></BuildingsGrid> : null
        }
        {
          showAttachments ? <AttachmentsDialog attachmentType="HospitalDocs" entityId={hospital4AttachmentDialog.id} entityName={hospital4AttachmentDialog.name} onDialogClosed={this.closeAttachmentsDialog} EnableAdd={this.state.Hospital4MenuAttachments === null}></AttachmentsDialog> : null
        }
        {
          editingHospital === null ? null : <HospitalEdit hospital={editingHospital} onDialogClosed={() => this.onBtnHospitalEditClicked(null)} />
        }
        <Menu
          id="simple-menu"
          anchorEl={anchorDownloadMenu}
          keepMounted
          open={showDownloadMenuItems}
          onClose={this.handleDownloadMenuClose}
        >

          <MenuItem onClick={() => this.handleDownloadMenuItemClick("RVA_Report")}>
            <ListItemIcon>
              {/* <HomeWorkIcon /> */}
            </ListItemIcon>
            <ListItemText primary={<Translate id="RVA_Report"></Translate>} />
          </MenuItem>
          <MenuItem onClick={() => this.handleDownloadMenuItemClick("PEA_REPORT")} role="RVA_Report" >
            <ListItemIcon>
              {/* <HomeWorkIcon /> */}
            </ListItemIcon>
            <ListItemText primary={<Translate id="PEA_REPORT"></Translate>} />
          </MenuItem>
          <MenuItem onClick={() => this.handleDownloadMenuItemClick("NonStructuralReport")}>
            <ListItemIcon>
              {/* <HomeWorkIcon /> */}
            </ListItemIcon>
            <ListItemText primary={<Translate id="NonStructuralReport"></Translate>} />
          </MenuItem>
          <MenuItem onClick={() => this.showAttachmentsDialog(this.state.Hospital4MenuAttachments)}>
            <ListItemIcon>
              {/* <HomeWorkIcon /> */}
            </ListItemIcon>
            <ListItemText primary={<Translate id="SuplementaryDocs"></Translate>} />
          </MenuItem>
        </Menu>
      </div>

    )

  }

}

HospitalsGrid.propTypes = {
  classes: PropTypes.object.isRequired,
  theme: PropTypes.object.isRequired,
};
export default withStyles(styles, { withTheme: true })(withLocalize(withSnackbar(HospitalsGrid)));


