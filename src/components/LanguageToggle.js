import React, { Component } from "react";
import PropTypes from 'prop-types';
import { withLocalize, Translate } from "react-localize-redux";
import Button from '@material-ui/core/Button';
import { withStyles } from '@material-ui/core/styles';
import globalTranslations from "../translations/global.json";
import Cookies from 'universal-cookie';


const styles = theme => ({
  root: {
    flexGrow: 1
  },
  grow: {
    flexGrow: 1,
  },
  button: {
    margin: theme.spacing.unit,
  }

})

class LanguageToggle extends Component {

  constructor(props) {
    super(props)
    this.props.addTranslation(globalTranslations);
  }
  componentDidUpdate(prevProps) {
    const prevLangCode =
      prevProps.activeLanguage && prevProps.activeLanguage.code;
    const curLangCode =
      this.props.activeLanguage && this.props.activeLanguage.code;

    const hasLanguageChanged = prevLangCode !== curLangCode;

 
    if (hasLanguageChanged) {  
      const cookies = new Cookies();
      cookies.set('languageCode', curLangCode, { path: '/' }); 
    }
  }

  render() {
    const { languages, activeLanguage, setActiveLanguage, classes, selectColor, unSelectColor } = this.props

    return (
      < div className="selector" >
        {
          languages.map(lang => (
            <Button className={classes.button} key={lang.code} color={(() => activeLanguage.name === lang.name ? selectColor : unSelectColor)()} onClick={() => setActiveLanguage(lang.code)}>
              <Translate id={lang.name} />
            </Button>
          ))
        }
      </div >
    )
  }
}

LanguageToggle.propTypes = {
  classes: PropTypes.object.isRequired,
};
export default withStyles(styles)(withLocalize(LanguageToggle));