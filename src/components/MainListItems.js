import React, { Component } from 'react';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import PeopleIcon from '@material-ui/icons/People';
import LayersIcon from '@material-ui/icons/Layers';
import SettingsIcon from '@material-ui/icons/Settings';
import LocalHospitalIcon from '@material-ui/icons/LocalHospital';
import { Paper, Collapse, List } from '@material-ui/core';
import PhonelinkSetup from '@material-ui/icons/PhonelinkSetup';
import ExpandLess from '@material-ui/icons/ExpandLess';
import ExpandMore from '@material-ui/icons/ExpandMore';
import { Translate, withLocalize } from 'react-localize-redux';
import { withStyles } from '@material-ui/core/styles';
import ListSubheader from '@material-ui/core/ListSubheader';
import PropTypes from 'prop-types';
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';
import Typography from '@material-ui/core/Typography';
import WarningIcon from '@material-ui/icons/Warning';
import EqualizerIcon from '@material-ui/icons/Equalizer';
import SortIcon from '@material-ui/icons/Sort';
import HelpOutlineIcon from '@material-ui/icons/HelpOutline';
import MapIcon from '@material-ui/icons/Map';
import Tooltip from '@material-ui/core/Tooltip'; 
import DashboardIcon from '@material-ui/icons/Dashboard';


const styles = theme => ({
    root: {
        width: '100%',
        maxWidth: 360,
        backgroundColor: theme.palette.background.paper,
    },
    nested: {
        paddingLeft: theme.spacing.unit * 4,
    },

});

export const ListItems = {
    Dashboard: "Dashboard",
    Layers: "Map",
    UserManage: "Users",
    Hospitals: "Hospitals",
    Settings: "Settings",
    MobileSettings: "MobileSettings",
    LevelOfHazard: "LevelOfHazard",
    ExposureIndex: "ExposureIndex",
    RelativeWeight: "RelativeWeight",
    Questions: "Questions"
}
class MainListItems extends Component {

    constructor(props) {
        super(props)
        this.handleClick = this.handleClick.bind(this);
        this.startAction = this.startAction.bind(this);
        this.onSettingMenuClicked = this.onSettingMenuClicked.bind(this);
        this.onSettingMenuClosed = this.onSettingMenuClosed.bind(this);
        this.state = {
            anchorEl: null,
            setAnchorEl: null
        }

    }


    handleClick(itemKey) {
        this.props.handleClick(itemKey);
        this.startAction()
    }

    onSettingMenuClicked(event) {
        this.setState({
            anchorEl: event.currentTarget
        })
    }
    onSettingMenuClosed(itemKey) {
        this.setState({
            anchorEl: null
        })
        this.props.handleClick(itemKey);
    }

    startAction() {

    }

    render() {

        const { anchorEl } = this.state
        const { classes } = this.props

        return (

            <div>
                <ListItem button onClick={() => this.handleClick(ListItems.Layers)}>

                    <ListItemIcon >
                        <Tooltip title={<Translate id="Map" />} aria-label="add">
                            <MapIcon />
                        </Tooltip>
                    </ListItemIcon>
                    <ListItemText primary={<Translate id="Map" />} />

                </ListItem>
                <ListItem button onClick={() => this.handleClick(ListItems.Dashboard)}>

                    <ListItemIcon >
                        <Tooltip title={<Translate id="Dashboard" />} aria-label="Dashboard">
                            <DashboardIcon />
                        </Tooltip>
                    </ListItemIcon>
                    <ListItemText primary={<Translate id="Dashboard" />} />
                </ListItem> 
                <ListItem button onClick={() => this.handleClick(ListItems.Hospitals)}>
                    <ListItemIcon>
                        <Tooltip title={<Translate id="Hospitals" />} aria-label="add">
                            <LocalHospitalIcon />
                        </Tooltip>
                    </ListItemIcon>
                    <ListItemText primary={<Translate id="Hospitals" />} />
                </ListItem>
                <ListItem button onClick={() => this.handleClick(ListItems.UserManage)}>
                    <ListItemIcon>
                        <Tooltip title={<Translate id="Users" />} aria-label="add">
                            <PeopleIcon />
                        </Tooltip>
                    </ListItemIcon>
                    <ListItemText primary={<Translate id="Users" />} />
                </ListItem>
                <ListItem button onClick={this.onSettingMenuClicked}>
                    <ListItemIcon>
                        <Tooltip title={<Translate id="Settings" />} aria-label="add">
                            <SettingsIcon />
                        </Tooltip>
                    </ListItemIcon>
                    <ListItemText primary={<Translate id="Settings" />} />
                </ListItem>
                <Menu
                    id="simple-menu"
                    anchorEl={anchorEl}
                    keepMounted
                    open={Boolean(anchorEl)}
                    onClose={this.onSettingMenuClosed}
                >
                    <MenuItem onClick={() => this.onSettingMenuClosed(ListItems.MobileSettings)}  >
                        <ListItemIcon>
                            <PhonelinkSetup />
                        </ListItemIcon>
                        <Typography variant="inherit" noWrap>
                            <Translate id="MobileSettings" />
                        </Typography>
                    </MenuItem>

                    <MenuItem onClick={() => this.onSettingMenuClosed(ListItems.Questions)}  >
                        <ListItemIcon>
                            <HelpOutlineIcon />
                        </ListItemIcon>
                        <Typography variant="inherit" noWrap>
                            <Translate id="Questions" />
                        </Typography>
                    </MenuItem>

                    <MenuItem onClick={() => this.onSettingMenuClosed(ListItems.LevelOfHazard)}  >
                        <ListItemIcon>
                            <WarningIcon />
                        </ListItemIcon>
                        <Typography variant="inherit" noWrap>
                            <Translate id="LevelOfHazard" />
                        </Typography>

                    </MenuItem>
                    <MenuItem onClick={() => this.onSettingMenuClosed(ListItems.RelativeWeight)}  >
                        <ListItemIcon>
                            <EqualizerIcon />
                        </ListItemIcon>
                        <Typography variant="inherit" noWrap>
                            <Translate id="RelativeWeight" />
                        </Typography>
                    </MenuItem>
                    <MenuItem onClick={() => this.onSettingMenuClosed(ListItems.ExposureIndex)}  >
                        <ListItemIcon>
                            <SortIcon />
                        </ListItemIcon>
                        <Typography variant="inherit" noWrap>
                            <Translate id="ExposureIndex" />
                        </Typography>
                    </MenuItem>
                </Menu>


            </div>
        )
    }

}

MainListItems.propTypes = {
    classes: PropTypes.object.isRequired,
    theme: PropTypes.object.isRequired,
};
export default withStyles(styles, { withTheme: true })(withLocalize(MainListItems));
