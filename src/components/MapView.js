import React, { Component } from "react";
import ReactDOM from 'react-dom';
import VectorSource from 'ol/source/Vector';
import Map from 'ol/Map';
import View from 'ol/View';
import Select from 'ol/interaction/Select';
import OMSLayer from 'ol/source/OSM';
import { fromLonLat, transform } from 'ol/proj';
import { defaults } from 'ol/control'
import '../stylesheets/MapView.css'
import '../stylesheets/popup.css'
import Feature from 'ol/Feature.js';
import Point from 'ol/geom/Point.js';
import Overlay from 'ol/Overlay.js';
import { toStringHDMS } from 'ol/coordinate.js';
import { toLonLat } from 'ol/proj.js';
import { ImageWMS } from 'ol/source';
import { Tile, Vector as VectorLayer, Image as ImageLayer } from 'ol/layer.js';
import { WFS, GeoJSON } from 'ol/format';
import { Icon, Style, Stroke, Text, Fill, Circle as CircleStyle, RegularShape } from 'ol/style.js';
import ScaleLine from 'ol/control/ScaleLine';
import jsPDF from 'jspdf';
import ZoomSlider from 'ol/control/ZoomSlider';
import MousePosition from 'ol/control/MousePosition';
import Tooltip from '@material-ui/core/Tooltip';
import Legend from 'ol-ext/control/Legend'
import 'ol-ext/control/Legend.css';
import Image from 'material-ui-image'
import Print from 'ol-ext/control/Print'
import 'ol-ext/control/Print.css';
import Cookies from 'universal-cookie';
import AttachmentsDialog from '../pages/AttachmentsDialog';
import SearchNominatim from 'ol-ext/control/SearchNominatim';
import 'ol-ext/control/Search.css';
import 'ol-ext/dist/ol-ext.css'
import globalTranslations from "../translations/global.json";

// import  LayerSwitcherImage from 'ol-ext/control/LayerSwitcherImage'
// import 'ol-ext/control/LayerSwitcherImage.css';

import ol_ordering from 'ol-ext/render/Ordering';
import Chart from 'ol-ext/style/Chart';


import BingMaps from 'ol/source/BingMaps';

import '../stylesheets/ol.css';
import BottomSheet from "./BottomSheet";
import HospitalsGrid from "./HospitalsGrid";
import HospitalEdit from '../pages/HospitalEdit';
import { Translate, withLocalize } from 'react-localize-redux';

import hospital from '../images/hospital.svg'
import hospital_local from '../images/hospital_local.svg'

import { withStyles } from '@material-ui/core/styles';

import AddIcon from '@material-ui/icons/Add';
import LayersIcon from '@material-ui/icons/Layers';
import HospitalIcon from '@material-ui/icons/LocalHospital';

import Fab from '@material-ui/core/Fab';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import IconButton from '@material-ui/core/IconButton';
import UsersGrid from '../components/UsersGrid';
import ProgressDialog from '../components/ProgressDialog';
import Toc from '../components/Toc';
import download from 'downloadjs';

import { ListItems } from '../components/MainListItems';
import { withSnackbar } from 'notistack';
import { Base64 } from 'js-base64';

import BarChart from '@material-ui/icons/BarChart'
import AttachmentIcon from '@material-ui/icons/Attachment'
import MapIcon from '@material-ui/icons/Map';
import ListAltIcon from '@material-ui/icons/ListAlt'
import CloudDownloadIcon from '@material-ui/icons/CloudDownload'
import AccountBalance from '@material-ui/icons/AccountBalance'
import Assignment from '@material-ui/icons/Assignment'
import { lang } from "moment";
import { Redirect } from 'react-router-dom'
import { renderToStaticMarkup } from "react-dom/server";

const layerInfos = Array.of(
    {
        "layerName": "hospital_area",
        "Fields": [
            { "Name": "name", "Alias_en": "Name", "Alias_fa": "نام" }
        ],
        cql_filter: "status <> 1"
    }
    , {
        "layerName": "candidate_location",
        "Fields": [
            { "Name": "name", "Alias_en": "Name", "Alias_fa": "نام" }
        ],
        cql_filter: "status <> 1"
    }
    , {
        "layerName": "building",
        "Fields": [
            { "Name": "name", "Alias_en": "Name", "Alias_fa": "نام" }
        ],
        cql_filter: "status <> 1"
    }, {
    "layerName": "province",
    "Fields": [],
    cql_filter: undefined
}
    , {
        "layerName": "hospital_center_point",
        "Fields": [
            { "Name": "name", "Alias_en": "Name", "Alias_fa": "نام" }
        ],
        cql_filter: undefined
    },
    {
        "layerName": "hospital_entrance",
        "Fields": [
            { "Name": "name", "Alias_en": "Name", "Alias_fa": "نام" }
        ],
        cql_filter: "status <> 1"
    }
)

const styles = theme => ({
    root: {
        position: 'absolute',
        widows: '100%',
        height: '100%',
        padding: '0%',
    },
    addbtn: {
        position: 'absolute',
        top: theme.spacing.unit * 10,
        right: theme.spacing.unit,
        zIndex: 20
    },
    tocBtn: {
        position: 'absolute',
        top: theme.spacing.unit * 16,
        right: theme.spacing.unit,
        zIndex: 20
    },
    extendedIcon: {
        marginRight: theme.spacing.unit,
    },
    card: {
        minWidth: 275,
    },
    title: {
        fontSize: 14,
    },
    pos: {
        marginBottom: 12,
    },

});


class MapView extends Component {

    constructor(props) {
        super(props)
         
        const cookies = new Cookies();
        const lang = cookies.get('languageCode') || "en";
        this.props.initialize({
          languages: [
            { name: "English", code: "en" },
            { name: "Farsi", code: "fa" }
          ],
          translation: globalTranslations,
          options: {
            renderToStaticMarkup,
            renderInnerHtml: true,
            defaultLanguage: lang
          }
        });
    
        this.state = {
            width: 0, height: 0,
            isShowingPanel: false,
            markerFeature: undefined,
            map: undefined,
            edititngHospital: undefined,
            showToc: false,
            layers: Array.of,
            showToc: false,
            IdentifiedFeaturePhotos: Array.of(),
            IdentifiedFeatureId: undefined,
            IdentifiedFeatureName: undefined,
            ShowAttachments: false,
            GenerateRvaReport: false,
            RedirectToHospitalPage: false
        };
        this.updateWindowDimensions = this.updateWindowDimensions.bind(this);
        this.onFabAddHospitalClicked = this.onFabAddHospitalClicked.bind(this);
        this.onPanelClosed = this.onPanelClosed.bind(this);
        this.handleMapClick = this.handleMapClick.bind(this);
        this.getMapCenter = this.getMapCenter.bind(this);
        this.onHospitalEditClicked = this.onHospitalEditClicked.bind(this);
        this.btoaUTF16 = this.btoaUTF16.bind(this);
        this.onMarkerFeatureLocationChanged = this.onMarkerFeatureLocationChanged.bind(this);
        this.getProvinceFeatureStyle = this.getProvinceFeatureStyle.bind(this);
        this.getFeatureStyle = this.getFeatureStyle.bind(this);
        this.hexToRgb = this.hexToRgb.bind(this);
        this.toggleTocDialog = this.toggleTocDialog.bind(this);
        this.doRequest = this.doRequest.bind(this);
        this.closeAttachmentsDialog = this.closeAttachmentsDialog.bind(this);
        this.getToken = this.getToken.bind(this)
        this.onBtnAttachmentClicked = this.onBtnAttachmentClicked.bind(this);
        this.onBtnRvaReportClicked = this.onBtnRvaReportClicked.bind(this);
        this.onBtnRedirectToHospitalsPage = this.onBtnRedirectToHospitalsPage.bind(this);
        this.call4Report = this.call4Report.bind(this);
        this.styleCache = {};
    }
    updateWindowDimensions() {
        this.setState({ width: window.innerWidth, height: window.innerHeight });
    }
    componentWillUnmount() {
        window.removeEventListener('resize', this.updateWindowDimensions);
    }
    componentDidMount() {
        this.updateWindowDimensions();
        window.addEventListener('resize', this.updateWindowDimensions);

        /**
       * Create an overlay to anchor the popup to the map.
       */
        var overlayPopUp = new Overlay({
            element: this.refs.popup,
            autoPan: true,
            autoPanAnimation: {
                duration: 250
            }
        });

        const popupCloser = this.refs.popup_closer;

        popupCloser.onclick = () => {
            overlayPopUp.setPosition(undefined);
            popupCloser.blur();
            this.setState({
                IdentifiedFeaturePhotos: Array.of(),
                IdentifiedFeatureId: undefined
            })
            return false;
        };


        const format = 'image/png';

        const wmsUrl = `${process.env.GEOSERVER_URL}/geoserver/behtab/wms`;


        let markerFeatures = Array.of()

        let markerLayerSource = new VectorSource({
            features: markerFeatures
        });

        var MarkerlLayer = new VectorLayer({
            source: markerLayerSource
        });

        // create map object with feature layer
        var map = new Map({
            target: this.refs.mapContainer,
            controls: defaults({
                attributeOptions: {
                    collapsible: false
                }

            }),
            overlays: [overlayPopUp],
            layers: [
                //default OSM layer
                new Tile({
                    source: new OMSLayer()
                }),
                new Tile({
                    visible: false,
                    preload: Infinity,
                    source: new BingMaps({
                        key: 'Aq36MUPDj_tXCpSoTxWmKqx4jz9ZyjxHWt5o_2sqsRR32Jn0RUtQUINlXiZu5Ipd',
                        imagerySet: 'AerialWithLabels'
                        // use maxZoom 19 to see stretched tiles instead of the BingMaps
                        // "no photos at this zoom level" tiles
                        // maxZoom: 19
                    })
                }),
                MarkerlLayer
            ],
            view: new View({
                center: fromLonLat([51.400261, 35.704338]),//Boulder
                zoom: 9,
            })
        });

        map.on('click', this.handleMapClick);


        const layers = Array.of()

        layerInfos.forEach(layerInfo => {

            const paramsObj = {
                'FORMAT': format,
                'VERSION': '1.1.1',
                "STYLES": '',
                "LAYERS": `behtab:${layerInfo.layerName}`,
                "exceptions": 'application/vnd.ogc.se_inimage'
            }

            if (layerInfo.cql_filter !== undefined) {
                paramsObj["cql_filter"] = layerInfo.cql_filter
            }

            const layer = new ImageLayer({
                source: new ImageWMS({
                    ratio: 1,
                    url: wmsUrl,
                    params: paramsObj
                })
            })

            layers.push(layer)
            map.addLayer(layer)

        })
        map.addControl(new ScaleLine())
        // map.addControl(new ZoomSlider())
        // map.addControl(new MousePosition())

        // map.addControl (new  LayerSwitcherImage());

        // Search control
        const search = new SearchNominatim();
        // Move to the position on selection in the control list
        search.on('select', function (e) {
            map.getView().animate({
                center: e.coordinate,
                zoom: Math.max(map.getView().getZoom(), 16)
            });
        });
        map.addControl(search);


        const u = `${process.env.GEOSERVER_URL}/geoserver/behtab/ows?service=WFS&version=1.0.0&request=GetFeature&typeName=behtab:province_risk_index&maxFeatures=100&outputFormat=application/json`;
        const v = new VectorLayer({
            title: 'comgeo',
            source: new VectorSource({
                url: u,
                format: new GeoJSON()
            }),
            // y ordering
            renderOrder: ol_ordering.yOrdering,
            style: (f) => { return this.getFeatureStyle(f); }
        });

        map.addLayer(v);


        // Define a new legend
        const legend = new Legend({
            title: 'Legend',
            margin: 5,
            style: this.getFeatureStyle,
            collapsed: false
        });
        // map.addControl(legend);

        legend.addRow({ title: '2.600.000' });
        legend.addRow({ title: '', properties: { pop: 2600000 }, typeGeom: 'Point' });

        var forms = { Trianle: 3, Square: 4, Pentagon: 5, Hexagon: 6 };
        const fillColors = ["#ff0", "#0ff", "#0f0", "#f0f", "#f00", "#00f"];
        const titles = ["structural", "non_structural", "institutional", "riskindex", "#f00", "#00f"];

        for (let i = 0; i < forms.length; i++) {
            const form = forms[i]

            var stroke = new Stroke({ color: 'black', width: 2 });
            var fill = new Fill({ color: fillColors[i] });

            legend.addRow({
                title: titles[i],
                typeGeom: 'Point',
                style: new Style({
                    image: new RegularShape({
                        points: form,
                        radius: 30,
                        stroke: new Stroke({ color: [255, 128, 0, 1], width: 1.5 }),
                        fill: fill
                    })
                })
            });

        }
        this.loadLayers()

        // Control Select 
        var select = new Select({
            style: (f) => { return this.getFeatureStyle(f, true); }
        });
        map.addInteraction(select);



        /**
              * Add a click handler to the map to render the popup.
              */
        map.on('singleclick', (evt) => {

            const cookies = new Cookies();
            const lang = cookies.get('languageCode') || "en";


            const coordinate = evt.coordinate;
            const hdms = toStringHDMS(toLonLat(coordinate));

            const requestUrlsGetFeatureInfo = Array.of();

            var viewResolution = /** @type {number} */ (map.getView().getResolution());

            const reversedLayers = layers.reverse()

            reversedLayers.forEach(layer => {
                var url = layer.getSource().getGetFeatureInfoUrl(
                    evt.coordinate, viewResolution, 'EPSG:3857',
                    { 'INFO_FORMAT': 'application/json' });

                requestUrlsGetFeatureInfo.push(fetch(url).then(value => value.json()));
            })



            Promise.all(requestUrlsGetFeatureInfo)
                .then((value) => {

                    for (let i in value) {
                        let item = value[i]
                        if (item.features.length == 1) {

                            const feature = item.features[0]
                            const layerName = feature.id.split(".")[0]

                            if (layerName !== "province") {

                                let atts = layerName

                                let layerInfo = null;

                                layerInfos.forEach(l => {
                                    if (l.layerName === layerName) {
                                        layerInfo = l
                                    }
                                })
  
                                const obj = feature.properties

                                layerInfo.Fields.forEach(field => {
                                    if (atts.length > 0)
                                        atts += "\n"
                                    atts += `${lang == "en" ? field.Alias_en : field.Alias_fa} : ${feature.properties[field.Name]}`
                                })

                                // Object.keys(obj).forEach(e => {
                                //     if (atts.length > 0)
                                //         atts += "\n"
                                //     if (layerInfo.layerName === layerName) {
                                //         atts += `${e} : ${obj[e]}`
                                //     }
                                // });

                                if (layerName === "hospital_area" || layerName === "hospital_center_point" || layerName === "hospital_entrance") {
                                    this.refs.popContent_bottom_content.style.display = "block"

                                    this.refs.popContent.innerText = atts
                                    overlayPopUp.setPosition(coordinate);

                                    let entityId = undefined
                                    let hospitalId = undefined
                                    let hospitalName = undefined

                                    if (layerName === "hospital_center_point") {
                                        entityId = feature.properties["id"];
                                        hospitalName = feature.properties["name"]
                                        hospitalId = entityId
                                    }

                                    if (layerName === "hospital_area") {
                                        entityId = feature.id.split(".")[1]
                                        hospitalName = feature.properties["name"]
                                        hospitalId = entityId
                                    }

                                    if (layerName === "hospital_entrance") {
                                        console.log(feature)
                                        hospitalId = feature.properties["hospital_id"]
                                        entityId = feature.id.split(".")[1]
                                        hospitalName = feature.properties["name"]
                                    }

                                    this.refs.popContent_btn_attachment.onclick = this.onBtnAttachmentClicked
                                    this.refs.popContent_btn_rva.onclick = this.onBtnRvaReportClicked
                                    // this.refs.popContent_btn_redirect_to_hospital_page.onclick = this.onBtnRedirectToHospitalsPage



                                    this.doRequest(`/protected/attachments/get?type=${layerName === "hospital_entrance" ? "HospitalEntrance" : "Hospital"}&id=${entityId}`, "GET", (resp) => {
                                        console.log(resp)

                                        this.setState({
                                            IdentifiedFeaturePhotos: resp.content,
                                            IdentifiedFeatureId: hospitalId,
                                            IdentifiedFeatureName: hospitalName
                                        })
                                    });

                                    break;
                                } else {
                                    this.refs.popContent_bottom_content.style.display = "none"

                                    this.refs.popContent.innerText = atts
                                    overlayPopUp.setPosition(coordinate);

                                    this.setState({
                                        AttachmentEntityType: undefined,
                                        AttachmentEntityId: undefined
                                    })
                                    break;
                                }
                            }
                        }

                    }


                })
                .catch((err) => {
                    console.log(err);
                });
        });


        // save map and layer references to local state
        this.setState({
            map: map,
            MarkerlLayer: MarkerlLayer,
            layers: layers
        });


    }


    getToken() {
        const cookies = new Cookies();
        return cookies.get('token') || ''
    }
    doRequest(url, method, func) {
        try {
            fetch(`${process.env.BASE_URL}${url}`, {
                method: method,
                headers: {
                    "Content-Type": "application/json",
                    "Authorization": this.getToken()
                },
            }).then(data => {

                if (data.ok) {

                    data.json().then((resp) => {

                        if (resp === "User not found") {
                            this.props.enqueueSnackbar(<Translate id="UserNotfound" />, {
                                variant: 'error',
                            });
                        } else {
                            func(resp)
                        }
                    });
                } else {
                    if (data.status === 500) {
                        this.props.enqueueSnackbar(<Translate id="ServerError" />, {
                            variant: 'error',
                        });
                    } else if (data.status === 401) {
                        this.props.enqueueSnackbar(<Translate id="UserValidationError" />, {
                            variant: 'error',
                        });
                    } else {
                        this.props.enqueueSnackbar(data.toString(), {
                            variant: 'error',
                        });
                    }
                }

            })
                .catch(error => this.props.enqueueSnackbar(error.toString(), {
                    variant: 'error'
                }));
        } catch (e) {
            this.props.enqueueSnackbar(e.toString(), {
                variant: 'error'
            })
        }
    }


    hexToRgb(hex) {
        var result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);
        return result ? {
            r: parseInt(result[1], 16),
            g: parseInt(result[2], 16),
            b: parseInt(result[3], 16)
        } : null;
    }

    btoaUTF16(sString) {

        var aUTF16CodeUnits = new Uint16Array(sString.length);
        Array.prototype.forEach.call(aUTF16CodeUnits, function (el, idx, arr) { arr[idx] = sString.charCodeAt(idx); });
        return btoa(String.fromCharCode.apply(null, new Uint8Array(aUTF16CodeUnits.buffer)));

    }



    onBtnAttachmentClicked() {
        console.log('onBtnAttachmentClicked')
        this.setState({
            ShowAttachments: true
        })
    }
    onBtnRvaReportClicked() {

        const { IdentifiedFeatureId } = this.state

        console.log(IdentifiedFeatureId)

        if (IdentifiedFeatureId === undefined)
            return

        let url = `${process.env.BASE_URL}/protected/hospitals/generateAnalyticalReport/${IdentifiedFeatureId}`

        this.call4Report(url);

        this.setState({
            GenerateRvaReport: true
        })
    }

    onBtnRedirectToHospitalsPage() {
        this.setState({
            RedirectToHospitalPage: true
        })
    }

    call4Report(url) {
        fetch(url, {
            method: "POST",
            headers: {
                "Authorization": this.getToken(),
                "content-type": "application/json"
            }
        }).then(resp => resp.clone().text()).then(jsResult => {
            if (jsResult === "NO_EVALUATION_FOUND") {
                this.hideProgress()
                this.props.enqueueSnackbar(<Translate id="NO_EVALUATION_FOUND" />, {
                    variant: 'info',
                })

            } else {
                this.checkIfExcelFileIsReady(jsResult)
            }
        }).catch(error => {
            this.hideProgress()
            this.props.enqueueSnackbar(error.toString(), {
                variant: 'error',
            })
        })
    }
    hideProgress() {
        this.setState({
            GenerateRvaReport: false
        })
    }

    checkIfExcelFileIsReady(fileName) {

        let url = `${process.env.BASE_URL}/hospitals/checkIfReportFileIsReady/${fileName}`
        setTimeout(() => {
            fetch(url, {
                method: "POST",
                headers: {
                    "Authorization": this.getToken(),
                    "content-type": "application/json"
                }
            }).then(resp => resp.clone().text()).then(fileStatus => {
                if (fileStatus === "READY") {
                    this.downlaodReportFile(fileName)
                } else if (fileStatus === "ERROR_GENERATING_REPORT_FILE") {
                    this.hideProgress()
                    this.props.enqueueSnackbar(<Translate id="ERROR_GENERATING_REPORT_FILE" />, {
                        variant: 'error',
                    })
                } else {
                    this.checkIfExcelFileIsReady(fileName)
                }
            }).catch(error => {
                this.hideProgress()
                this.props.enqueueSnackbar(error.toString(), {
                    variant: 'error',
                })
            })
        }, 300);
    }

    downlaodReportFile(fileName) {
        const url = `${process.env.BASE_URL}/hospitals/downloadreport/${fileName}`
        this.hideProgress()

        fetch(url).then(resp => resp.blob())
            .then(blob => {
                download(blob, fileName, 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet')
            })
    }

    getFeatureStyle(feature, sel) {
        const data = Array.of(feature.get("structural"), feature.get("non_structural"), feature.get("institutional"), feature.get("riskindex"))

        const c = "neon";

        const k = "bar-neon-" + (sel ? "1-" : "") + data;
        let style = this.styleCache[k];


        if (!style) {
            let radius = 30;
            radius *= (sel ? 1.2 : 1);
            //Create chart style
            style = new Style(
                {
                    image: new Chart(
                        {
                            type: "bar",
                            radius: (sel ? 1.2 : 1) * radius,
                            offsetY: 5,
                            offsetX: -40,
                            data: data,
                            colors: /,/.test(c) ? c.split(",") : c,
                            rotateWithView: true,
                            animation: true,
                            stroke: new Stroke(
                                {
                                    color: "#000",
                                    width: 1
                                }),
                        })
                });
            this.styleCache[k] = style;
        }
        return style;
    }

    getProvinceFeatureStyle(feature) {

        let style = new Style({
            image: new CircleStyle({
                radius: 50,
                fill: new Fill({
                    color: 'rgba(255, 153, 0, 0.4)'
                }),
                stroke: new Stroke({
                    color: 'rgba(255, 204, 0, 0.2)',
                    width: 1
                })
            })
        });



        return style;
    }

    loadLayers() {
        // const { map } = this.state

        // let wmsUrl = `${process.env.GEOSERVER_URL}/geoserver/behtab/wms`

        // const { geoserverUserName, geoserverPassword, geoserverDataStore, geoserverWorkspase } = Strings;

        // const geoserverCoverageRestUrl = `${process.env.GEOSERVER_URL}/geoserver/rest/workspaces/${geoserverWorkspase}/datastores/${geoserverDataStore}/featuretypes.json`
        // const geoserverFeatureTypesRestUrl = `${process.env.GEOSERVER_URL}/geoserver/rest/workspaces/${geoserverWorkspase}/coveragestores.json`

        // const userPass = Base64.toBase64(geoserverUserName + ':' + geoserverPassword)

        // console.log(`YWRtaW46Z2Vvc2VydmVy =>${userPass}`)

        // fetch(geoserverFeatureTypesRestUrl, {
        //     method: "GET", 
        //     withCredentials: true,
        //     credentials: "same-origin",
        //     mode: "cors",
        //     headers: {
        //         "content-type": "application/json",
        //         "Authorization": 'Basic ' + userPass
        //     },
        //     redirect: "follow", // manual, *follow, error
        //     referrer: "no-referrer",
        // }).then(response => response.json()).then(result => {
        //     console.log(result)
        // }).catch(error => this.props.enqueueSnackbar(error.toString(), {
        //     variant: 'error',
        // }))

    }
    handleMapClick(event) {

        let { isShowingPanel } = this.state

        if (isShowingPanel) {

            let newFeature = this.state.markerFeature

            newFeature.getGeometry().setCoordinates(event.coordinate)

            this.setState({
                markerFeature: newFeature
            })
        }

    }

    getMapCenter() {

        let { width, height } = this.state

        const pixel = Array.of(width / 2, height / 2)

        const geom = this.state.map.getCoordinateFromPixel(pixel)

        return geom
    }

    // pass new features from props into the OpenLayers layer object
    componentDidUpdate(prevProps, prevState) {

        let markerFeatures = Array.of()

        let { isShowingPanel, markerFeature } = this.state


        if (isShowingPanel) {

            if (markerFeature === undefined) {

                const geom = this.getMapCenter()

                // let transformCooridinates = transform(geom, this.state.map.getView().getProjection().getCode(), 'EPSG:4326')

                let markerGeom = new Point([geom[0], geom[1]])

                markerFeature = new Feature({
                    geometry: markerGeom
                });

                this.state.markerFeature = markerFeature
            }
            let newHospitalStyle = new Style({
                image: new Icon(/** @type {module:ol/style/Icon~Options} */({
                    anchor: [0.5, 30],
                    anchorXUnits: 'fraction',
                    anchorYUnits: 'pixels',
                    src: hospital
                }))
            });
            markerFeature.setStyle(newHospitalStyle)

            markerFeatures.push(markerFeature)
        }

        this.state.MarkerlLayer.setSource(
            new VectorSource({
                features: markerFeatures
            })
        );
    }

    closeAttachmentsDialog() {
        this.setState({
            ShowAttachments: false
        })
    }
    onFabAddHospitalClicked(e) {
        e.preventDefault()
        this.setState({
            isShowingPanel: true,
            markerFeature: undefined,
            edititngHospital: undefined,
        })
    }
    onHospitalEditClicked(hospital) {
        let markerFeature = undefined

        if (hospital.centerPoint !== null) {

            const coordinates = transform(hospital.centerPoint.coordinates, 'EPSG:4326', this.state.map.getView().getProjection().getCode())

            markerFeature = new Feature({
                geometry: new Point(coordinates)
            })
        }
        this.setState({
            isShowingPanel: true,
            edititngHospital: hospital,
            markerFeature: markerFeature,
        })
    }
    onPanelClosed() {
        this.setState({
            isShowingPanel: false,
            markerFeature: undefined,
            edititngHospital: undefined
        })
    }

    toggleTocDialog() {
        this.setState({
            showToc: !this.state.showToc
        })
    }


    onMarkerFeatureLocationChanged(coordinates) {

        let marker = this.state.markerFeature

        if (coordinates.length == 0) {
            marker.setGeometry(new Point(Array.of(0, 0)))
            this.setState({
                markerFeature: marker
            })
        } else {
            let transformCooridinates = transform(coordinates, 'EPSG:4326', this.state.map.getView().getProjection().getCode())
            marker.setGeometry(new Point(transformCooridinates))

            this.state.map.getView().setCenter(transformCooridinates)

            this.setState({
                markerFeature: marker
            })
        }
    }




    render() {

        const { classes } = this.props

        const { isShowingPanel, markerFeature, edititngHospital, layers, showToc, IdentifiedFeaturePhotos, IdentifiedFeatureId, ShowAttachments, GenerateRvaReport, IdentifiedFeatureName, RedirectToHospitalPage } = this.state

        // if (RedirectToHospitalPage) {
        //     return <Redirect 
        //         to={{
        //             pathname: "/",
        //             search: "",
        //             state: { ViewType: ListItems.Hospitals, hospitalName: IdentifiedFeatureName }
        //         }}
        //     />
        // }

        let bottomSheet = null
        let panel = null

        if (isShowingPanel) {

            const mapCenter = this.getMapCenter()

            let transformCooridinates = transform(markerFeature === undefined ? mapCenter : markerFeature.getGeometry().getCoordinates(), this.state.map.getView().getProjection().getCode(), 'EPSG:4326')

            panel = <div><HospitalEdit hospital={edititngHospital} onPanelClosed={this.onPanelClosed} location={transformCooridinates} onLocationChanged={this.onMarkerFeatureLocationChanged} />  </div>
        }

        let panelToc = showToc ? <Toc layers={layers} onPanelClosed={this.toggleTocDialog}></Toc> : null


        return (
            <div id='mapContainer' className='mapContainer' ref="mapContainer">

                {bottomSheet}

                <Tooltip title={<Translate id="addHospital" />} aria-label="add">
                    <Fab size="small" color="secondary" aria-label="Add" onClick={(e) => this.onFabAddHospitalClicked(e)} className={classes.addbtn}>
                        <AddIcon />
                    </Fab>
                </Tooltip>
                <Tooltip title={<Translate id="Layers" />} aria-label="add">
                    <Fab size="small" color="secondary" aria-label="Add" onClick={(e) => this.toggleTocDialog()} className={classes.tocBtn}>
                        <LayersIcon />
                    </Fab>
                </Tooltip>

                {this.props.children}

                <div id="popup" class="ol-popup" ref="popup">

                    <a href="#" id="popup-closer" class="ol-popup-closer" ref="popup_closer"></a>

                    {
                        <div  >
                            <div id="popup-content" ref="popContent">  </div>
                            <div ref="popContent_bottom_content">
                                {
                                    <Image ref="popContent_bottom_photo" src={IdentifiedFeaturePhotos.length > 0 ? `${process.env.BASE_URL}/downloadFile/${IdentifiedFeaturePhotos[0].fileName}` : hospital_local} />
                                }
                                <IconButton onclick={this.onBtnAttachmentClicked} className={classes.button} ref="popContent_btn_attachment">
                                    <Tooltip title={<Translate id="AttachmentManage" />} aria-label="Dashboard">
                                        <AttachmentIcon />
                                    </Tooltip>
                                </IconButton>
                                <IconButton onclick={this.onBtnRvaReportClicked} className={classes.button} ref="popContent_btn_rva" >
                                    <Tooltip title={<Translate id="ExportHospitalAnalyticalReport" />} aria-label="Dashboard">
                                        <BarChart />
                                    </Tooltip>
                                </IconButton>
                                {/* <IconButton onclick={this.onBtnRedirectToHospitalsPage} className={classes.button} ref="popContent_btn_redirect_to_hospital_page" >
                                    <Tooltip title={<Translate id="Dashboard" />} aria-label="Dashboard">
                                        <MapIcon />
                                    </Tooltip>
                                </IconButton> */}
                            </div>
                        </div>
                    }
                </div>
                {panelToc}
                {panel}
                {
                    ShowAttachments ? <AttachmentsDialog attachmentType="HospitalDocs" entityId={IdentifiedFeatureId} entityName={IdentifiedFeatureName} onDialogClosed={this.closeAttachmentsDialog} ></AttachmentsDialog> : null
                }
                {
                    GenerateRvaReport ? <ProgressDialog title={<Translate id="GeneratigRvaReport"></Translate>}></ProgressDialog> : null
                }
            </div >
        );
    }

}
export default withStyles(styles, { withTheme: true })(withLocalize(withSnackbar(MapView)));
