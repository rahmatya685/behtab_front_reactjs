import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import MenuIcon from '@material-ui/icons/Menu';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import IconButton from '@material-ui/core/IconButton';
import { Translate, withLocalize } from 'react-localize-redux';
import LanguageToggle from '../components/LanguageToggle'
import classNames from 'classnames';

const drawerWidth = 240;

const styles = theme => ({

    menuButton: {
        marginLeft: -12,
        marginRight: 20,
    },
    menuButtonHidden: {
        display: 'none',
    },
    title: {
        flexGrow: 1,
    },
    menuButton: {
        marignLeft: -12,
        marginRight: 20,
    },
    appBar: {
        zIndex: theme.zIndex.drawer + 1,
        transition: theme.transitions.create(['width', 'margin'], {
            easing: theme.transitions.easing.sharp,
            duration: theme.transitions.duration.leavingScreen,
        }),
    },
    appBarShift: {
        marginLeft: drawerWidth,
        width: `calc(100% - ${drawerWidth}px)`,
        transition: theme.transitions.create(['width', 'margin'], {
            easing: theme.transitions.easing.sharp,
            duration: theme.transitions.duration.enteringScreen,
        }),
    },
    toolbar: {
        paddingRight: 24, // keep right padding when drawer closed
    }

});


class AppMenuBar extends Component {


    render() {

        const { classes, drawerIsOpen, handleDrawerOpen, logout } = this.props

        return (
            <AppBar position="absolute" className={classNames(classes.appBar, drawerIsOpen && classes.appBarShift)}>
                <Toolbar disableGutters={!drawerIsOpen} className={classes.toolbar}>
                    <IconButton color="inherit" aria-label="Open drawer"
                        className={classNames(
                            classes.menuButton, drawerIsOpen && classes.menuButtonHidden,
                        )}
                        onClick={handleDrawerOpen}
                    >
                    <MenuIcon />
                    </IconButton>
                    <Typography
                        component="h1"
                        variant="h6"
                        color="inherit"
                        noWrap
                        className={classes.title}>
                        <Translate id="AppName" />
                    </Typography>
                    <LanguageToggle selectColor="secondary"
                        unSelectColor="inherit" />
                    <Button color="inherit" onClick={logout}><Translate id="Logout" /></Button>

                </Toolbar>
            </AppBar>
        )

    }
}


export default withStyles(styles)(withLocalize(AppMenuBar));