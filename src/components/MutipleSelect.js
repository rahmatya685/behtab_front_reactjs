import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import { withStyles } from '@material-ui/core/styles';
import Input from '@material-ui/core/Input';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import Chip from '@material-ui/core/Chip';
import { Translate } from 'react-localize-redux';


const styles = theme => ({
    root: {
        display: 'flex',
        flexWrap: 'wrap',
        width: '100%',
    },
    formControl: {
        margin: theme.spacing.unit,
        minWidth: 120,
        maxWidth: '100%',
        width: '100%',
    },
    chips: {
        width: '100%',
        display: 'flex',
        flexWrap: 'wrap',
    },
    chip: {
        margin: theme.spacing.unit / 4,
    },
    noLabel: {
        marginTop: theme.spacing.unit * 3,
    }
});

const ITEM_HEIGHT = 48;
const ITEM_PADDING_TOP = 8;
const MenuProps = {
    PaperProps: {
        style: {
            maxHeight: ITEM_HEIGHT * 4.5 + ITEM_PADDING_TOP,
            width: 250,
        },
    },
};


function getStyles(name, that) {
    return {
        fontWeight:
            that.state.name.indexOf(name) === -1
                ? that.props.theme.typography.fontWeightRegular
                : that.props.theme.typography.fontWeightMedium,
    };
}

class MultipleSelect extends React.Component {



    constructor(props) {
        super(props)

        this.handleChange = this.handleChange.bind(this)
    }


    handleChange = event => {

        this.props.onChange(event.target.value)
        //this.props.handleChange({ name: event.target.value })
    };

    render() {
        const { classes, selectedValues } = this.props;

        return (
            <div className={classes.root}>
                <FormControl className={classes.formControl}>
                    <InputLabel htmlFor="select-multiple-chip"> <Translate id={this.props.labelId} /> </InputLabel>
                    <Select
                        multiple
                        value={selectedValues}
                        onChange={this.handleChange}
                        input={<Input id="select-multiple-chip" />}
                        renderValue={selected => (
                            <div className={classes.chips}>
                                {selected.map(value => (
                                    <Chip key={value.id} label={value.name} className={classes.chip} />
                                ))}
                            </div>
                        )}
                        MenuProps={MenuProps}  >
                        {this.props.data.map(item => (
                            <MenuItem key={item.id} value={item}  >
                                {item.name}
                            </MenuItem>
                        ))}
                    </Select>
                </FormControl>
            </div>
        );
    }
}

MultipleSelect.propTypes = {
    classes: PropTypes.object.isRequired,
    data: PropTypes.array.isRequired,
    onChange: PropTypes.func.isRequired
};

export default withStyles(styles, { withTheme: true })(MultipleSelect);
