import React, { Component } from 'react'
import jsPanel from 'jspanel4/dist/jspanel';
import 'jspanel4/es6module/jspanel.css'
import { withLocalize, Translate } from "react-localize-redux";




class Panel extends Component {
    constructor(props) {
        super(props)
        this.state = {
            width: 0, height: 0,
            panel: undefined
        };
        this.updateWindowDimensions = this.updateWindowDimensions.bind(this);
    }

    updateWindowDimensions() {
        this.setState({ width: window.innerWidth, height: window.innerHeight });
    }
    componentWillUnmount() {
        window.removeEventListener('resize', this.updateWindowDimensions);

    }

    componentDidUpdate(prevProps, prevState) {
        const { panel } = this.state
        if (this.props.closePanel && panel != undefined) {
            panel.close()
        }
    }

    componentDidMount() {

        // console.log( `${this.state.height -160}px`)

        this.updateWindowDimensions();

        window.addEventListener('resize', this.updateWindowDimensions);

        const panel = jsPanel.create({
            id: this.props.titleId,
            theme: 'primary',
            borderRadius: '8px',
            headerTitle: this.props.titleId,
            position: this.props.position,
            contentSize: {
                width: "400px",
                height: `400px`
            },
            contentOverflow: 'hidden scroll', syncMargins: true,
            maximizedMargin: 50,
            headerControls: {
                maximize: "remove",
            },
            content: this.refs.content,
            callback: () => { 
                // this.content.style.padding = '10px';

            },
            onclosed: () => {
                this.props.onPanelClosed()
            }
        });

        this.setState({
            panel: panel
        })
    }
    render() {
        return (
            <div ref="content">{this.props.children}</div>
        )
    }
}

export default withLocalize(Panel);