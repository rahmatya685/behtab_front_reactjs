import React from 'react'
import ReactDom from 'react-dom'


export class SkiDayCount extends React.Component {
    percentageToDecimal(decimal) {
        return ((decimal * 100) + '%')
    }
    calcGoalProgress(total, goal) {
        return this.percentageToDecimal(total / goal)
    }
    render() {
        return (
            <div className="ski-day-count">
                <div className="total-days">
                    <span>{this.props.total}</span>
                    <span>days</span>
                </div>
                <div className="powder-days">
                    <span>{this.props.powder}</span>
                    <span>Days</span>
                </div>
                <div className="backcountry-days">
                    <span>{this.props.backcountry}</span>
                    <span>  hiking day</span>
                </div>
                <div>
                    {this.calcGoalProgress(
                        this.props.total,
                        this.props.goal
                    )}
                </div>

            </div>
        )
    }
} 