import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import Panel from "../components/Panel";
import TocItem from "../components/TocItem"; 
import Cookies from 'universal-cookie';
import { instanceOf } from 'prop-types';
import InputAdornment from '@material-ui/core/InputAdornment';
import Button from '@material-ui/core/Button';
import LocationOn from '@material-ui/icons/LocationOn';
import Select from '@material-ui/core/Select';
import { Translate, withLocalize } from 'react-localize-redux';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import { withSnackbar } from 'notistack';
import ImageWMS from "ol/source/ImageWMS";
import List from '@material-ui/core/List';
import PropTypes from 'prop-types';
import Avatar from '@material-ui/core/Avatar';
import Grid from '@material-ui/core/Grid';
import Divider from '@material-ui/core/Divider';
import ListItem from '@material-ui/core/ListItem';
import ListItemSecondaryAction from '@material-ui/core/ListItemSecondaryAction';
import ListItemText from '@material-ui/core/ListItemText';
import ListItemAvatar from '@material-ui/core/ListItemAvatar';

const styles = theme => ({
    container: {
        display: 'flex',
        flexWrap: 'wrap',
    },
    root: {
        width: '100%',
        maxWidth: 360,
        backgroundColor: theme.palette.background.paper,
    },
    textField: {
        marginLeft: theme.spacing.unit,
        marginRight: theme.spacing.unit,
        marginTop: theme.spacing.unit * 2,
        width: '100%',
    },
    closeButton: {
        position: 'absolute',
        right: theme.spacing.unit,
        top: theme.spacing.unit,
        color: theme.palette.grey[500],
    },
    avatar: {
        margin: 20,
        width: 60
    },

    STRUCTURAL_MODULE: {
        margin: 10,
        color: "#fff",
        backgroundColor: "#ff0",
    },
    NON_STRUCTURAL_MODULE: {
        margin: 10,
        color: "#fff",
        backgroundColor: "#0ff",
    },
    INSTUTIONAL_MODULE: {
        margin: 10,
        color: '#fff',
        backgroundColor: "#0f0",
    },
    RiskIndex: {
        margin: 10,
        color: '#fff',
        backgroundColor: "#f00",
    },
});



class Toc extends React.Component {

    constructor(props) {
        super(props)

    }
    state = {
        closePanel: false
    }



    render() {
        
        const { classes, onPanelClosed, layers } = this.props;

        const { closePanel } = this.state

        let panelTitleId = "Layers"

        const position = {
            my: "left-top",
            at: "left-top",
            offsetX: 90,
            offsetY: 90
        }
        const proviceChartLegendItems = [
            'NON_STRUCTURAL_MODULE', 'STRUCTURAL_MODULE', 'INSTUTIONAL_MODULE', 'RiskIndex'
        ]


        return (
            <div>
                <Panel titleId={panelTitleId} position={position} onPanelClosed={onPanelClosed} closePanel={closePanel}>
                    <List dense className={classes.root}>
                        {
                            layers.map(layer => (
                                <TocItem key={layer.getSource().getParams().LAYERS} layer={layer} />
                            ))

                        }
                    </List>
                    <Divider component="li" />
                    <InputLabel>Chart Colors</InputLabel>
                    <List dense className={classes.root}>
                        {
                            proviceChartLegendItems.map(item => (

                                <ListItem key={item} button>
                                    <ListItemAvatar>
                                        <Avatar
                                            className={classes[item]}
                                        />
                                    </ListItemAvatar>
                                    <ListItemText primary={<Translate id={item} />} />
                                </ListItem>

                            ))

                        }
                    </List>
                
                </Panel>
            </div>
        )
    }
}
Toc.propTypes = {
    classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(withLocalize(withSnackbar(Toc)))
