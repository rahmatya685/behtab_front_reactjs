import React from 'react';
import { Translate, withLocalize } from 'react-localize-redux';
import { withSnackbar } from 'notistack';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import ListItem from '@material-ui/core/ListItem';
import ListItemSecondaryAction from '@material-ui/core/ListItemSecondaryAction';
import ListItemText from '@material-ui/core/ListItemText';
import ListItemAvatar from '@material-ui/core/ListItemAvatar';
import Checkbox from '@material-ui/core/Checkbox';
import Avatar from '@material-ui/core/Avatar';
import hospital from '../images/hospital.svg'


const styles = theme => ({
    root: {
        width: '100%',
        maxWidth: 360,
        backgroundColor: theme.palette.background.paper,
    },
});



class TocItem extends React.Component {

    constructor(props) {
        super(props)

        this.state = {
            layerVisibiliy: this.props.layer.getVisible(),
            image: undefined
        }


    }

    componentDidMount() {
        const { layer } = this.props;

        const url = `${process.env.GEOSERVER_URL}/geoserver/wms?REQUEST=GetLegendGraphic&VERSION=1.0.0&FORMAT=image/png&WIDTH=40&HEIGHT=40&LAYER=${layer.getSource().getParams().LAYERS}`

        var options = {
            method: 'GET',
            mode: 'cors',
            cache: 'default'
        };

        var request = new Request(url);

        fetch(request, options).then((response) => {
            response.arrayBuffer().then((buffer) => {
                var base64Flag = 'data:image/jpeg;base64,';
                var imageStr = this.arrayBufferToBase64(buffer);

                this.setState({
                    image: base64Flag + imageStr
                })

            });
        });
    }

    arrayBufferToBase64(buffer) {
        var binary = '';
        var bytes = [].slice.call(new Uint8Array(buffer));

        bytes.forEach((b) => binary += String.fromCharCode(b));

        return window.btoa(binary);
    }

    handleLayerVisbility() {

        const { layer } = this.props;

        layer.setVisible(!layer.getVisible())

        this.setState({
            layerVisibiliy: layer.getVisible()
        })

    }

    render() {
        const { classes, layer } = this.props;

        const { layerVisibiliy, image } = this.state

        const layerNames = layer.getSource().getParams().LAYERS

        return (
            <div>
                <ListItem key={layerNames} button>
                    <ListItemAvatar>
                        <Avatar
                            alt={layerNames}
                            src={image}
                        />
                    </ListItemAvatar>
                    <ListItemText primary={<Translate id={layerNames} />} />
                    <ListItemSecondaryAction>
                        <Checkbox id={layerNames}
                            onChange={() => this.handleLayerVisbility()}
                            checked={layerVisibiliy}
                        />
                    </ListItemSecondaryAction>
                </ListItem>
            </div>
        )
    }
}

TocItem.propTypes = {
    classes: PropTypes.object.isRequired,
};
export default withStyles(styles)(withLocalize(withSnackbar(TocItem)))
