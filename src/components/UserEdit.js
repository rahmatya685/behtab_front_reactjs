import React, { Component } from 'react';
import { Translate, withLocalize } from 'react-localize-redux';
import globalTranslations from "../translations/global.json";
 import { withStyles } from '@material-ui/core/styles';
import PropTypes from 'prop-types';
import Cookies from 'universal-cookie';
import CustomizedDialog from './Dialog.js';
import { Grid } from '@material-ui/core';
import MultipleSelect from './MutipleSelect'
import TextField from '@material-ui/core/TextField';
import { withSnackbar } from 'notistack';



const styles = theme => ({
    root: {
        flexGrow: 1,
    },
});



class User {
    constructor() {
        this.name = "";
        this.username = "";
        this.authorities = [];
        this.password = "";
    }
}

class UserEdit extends Component {

    constructor(props) {
        super(props)

        this.state = {
            user: this.props.user === null ? new User() : JSON.parse(JSON.stringify(this.props.user))   ,
            passwordReapet: this.props.user === null ? "" : this.props.user.password
        }

        this.handleOkClicked = this.handleOkClicked.bind(this);
        this.handleChange = this.handleChange.bind(this);
        this.handleRoleChange = this.handleRoleChange.bind(this)
        this.getToken = this.getToken.bind(this)
        this.onDialogClosed = this.onDialogClosed.bind(this)
    }

    onDialogClosed(user) {
        this.props.onDialogClosed(user)
    }



    getToken() {
        const cookies = new Cookies();
        return cookies.get('token') || ''
    }


    handleChange = name => event => {
        const val = event.target.value
        const { user } = this.state
        if (name !== 'passwordReapet') {
            user[name] = val
            this.setState((state, props) => ({
                user: user
            }));
        } else {
            this.setState((state, props) => ({
                passwordReapet: val
            }));
        }

    };

    handleRoleChange(roles) {

        let changedUser = this.state.user

        changedUser.authorities = roles.map(value => value)

        this.setState({ user: changedUser });

    }
    handleOkClicked() {

        const { user } = this.state

        if (user.authorities[0].id !== undefined) {


            user.authorities = user.authorities.map(item => {
                return { 'authority': `${item.id}` }
            })
        }

        try {
            fetch(`${process.env.BASE_URL}/${this.props.user === undefined ? 'users/register' : 'protected/users/update'}`, {
                method: "POST", // *GET, POST, PUT, DELETE, etc.   
                headers: {
                    "Content-Type": "application/json",
                    "Authorization": this.getToken()
                },
                body: JSON.stringify(user)
            })
                .then(data => {
                    if (data.ok) {
                        data.json().then((resp) => {
                            if (resp === "User not found") {
                                this.props.enqueueSnackbar(<Translate id="UserNotfound" />, {
                                    variant: 'error',
                                });
                            } else {
                                this.props.enqueueSnackbar(<Translate id="UserEditSuccess" />, {
                                    variant: 'success',
                                });
                                this.onDialogClosed(user)
                            }
                        });
                    } else {
                        if (data.status === 500) {
                            this.props.enqueueSnackbar(<Translate id="ServerError" />, {
                                variant: 'error',
                            });
                        } else if (data.status === 401) {
                            this.props.enqueueSnackbar(<Translate id="UserValidationError" />, {
                                variant: 'error',
                            });
                        } else {
                            this.props.enqueueSnackbar(data.toString(), {
                                variant: 'error',
                            });
                        }
                    }
                })
                .catch(error => this.props.enqueueSnackbar(error.toString(), {
                    variant: 'error'
                }));
        } catch (e) {
            this.props.enqueueSnackbar(e.toString(), {
                variant: 'error'
            })
        }
    }

    validateForm() {

        const { user, passwordReapet } = this.state

        let formIsValid = user.password !== undefined && user.password.length > 0 && passwordReapet !== undefined && passwordReapet.length > 0 && user.password === passwordReapet

        Object.entries(user).forEach((key, value) => {
            switch (typeof (value)) {
                case 'string':
                    if (!(value !== undefined && value !== null && value.length != 0))
                        formIsValid = false
                    break;
            }
        })

        if (user.authorities === undefined) {
            formIsValid = false
        } else if (user.authorities.length == 0)
            formIsValid = false

        return formIsValid
    }

    render() {

        const { classes, roles } = this.props

        let { name, username, password, authorities } = this.state.user

        const { passwordReapet } = this.state



        password = (password === undefined || password === null) ? "" : password

        let userRolesOld = Array.of()

        let mappingIsDone = true

        if (authorities !== undefined) {
            authorities.forEach(item => {
                roles.forEach(role => {
                    /*this is because authorities field have two form, one with authority and another with id and name 
                    the second form is for displaying proper values in app and is made inside app but first form is comming from server
                    */
                    if (item.authority !== undefined) {
                        if (`${role.id}` == item.authority) {
                            userRolesOld.push(role)
                        }
                    } else {
                        mappingIsDone = false
                    }
                })
            })
        }
        if (!mappingIsDone) {
            userRolesOld = authorities;
        }


        return (
            <div>
                <CustomizedDialog saveBtnEnable={!this.validateForm()} titleId="addUser" okBtnTitleId="save" onDialogClosed={this.onDialogClosed} handleOkClicked={this.handleOkClicked}>
                    <form className={classes.root}   >
                        <Grid container spacing={24}>
                            <Grid item xs={6}>
                                <TextField
                                    id="FirstLastName"
                                    label={<Translate id="FirstLastName" />}
                                    className={classes.textField}
                                    value={name}
                                    onChange={this.handleChange('name')}
                                    margin="normal"
                                    required
                                />
                            </Grid>
                            <Grid item xs={6}>
                                <TextField
                                    id="UserName"
                                    label={<Translate id="UserName" />}
                                    className={classes.textField}
                                    value={username}
                                    onChange={this.handleChange('username')}
                                    margin="normal"
                                    required
                                />
                            </Grid>

                            <Grid item xs={6}>
                                <TextField
                                    id="Password"
                                    label={<Translate id="Password" />}
                                    className={classes.textField}
                                    value={password}
                                    type="password"
                                    onChange={this.handleChange('password')}
                                    margin="normal"
                                    required
                                />
                            </Grid>
                            <Grid item xs={6}>
                                <TextField
                                    id="RepeatPassword"
                                    label={<Translate id="RepeatPassword" />}
                                    className={classes.textField}
                                    onChange={this.handleChange('passwordReapet')}
                                    margin="normal"
                                    type="password"
                                    value={passwordReapet}
                                    required
                                />
                            </Grid>

                            <Grid item xs={12}>
                                <MultipleSelect data={roles} onChange={this.handleRoleChange} selectedValues={authorities === undefined ? Array.of() : userRolesOld} labelId="UserRole" />
                            </Grid>
                        </Grid>
                    </form>
                </CustomizedDialog>
            </div>
        )
    }
}

UserEdit.propTypes = {
    classes: PropTypes.object.isRequired,
    theme: PropTypes.object.isRequired,
};
export default withStyles(styles, { withTheme: true })(withLocalize(withSnackbar(UserEdit)));