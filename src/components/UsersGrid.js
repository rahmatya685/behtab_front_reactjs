import React, { Component } from 'react';
import MaterialTable from 'material-table'
import { Translate, withLocalize } from 'react-localize-redux';
import globalTranslations from "../translations/global.json";
import { withStyles } from '@material-ui/core/styles';
import PropTypes from 'prop-types';
import Cookies from 'universal-cookie';
import Fab from '@material-ui/core/Fab'
import AddIcon from '@material-ui/icons/Add'
import UserEdit from './UserEdit'
import classNames from 'classnames';
import { withSnackbar } from 'notistack';
import gridLocalization from "../components/GridLocalization"
import Tooltip from '@material-ui/core/Tooltip';


const styles = theme => ({
    addbtn: {
        position: 'absolute',
        top: theme.spacing.unit * 10,
        right: theme.spacing.unit * 2,
        zIndex: 20
    },
    extendedIcon: {
        marginRight: theme.spacing.unit,
    },
});

class UsersGrid extends Component {
    constructor(props) {
        super(props)
        this.props.addTranslation(globalTranslations);

        this.state = {
            height: 0, showUserEditDialog: false, editingUser: null, roles: Array.of(), isLoading: false
        };

        this.updateWindowDimensions = this.updateWindowDimensions.bind(this);
        this.onUserEditDialogClosed = this.onUserEditDialogClosed.bind(this);
        this.showUserEditDialog = this.showUserEditDialog.bind(this);
        this.getRoles = this.getRoles.bind(this);
        this.getToken = this.getToken.bind(this);
        this.query = undefined;
        this.tableRef = undefined

    }

    onUserEditDialogClosed(editedUser) {

        const { editingUser } = this.state

        if (editingUser != null) {
            if (editedUser !== undefined)
                Object.entries(editedUser).forEach(item => {
                    editingUser[item[0]] = item[1]
                })
        }

        this.setState({
            showUserEditDialog: false,
            editingUser: null
        })
        this.tableRef.onQueryChange(this.query)

    }

    getToken() {
        const cookies = new Cookies();
        return cookies.get('token') || ''
    }

    componentWillMount() {
        this.getRoles()
    }

    getRoles() {

        const { activeLanguage } = this.props

        try {
            fetch(`${process.env.BASE_URL}/protected/users/roles`, {
                method: "POST",
                headers: {
                    "Content-Type": "application/json",
                    "Authorization": this.getToken()
                },
            }).then(data => {

                if (data.ok) {

                    data.json().then((resp) => {

                        if (resp === "User not found") {
                            this.props.enqueueSnackbar(<Translate id="UserNotfound" />, {
                                variant: 'error',
                            });

                        } else {
                            let roles = Array.of()
                            resp.map(item => {
                                if (activeLanguage.code === "fa")
                                    roles.push({ "id": item.type, "name": item.typeNameFa })
                                else
                                    roles.push({ "id": item.type, "name": item.typeNameEn })
                            })

                            this.setState({
                                roles: roles
                            })
                        }
                    });

                } else {
                    if (data.status === 500) {
                        this.props.enqueueSnackbar(<Translate id="ServerError" />, {
                            variant: 'error',
                        });
                    } else if (data.status === 401) {
                        this.props.enqueueSnackbar(<Translate id="UserValidationError" />, {
                            variant: 'error',
                        });
                    } else {
                        this.props.enqueueSnackbar(data.toString(), {
                            variant: 'error',
                        });
                    }
                }

            })
                .catch(error => this.props.enqueueSnackbar(error.toString(), {
                    variant: 'error'
                }));
        } catch (e) {
            this.props.enqueueSnackbar(e.toString(), {
                variant: 'error'
            })
        }
    }


    showUserEditDialog(user) {
        this.setState({
            showUserEditDialog: true,
            editingUser: user
        })
    }


    componentDidMount() {

        this.updateWindowDimensions();
        window.addEventListener('resize', this.updateWindowDimensions);

    }

    componentWillUnmount() {
        window.removeEventListener('resize', this.updateWindowDimensions);
    }

    updateWindowDimensions() {
        this.setState({ height: window.innerHeight });
    }

    getData = (query, resolve, reject) => {

        this.query = query

        let url = `${process.env.BASE_URL}/protected/users?`
        url += 'size=' + query.pageSize
        url += '&page=' + (query.page)

        fetch(url, {
            method: "POST",
            mode: "cors",
            headers: {
                "Authorization": this.getToken(),
                "content-type": "application/json",
            }
        }).then(response => response.clone().json())
            .then(result => {
                if (result.message !== undefined && result.message === "Action not allowed") {
                    this.props.enqueueSnackbar(<Translate id="ActionNotAllowed" />, {
                        variant: 'error',
                    });
                    reject()
                    this.tableRef.setState({
                        isLoading: false
                    })
                } else {
                    if (result.content.length === 0) {
                        if (result.totalElements !== 0) {
                            query.page = query.page - 1
                            this.getData(query, resolve, reject)
                        } else {
                            resolve({
                                data: result.content,
                                page: result.number,
                                totalCount: result.totalElements,
                            })
                        }

                    } else {
                        resolve({
                            data: result.content,
                            page: result.number,
                            totalCount: result.totalElements,
                        })
                    }
                }
            }).catch(error => this.props.enqueueSnackbar(error.toString(), {
                variant: 'error',
            }));
    }
    render() {

        const { classes, theme } = this.props;

        const { showUserEditDialog, editingUser, roles, page } = this.state

        let name = (<Translate id="FirstLastName" options={{ renderInnerHtml: false }} />)
        const userName = (<Translate id="UserName" options={{ renderInnerHtml: false }} />)

        return (
            <div>

                <MaterialTable ref="table"
                    style={
                        { fontFamily: '"Vazir", sans-serif' }
                    }
                    columns={[
                        {
                            title: name, field: 'name', searchable: true,
                            headerStyle: {
                                fontFamily: '"Vazir", sans-serif'
                            },
                            cellStyle: {
                                fontFamily: '"Vazir", sans-serif'
                            }
                        },
                        { title: userName, field: 'username', searchable: true },
                    ]}
                    data={query => new Promise((resolve, reject) => {
                        this.getData(query, resolve, reject)
                    })
                    }
                    title={<Translate id="Users"></Translate>}
                    options={{
                        paging: true,
                        maxBodyHeight: this.state.height - 250,
                        toolbar: true,
                        actionsColumnIndex: -1,
                        showTitle: true,
                        search: false,
                        filtering: false,
                        isLoading: showUserEditDialog,
                        headerStyle: {
                            fontFamily: '"Vazir", sans-serif'
                        },
                        rowStyle: {
                            fontFamily: '"Vazir", sans-serif'
                        },
                        searchFieldStyle: {
                            fontFamily: '"Vazir", sans-serif'
                        }
                    }}
                    actions={[
                        {
                            icon: 'edit',
                            tooltip: <Translate id="EditAction" />,
                            onClick: (event, data) => {
                                this.showUserEditDialog(data)
                            }
                        }
                    ]} 
                    tableRef={table => this.tableRef = table}
                    editable={{
                        onRowDelete: oldData =>
                            new Promise((resolve, reject) => {
                                let url = `${process.env.BASE_URL}/protected/users/delete`
                                fetch(url, {
                                    method: "POST",
                                    headers: {
                                        "Authorization": this.getToken(),
                                        "content-type": "application/json",
                                    },
                                    body: JSON.stringify(oldData)
                                })
                                    .then(response => {
                                        if (response.ok) {
                                            response.json().then((resp) => {

                                                if (resp === true) {
                                                    this.props.enqueueSnackbar(<Translate id="UserDeleteSuccess" />, {
                                                        variant: 'success',
                                                    });
                                                    resolve()
                                                } else {
                                                    if (resp === false) {
                                                        this.props.enqueueSnackbar(<Translate id="ErrorProccessingAction" />, {
                                                            variant: 'error',
                                                        });
                                                    } else if (resp.localizedMessage === "User not found") {
                                                        this.props.enqueueSnackbar(<Translate id="UserNotfound" />, {
                                                            variant: 'error',
                                                        });
                                                    } else if (resp.localizedMessage === "DELETING_USER_IS_ALLOWED_4_ADMIN") {
                                                        this.props.enqueueSnackbar(<Translate id="DELETING_USER_IS_ALLOWED_4_ADMIN" />, {
                                                            variant: 'error',
                                                        });
                                                    } else {
                                                        this.props.enqueueSnackbar(resp.toString(), {
                                                            variant: 'error',
                                                        });
                                                    }
                                                    reject()
                                                }
                                            });
                                        } else {
                                            if (response.status === 500) {
                                                this.props.enqueueSnackbar(<Translate id="ServerError" />, {
                                                    variant: 'error',
                                                });
                                            } else if (response.status === 401) {
                                                this.props.enqueueSnackbar(<Translate id="UserValidationError" />, {
                                                    variant: 'error',
                                                });
                                            } else {
                                                this.props.enqueueSnackbar(response.toString(), {
                                                    variant: 'error',
                                                });
                                            }
                                            reject()
                                        }

                                    })
                                    .catch(error => {
                                        reject()
                                        this.props.enqueueSnackbar(error.toString(), {
                                            variant: 'error',

                                        })
                                    });
                            })

                    }}
                    localization={gridLocalization}

                />
                <Tooltip title={<Translate id="addUser" />} aria-label="add">
                    <Fab size="small" color="secondary" aria-label="Add" className={classNames(classes.addbtn)} onClick={() => this.showUserEditDialog(null)} >
                        <AddIcon />
                    </Fab>
                </Tooltip>
                {
                    showUserEditDialog ? <UserEdit user={editingUser} roles={roles} onDialogClosed={this.onUserEditDialogClosed}></UserEdit> : null
                }

            </div>

        )
    }
}



UsersGrid.propTypes = {
    classes: PropTypes.object.isRequired,
    theme: PropTypes.object.isRequired,
};
export default withStyles(styles, { withTheme: true })(withLocalize(withSnackbar(UsersGrid)));


