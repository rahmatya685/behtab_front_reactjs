import React from 'react'
import ReactDOM from 'react-dom'
import { render } from 'react-dom'
import { LocalizeProvider, Translate, withLocalize } from "react-localize-redux";
import { BrowserRouter as Router, Route, Link } from 'react-router-dom'
import '../node_modules/bootstrap/dist/css/bootstrap.min.css';
import SignIn from './pages/SignIn'
import { SnackbarProvider } from 'notistack';
import Button from '@material-ui/core/Button';

import MainPage from './pages/MainPage'
import { MuiThemeProvider, createMuiTheme } from '@material-ui/core/styles';
import MapView from './components/MapView';
import HospitalsGrid from "./components/HospitalsGrid";

window.React = React.def

const themeRtl = createMuiTheme({
    direction: 'rtl',
    typography: {
        fontFamily: 'Vazir, sans-serif',
        useNextVariants: true,
    }
});

const App = props => (
    <MuiThemeProvider theme={themeRtl}>
        <LocalizeProvider>
            <SnackbarProvider maxSnack={3}
                anchorOrigin={{
                    vertical: 'bottom',
                    horizontal: 'left',
                }}
                action={[
                    <Button size="small">
                        <Translate id="dismiss" />
                    </Button>
                ]}>
                <Router basename={'/web_ui'} >
                    <div>
                        <Route path="/login" component={SignIn}></Route>
                        <Route path="/map" component={MapView}></Route>
                        <Route path="/hospitals" component={HospitalsGrid}></Route>
                        <Route exact path="/" component={MainPage}></Route>
                    </div>
                </Router>

            </SnackbarProvider>
        </LocalizeProvider>
    </MuiThemeProvider>

);

render(
    <App />,
    document.getElementById('react-continer')
)
