import React, { Component } from 'react';
import MaterialTable from 'material-table'
import { Translate, withLocalize } from 'react-localize-redux';
import globalTranslations from "../translations/global.json";
import { withStyles } from '@material-ui/core/styles';
import PropTypes from 'prop-types';
import Cookies from 'universal-cookie';
import Fab from '@material-ui/core/Fab'
import AddIcon from '@material-ui/icons/Add'
import CloudDownloadIcon from '@material-ui/icons/CloudDownload'
import classNames from 'classnames';
import { withSnackbar } from 'notistack';
import gridLocalization from "../components/GridLocalization"
import CustomizedDialog from '../components/Dialog.js';
import { ListItems } from '../components/MainListItems';
import download from 'downloadjs';



const styles = theme => ({
    extendedIcon: {
        marginRight: theme.spacing.unit,
    },
});

export const DoucumentsTypes =
{
    "ArchitecturalDocs": (<Translate id="ArchitecturalDocs" />),
    "StructuralDrawings": (<Translate id="StructuralDrawings" />),
    "StructuralDrawings": (<Translate id="StructuralDrawings" />),
    "NonStructuralReport": (<Translate id="NonStructuralReport" />),
    "RVA_Report": (<Translate id="RVA_Report" />),
    "PEA_REPORT": (<Translate id="PEA_REPORT" />),
    "Suplementary_Docs": (<Translate id="SuplementaryDocs" />)
};

class AttachmentsDialog extends Component {
    constructor(props) {
        super(props)
        this.props.addTranslation(globalTranslations);

        this.state = {
            width: 0, height: 0,
            attchments: Array.of(),
            isLoading: false
        };

        this.updateWindowDimensions = this.updateWindowDimensions.bind(this);
        this.getToken = this.getToken.bind(this);
        this.onDialogClosed = this.onDialogClosed.bind(this);
        this.doRuquest = this.doRuquest.bind(this);
        this.query = undefined;
        this.tableRef = undefined
        this.uuidv4 = this.uuidv4.bind(this);
        this.uploadAttachment = this.uploadAttachment.bind(this)
        this.uploadFile = null;
    }

    uuidv4() {
        return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
            var r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
            return v.toString(16);
        });
    }

    getToken() {
        const cookies = new Cookies();
        return cookies.get('token') || ''
    }

    componentWillMount() {

    }

    getData = (query, resolve, reject) => {

        const { attachmentType, entityId } = this.props;

        this.query = query

        let url = `${process.env.BASE_URL}/protected/attachments/get?type=${attachmentType}&id=${entityId}&`
        url += "size=" + query.pageSize
        url += "&page=" + (query.page)

        fetch(url, {
            method: "GET",
            mode: "cors",
            headers: {
                "Authorization": this.getToken(),
                "content-type": "application/json",
            }
        })
            .then(response => response.clone().json())
            .then(result => {
                if (result.content.length === 0) {
                    if (result.totalElements !== 0) {
                        query.page = query.page - 1
                        this.getData(query, resolve, reject)
                    } else {
                        resolve({
                            data: result.content,
                            page: result.number,
                            totalCount: result.totalElements,
                        })
                    }

                } else {
                    resolve({
                        data: result.content,
                        page: result.number,
                        totalCount: result.totalElements,
                    })
                }
            }).catch(error => { console.log("Error:", error) })

    }




    componentDidMount() {
        this.updateWindowDimensions();
        window.addEventListener('resize', this.updateWindowDimensions);

    }

    componentWillUnmount() {
        window.removeEventListener('resize', this.updateWindowDimensions);
    }

    updateWindowDimensions() {
        this.setState({ width: window.innerWidth, height: window.innerHeight });
    }

    onDialogClosed() {
        this.props.onDialogClosed()
    }
    doRuquest(resolve, reject, url, oldData) {
        fetch(url, {
            method: "POST",
            headers: {
                "Authorization": this.getToken(),
                "content-type": "application/json",
            },
            body: JSON.stringify(oldData)
        })
            .then(response => {
                if (response.ok) {
                    response.json().then((resp) => {
                        this.props.enqueueSnackbar(<Translate id="OperationDoneSuccessfully" />, {
                            variant: 'success',
                        });
                        resolve()
                    });
                } else {
                    if (response.status === 500) {
                        this.props.enqueueSnackbar(<Translate id="ServerError" />, {
                            variant: 'error',
                        });
                    } else if (response.status === 401) {
                        this.props.enqueueSnackbar(<Translate id="UserValidationError" />, {
                            variant: 'error',
                        });
                    } else {
                        this.props.enqueueSnackbar(response.toString(), {
                            variant: 'error',
                        });
                    }
                    reject()
                }
            })
            .catch(error => {
                reject()
                this.props.enqueueSnackbar(error.toString(), {
                    variant: 'error',

                })
            });
    }

    uploadAttachment(resolve, reject, newData) {

        const { entityId, attachmentType } = this.props;

        var formData = new FormData();
        formData.append('file', this.uploadFile);
        formData.append('category', attachmentType);
        formData.append('linkedId', entityId)
        formData.append('RowGUID', this.uuidv4())
        formData.append('comment', newData.comment)
        formData.append('extension', this.uploadFile.type)
        formData.append('fileName', this.uploadFile.name)

        try {
            fetch(`${process.env.BASE_URL}/protected/attachments/uploadFile`, {
                method: "POST", // *GET, POST, PUT, DELETE, etc.   
                headers: {
                    "Authorization": this.getToken()
                },
                body: formData
            })
                .then(data => {
                    if (data.ok) {
                        data.json().then((resp) => {
                            if (resp === "User not found") {
                                this.props.enqueueSnackbar(<Translate id="UserNotfound" />, {
                                    variant: 'error',
                                });
                                reject()
                            } else {
                                this.props.enqueueSnackbar(<Translate id="OperationDoneSuccessfully" />, {
                                    variant: 'success',
                                });
                                this.uploadFile = null;

                                resolve()
                            }
                        });
                    } else {
                        if (data.status === 500) {
                            this.props.enqueueSnackbar(<Translate id="ServerError" />, {
                                variant: 'error',
                            });

                        } else if (data.status === 401) {
                            this.props.enqueueSnackbar(<Translate id="UserValidationError" />, {
                                variant: 'error',
                            });
                        } else {
                            this.props.enqueueSnackbar(data.toString(), {
                                variant: 'error',
                            });
                        }
                        reject()

                    }
                })
                .catch(error => this.props.enqueueSnackbar(error.toString(), {
                    variant: 'error'
                }));
        } catch (e) {
            this.props.enqueueSnackbar(e.toString(), {
                variant: 'error'
            })
        }
    }

    render() {

        const { classes, theme, activeLanguage, entityName, entityId, attachmentType } = this.props;

        const { width } = this.state

        let fileName = (<Translate id="fileName" options={{ renderInnerHtml: false }} />)
        const comment = (<Translate id="comment" options={{ renderInnerHtml: false }} />)
        const fileType = (<Translate id="FileType" options={{ renderInnerHtml: false }} />)

        const gridTitle = activeLanguage.code === "en" ? `Attachments of ${entityName}` : `مستندات  ${entityName}`;


        return (
            <div >
                <CustomizedDialog customTitle={gridTitle} hideActions={true} okBtnTitleId="save" onDialogClosed={this.onDialogClosed} >
                    <MaterialTable ref="table"
                        columns={[
                            {
                                title: fileName, field: "fileName", searchable: true,
                                headerStyle: {
                                    fontFamily: '"Vazir", sans-serif'
                                },
                                cellStyle: {
                                    fontFamily: '"Vazir", sans-serif'
                                },
                                editComponent: props => (

                                    <input
                                        type="file"
                                        value={(props !== undefined && props.value !== undefined ? props.value : "")}
                                        onChange={e => {
                                            this.uploadFile = e.target.files[0]
                                            props.onChange(e.target.value);
                                        }
                                        }
                                    />

                                )
                            },
                            {
                                title: fileType, field: "comment", searchable: false, type: 'text',
                                lookup: DoucumentsTypes,
                                headerStyle: {
                                    fontFamily: '"Vazir", sans-serif'
                                },
                                cellStyle: {
                                    fontFamily: '"Vazir", sans-serif'
                                }
                            },

                        ]}
                        data={query => new Promise((resolve, reject) => {
                            this.getData(query, resolve, reject)
                        })
                        }
                        options={{
                            paging: true,
                            // maxBodyHeight: this.state.height/3,
                            toolbar: true,
                            actionsColumnIndex: -1,
                            showTitle: false,
                            search: false,
                            filtering: false,
                            addRowPosition: 'first',
                            headerStyle: {
                                fontFamily: '"Vazir", sans-serif'
                            },
                            doubleHorizontalScroll: true,
                            rowStyle: {
                                fontFamily: '"Vazir", sans-serif'
                            }
                        }}
                        actions={[
                            {
                                icon: CloudDownloadIcon,
                                tooltip: <Translate id="download" />,
                                onClick: (event, data) => {
                                    const url = `${process.env.BASE_URL}/downloadFile/${data.fileName}`
                                    fetch(url).then(resp => resp.blob())
                                        .then(blob => {
                                            download(blob, data.fileName, 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet')
                                        })
                                }
                            }
                        ]}
             
                        tableRef={table => this.tableRef = table}
                        editable={{
                            // onRowUpdate: oldData =>
                            //     new Promise((resolve, reject) => {
                            //         let url = `${process.env.BASE_URL}/protected/attachments/deleteFiles`

                            //         this.doRuquest(resolve, reject, url, oldData)
                            //     }),
                            onRowDelete: oldData =>
                                new Promise((resolve, reject) => {

                                    let url = `${process.env.BASE_URL}/protected/attachments/deleteFiles`

                                    this.doRuquest(resolve, reject, url, Array.of(oldData.rowGuid))

                                }),
                            onRowAdd: newData =>
                                new Promise((resolve, reject) => {

                                    this.uploadAttachment(resolve, reject, newData);

                                })
                        }}
                        localization={gridLocalization}

                    />
                </CustomizedDialog>
            </div>

        )
    }
}


AttachmentsDialog.propTypes = {
    classes: PropTypes.object.isRequired,
    theme: PropTypes.object.isRequired,
};
export default withStyles(styles, { withTheme: true })(withLocalize(withSnackbar(AttachmentsDialog)));


