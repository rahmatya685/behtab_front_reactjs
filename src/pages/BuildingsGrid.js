import React, { Component } from 'react';
import MaterialTable from 'material-table'
import { Translate, withLocalize } from 'react-localize-redux';
import globalTranslations from "../translations/global.json";
import { withStyles } from '@material-ui/core/styles';
import PropTypes from 'prop-types';
import Cookies from 'universal-cookie';
import Fab from '@material-ui/core/Fab'
import AddIcon from '@material-ui/icons/Add';
import classNames from 'classnames';
import { withSnackbar } from 'notistack';
import gridLocalization from "../components/GridLocalization"
import CustomizedDialog from '../components/Dialog.js';


const styles = theme => ({
    extendedIcon: {
        marginRight: theme.spacing.unit,
    },
});

class BuildingsGrid extends Component {
    constructor(props) {
        super(props)
        this.props.addTranslation(globalTranslations);

        this.state = {
            height: 0, buildings: Array.of(), isLoading: false
        };

        this.updateWindowDimensions = this.updateWindowDimensions.bind(this);
        this.getToken = this.getToken.bind(this);
        this.onDialogClosed = this.onDialogClosed.bind(this);
        this.doRuquest = this.doRuquest.bind(this);
        this.query = undefined;
        this.tableRef = undefined

    }



    getToken() {
        const cookies = new Cookies();
        return cookies.get('token') || ''
    }

    componentWillMount() {

    }

    getData = (query, resolve, reject) => {

        this.query = query

        let url = `${process.env.BASE_URL}/protected/hospitals/${this.props.hospital.id}/buildings?`
        url += "size=" + query.pageSize
        url += "&page=" + (query.page)

        fetch(url, {
            method: "GET",
            mode: "cors",
            headers: {
                "Authorization": this.getToken(),
                "content-type": "application/json",
            }
        })
            .then(response => response.clone().json())
            .then(result => {
                if (result.content.length === 0) {
                    if (result.totalElements !== 0) {
                        query.page = query.page - 1
                        this.getData(query, resolve, reject)
                    } else {
                        resolve({
                            data: result.content,
                            page: result.number,
                            totalCount: result.totalElements,
                        })
                    }

                } else {
                    resolve({
                        data: result.content,
                        page: result.number,
                        totalCount: result.totalElements,
                    })
                }
            }) 

    }




    componentDidMount() {
        this.updateWindowDimensions();
        window.addEventListener('resize', this.updateWindowDimensions);

    }

    componentWillUnmount() {
        window.removeEventListener('resize', this.updateWindowDimensions);
    }

    updateWindowDimensions() {
        this.setState({ height: window.innerHeight });
    }

    onDialogClosed() {
        this.props.onDialogClosed()
    }
    doRuquest(resolve, reject, url, oldData) {
        fetch(url, {
            method: "POST",
            headers: {
                "Authorization": this.getToken(),
                "content-type": "application/json",
            },
            body: JSON.stringify(oldData)
        })
            .then(response => {
                if (response.ok) {
                    response.json().then((resp) => {
                        this.props.enqueueSnackbar(<Translate id="OperationDoneSuccessfully" />, {
                            variant: 'success',
                        });
                        resolve()
                    });
                } else {
                    if (response.status === 500) {
                        this.props.enqueueSnackbar(<Translate id="ServerError" />, {
                            variant: 'error',
                        });
                    } else if (response.status === 401) {
                        this.props.enqueueSnackbar(<Translate id="UserValidationError" />, {
                            variant: 'error',
                        });
                    } else {
                        this.props.enqueueSnackbar(response.toString(), {
                            variant: 'error',
                        });
                    }
                    reject()
                }

            })
            .catch(error => {
                reject()
                this.props.enqueueSnackbar(error.toString(), {
                    variant: 'error',

                })
            });
    }

    render() {

        const { classes, theme, hospital, activeLanguage } = this.props;
        let titile = activeLanguage.code === "en" ? `Buildings of ${hospital.name}` : `لیست ساختمانهای    ${hospital.name}`;


        let name = (<Translate id="name" options={{ renderInnerHtml: false }} />)
        const comment = (<Translate id="comment" options={{ renderInnerHtml: false }} />)
        const totalConstructionArea = (<Translate id="totalConstructionArea" options={{ renderInnerHtml: false }} />)
        const floorArea = (<Translate id="floorArea" options={{ renderInnerHtml: false }} />)
        const typology = (<Translate id="typology" options={{ renderInnerHtml: false }} />)
        const constructionYear = (<Translate id="constructionYear" options={{ renderInnerHtml: false }} />)
        const numFloor = (<Translate id="numFloor" options={{ renderInnerHtml: false }} />)
        const extension = (<Translate id="extension" options={{ renderInnerHtml: false }} />)


        const BuildingTypology4 = (<Translate id="BuildingTypology4" options={{ renderInnerHtml: false }} />)
        const BuildingTypology5 = (<Translate id="BuildingTypology5" options={{ renderInnerHtml: false }} />)
        const BuildingTypology6 = (<Translate id="BuildingTypology6" options={{ renderInnerHtml: false }} />)
        const BuildingTypology7 = (<Translate id="BuildingTypology7" options={{ renderInnerHtml: false }} />)
        const BuildingTypology8 = (<Translate id="BuildingTypology8" options={{ renderInnerHtml: false }} />)
        const BuildingTypology9 = (<Translate id="BuildingTypology9" options={{ renderInnerHtml: false }} />)
        const BuildingTypology10 = (<Translate id="BuildingTypology10" options={{ renderInnerHtml: false }} />)
        const BuildingTypology11 = (<Translate id="BuildingTypology11" options={{ renderInnerHtml: false }} />)
        const BuildingTypology12 = (<Translate id="BuildingTypology12" options={{ renderInnerHtml: false }} />)
        const BuildingTypology13 = (<Translate id="BuildingTypology13" options={{ renderInnerHtml: false }} />)
        const BuildingTypology14 = (<Translate id="BuildingTypology14" options={{ renderInnerHtml: false }} />)
        const BuildingTypology15 = (<Translate id="BuildingTypology15" options={{ renderInnerHtml: false }} />)
        const BuildingTypology16 = (<Translate id="BuildingTypology16" options={{ renderInnerHtml: false }} />)
        const BuildingTypology17 = (<Translate id="BuildingTypology17" options={{ renderInnerHtml: false }} />)


        return (
            <div>
                <CustomizedDialog customTitle={titile} fullScreen={true} hideActions={true} saveBtnEnable={true} okBtnTitleId="save" onDialogClosed={this.onDialogClosed} >
                    <MaterialTable ref="table"
                        columns={[
                            {
                                title: name, field: 'name', searchable: true,
                                headerStyle: {
                                    fontFamily: '"Vazir", sans-serif'
                                },
                                cellStyle: {
                                    fontFamily: '"Vazir", sans-serif'
                                }
                            },
                            {
                                title: totalConstructionArea, field: 'totalConstructionArea', searchable: false, type: 'numeric',
                                headerStyle: {
                                    fontFamily: '"Vazir", sans-serif'
                                },
                                cellStyle: {
                                    fontFamily: '"Vazir", sans-serif'
                                }
                            },
                            {
                                title: floorArea, field: 'floorArea', searchable: false, type: 'numeric',
                                headerStyle: {
                                    fontFamily: '"Vazir", sans-serif'
                                },
                                cellStyle: {
                                    fontFamily: '"Vazir", sans-serif'
                                }
                            },
                            {
                                title: typology, field: 'typology', searchable: false, type: 'numeric',
                                lookup: {
                                    1: BuildingTypology4,
                                    2: BuildingTypology5,
                                    3: BuildingTypology6,
                                    4: BuildingTypology7,
                                    5: BuildingTypology8,
                                    6: BuildingTypology9,
                                    7: BuildingTypology10,
                                    8: BuildingTypology11,
                                    9: BuildingTypology12,
                                    10: BuildingTypology13,
                                    11: BuildingTypology14,
                                    12: BuildingTypology15,
                                    13: BuildingTypology16,
                                    14: BuildingTypology17
                                },
                                headerStyle: {
                                    fontFamily: '"Vazir", sans-serif'
                                },
                                cellStyle: {
                                    fontFamily: '"Vazir", sans-serif'
                                }
                            },
                            {
                                title: constructionYear, field: 'constructionYear', searchable: false,
                                headerStyle: {
                                    fontFamily: '"Vazir", sans-serif'
                                },
                                cellStyle: {
                                    fontFamily: '"Vazir", sans-serif'
                                }
                            },
                            {
                                title: numFloor, field: 'numFloor', searchable: false, type: 'numeric',
                                headerStyle: {
                                    fontFamily: '"Vazir", sans-serif'
                                },
                                cellStyle: {
                                    fontFamily: '"Vazir", sans-serif'
                                }
                            },
                            {
                                title: extension, field: 'extension', searchable: false,
                                lookup: { true: 'Yes', false: 'No' },
                                headerStyle: {
                                    fontFamily: '"Vazir", sans-serif'
                                },
                                cellStyle: {
                                    fontFamily: '"Vazir", sans-serif'
                                }
                            },
                            {
                                title: comment, field: 'comment', searchable: false,
                                headerStyle: {
                                    fontFamily: '"Vazir", sans-serif'
                                },
                                cellStyle: {
                                    fontFamily: '"Vazir", sans-serif'
                                }
                            },
                        ]}
                        data={query => new Promise((resolve, reject) => {
                            this.getData(query, resolve, reject)
                        })
                        }
                        options={{
                            paging: true,
                            // maxBodyHeight: this.state.height/3,
                            toolbar: true,
                            actionsColumnIndex: -1,
                            showTitle: false,
                            search: false,
                            filtering: false,
                            headerStyle: {
                                fontFamily: '"Vazir", sans-serif'
                            },
                            doubleHorizontalScroll: true,
                            rowStyle: {
                                fontFamily: '"Vazir", sans-serif'
                            }

                        }} 
                        tableRef={table => this.tableRef = table}
                        editable={{
                            onRowDelete: oldData =>
                                new Promise((resolve, reject) => {
                                    let url = `${process.env.BASE_URL}/protected/hospitals/${hospital.id}/buildings/delete`
                                    this.doRuquest(resolve, reject, url, Array.of(oldData.rowGuid))
                                }),
                            onRowUpdate: oldData =>
                                new Promise((resolve, reject) => {
                                    let url = `${process.env.BASE_URL}/protected/hospitals/${hospital.id}/buildings/add`

                                    this.doRuquest(resolve, reject, url, oldData)
                                })
                        }}
                        localization={gridLocalization}

                    />
                </CustomizedDialog>
            </div>

        )
    }
}



BuildingsGrid.propTypes = {
    classes: PropTypes.object.isRequired,
    theme: PropTypes.object.isRequired,
};
export default withStyles(styles, { withTheme: true })(withLocalize(withSnackbar(BuildingsGrid)));


