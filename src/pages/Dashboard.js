import React, { Component } from 'react';
import MaterialTable from 'material-table'
import { Translate, withLocalize } from 'react-localize-redux';
import globalTranslations from "../translations/global.json";
import { withStyles } from '@material-ui/core/styles';
import PropTypes from 'prop-types';
import Cookies from 'universal-cookie';
import Fab from '@material-ui/core/Fab'
import AddIcon from '@material-ui/icons/Add'
import classNames from 'classnames';
import { withSnackbar } from 'notistack';
import gridLocalization from "../components/GridLocalization"
import Tooltip from '@material-ui/core/Tooltip';
import { makeStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import Button from '@material-ui/core/Button';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import Typography from '@material-ui/core/Typography';
import { Grid } from '@material-ui/core';

import ProvinceRiskInfoPages from './ProvinceRiskInfoPages';
import HospitalRiskInfoGrid from '../components/HospitalRiskInfoGrid';
import RiskInfoByLevelOfImportance from '../pages/RiskInfoByLevelOfImportance';
import RiskInfoByLevelOfHazard from '../pages/RiskInfoByLevelOfHazard';
import PrioritizationInfoByLevelOfImportance from '../pages/PrioritizationInfoByLevelOfImportance';
import { renderToStaticMarkup } from "react-dom/server";
import topojson, { topology } from 'topojson-server';
import ExpansionPanel from '@material-ui/core/ExpansionPanel';
import ExpansionPanelDetails from '@material-ui/core/ExpansionPanelDetails';
import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';

const styles = theme => ({
  addbtn: {
    position: 'absolute',
    top: theme.spacing.unit * 10,
    right: theme.spacing.unit * 2,
    zIndex: 20
  },
  extendedIcon: {
    marginRight: theme.spacing.unit,
  },
  heading: {
    fontSize: theme.typography.pxToRem(15),
    flexBasis: '33.33%',
    flexShrink: 0,
  },
  secondaryHeading: {
    fontSize: theme.typography.pxToRem(15),
    color: theme.palette.text.secondary,
  },
});
class Dashboard extends Component {
  constructor(props) {
    super(props)

    this.state = {
      provinceRiskInfo4Hospital: null,
      provinceRiskInfo4HealthHouse: null,
      provinceRiskInfo4HospitalGeojson: {
        "type": "FeatureCollection",
        "features": []
      },
      provinceRiskInfo4HealthHouseGeojson: {
        "type": "FeatureCollection",
        "features": []
      },
      riskInfo4LevelOfImportanceHospital: null,
      riskInfo4LevelOfImportanceHealthHome: null,
      riskInfo4LevelOfHazardHospital: null,
      riskInfo4LevelOfHazardHealthHome: null,
      sampleData: null,
      HospitalsRiskInfoPanelExpanded: false
    }

    this.doRequest = this.doRequest.bind(this);
    this.updateWindowDimensions = this.updateWindowDimensions.bind(this);
    this.getToken = this.getToken.bind(this);
    this.getPorvinceRiskIndices4Hospital = this.getPorvinceRiskIndices4Hospital.bind(this);
    this.getPorvinceRiskIndices4HealthHouse = this.getPorvinceRiskIndices4HealthHouse.bind(this);
    this.handleHospitalsRiskInfoPanelExpansion = this.handleHospitalsRiskInfoPanelExpansion.bind(this);
  }

  getToken() {
    const cookies = new Cookies();
    return cookies.get('token') || ''
  }

  handleHospitalsRiskInfoPanelExpansion() {
    this.setState({
      HospitalsRiskInfoPanelExpanded: !this.state.HospitalsRiskInfoPanelExpanded
    })
  }

  componentDidMount() {
    this.updateWindowDimensions();
    window.addEventListener('resize', this.updateWindowDimensions);


    const requestQueue = Array.of();

    // requestQueue.push(this.doRequest("/protected/province/getanalyticalinfo/1", "GET", (resp) => this.getPorvinceRiskIndices4Hospital(resp, 1)))

    // requestQueue.push(this.doRequest("/protected/province/getanalyticalinfo/2", "GET", (resp) => this.getPorvinceRiskIndices4Hospital(resp, 2)))

    this.doRequest("/protected/hospitals/risk_info_for_level_of_importance/1", "GET", (resp) => {
      this.setState({
        riskInfo4LevelOfImportanceHospital: resp
      })
    })

    this.doRequest("/protected/hospitals/risk_info_for_level_of_importance/2", "GET", (resp) => {
      this.setState({
        riskInfo4LevelOfImportanceHealthHome: resp
      })
    })

    this.doRequest("/protected/hospitals/risk_info_for_level_of_hazard/1", "GET", (resp) => {
      this.setState({
        riskInfo4LevelOfHazardHospital: resp
      })
    })

    this.doRequest("/protected/hospitals/risk_info_for_level_of_hazard/2", "GET", (resp) => {
      this.setState({
        riskInfo4LevelOfHazardHealthHome: resp
      })
    })


    // Promise.all(requestQueue.reverse())
    //   .then((value) => {

    //   })
  }

  componentWillUnmount() {
    window.removeEventListener('resize', this.updateWindowDimensions);
  }


  updateWindowDimensions() {
    this.setState({ height: window.innerHeight });
  }


  getPorvinceRiskIndices4HealthHouse(resp) {

    let provinces = Array.of()

    resp.forEach(element => {
      if (element.prioritizationIndex > 0)
        provinces.push(element)
    });


    this.setState({
      provinceRiskInfo4HealthHouse: provinces
    })
  }
  getPorvinceRiskIndices4Hospital(resp, type) {

    let provinces = Array.of()

    const temGeojson = {
      "type": "FeatureCollection",
      "features": []
    }

    resp.forEach(element => {
      if (element.prioritizationIndex > 0) {
        provinces.push(element);
      }
      const featureGeojsonTemp = {
        "type": "Feature",
        "properties": {}
      }
      featureGeojsonTemp.properties["id"] = element.id.toString();
      featureGeojsonTemp.properties["name"] = element.name;
      featureGeojsonTemp.properties["ename"] = element.ename;
      featureGeojsonTemp.properties["riskIndexStructural"] = element.riskIndexStructural;
      featureGeojsonTemp.properties["riskIndexNonStructural"] = element.riskIndexNonStructural;
      featureGeojsonTemp.properties["riskIndexOrganizational"] = element.riskIndexOrganizational;
      featureGeojsonTemp.properties["prioritizationIndex"] = element.prioritizationIndex;

      featureGeojsonTemp["geometry"] = element.geom;
      featureGeojsonTemp["id"] = element.id.toString();


      temGeojson.features.push(featureGeojsonTemp)


    });

    if (type == 1) {
      this.setState({
        provinceRiskInfo4Hospital: provinces,
        provinceRiskInfo4HospitalGeojson: temGeojson
      })
    } else {
      this.setState({
        provinceRiskInfo4HealthHouse: provinces,
        provinceRiskInfo4HealthHouseGeojson: temGeojson
      })
    }

  }

  doRequest = (url, method, func) => {
    try {
      fetch(`${process.env.BASE_URL}${url}`, {
        method: method,
        headers: {
          "Content-Type": "application/json",
          "Authorization": this.getToken()
        },
      }).then(data => {

        if (data.ok) {

          data.json().then((resp) => {

            if (resp === "User not found") {
              this.props.enqueueSnackbar(<Translate id="UserNotfound" />, {
                variant: 'error',
              });
            } else {
              func(resp)
            }
          });
        } else {
          if (data.status === 500) {
            this.props.enqueueSnackbar(<Translate id="ServerError" />, {
              variant: 'error',
            });
          } else if (data.status === 401) {
            this.props.enqueueSnackbar(<Translate id="UserValidationError" />, {
              variant: 'error',
            });
          } else {
            this.props.enqueueSnackbar(data.toString(), {
              variant: 'error',
            });
          }
        }

      })
        .catch(error => this.props.enqueueSnackbar(error.toString(), {
          variant: 'error'
        }));
    } catch (e) {
      this.props.enqueueSnackbar(e.toString(), {
        variant: 'error'
      })
    }
  }

  render() {

    const { classes, theme } = this.props;

    const {
      provinceRiskInfo4Hospital,
      provinceRiskInfo4HealthHouse,
      riskInfo4LevelOfImportance,
      provinceRiskInfo4HospitalGeojson,
      provinceRiskInfo4HealthHouseGeojson,
      riskInfo4LevelOfImportanceHospital,
      riskInfo4LevelOfImportanceHealthHome,
      riskInfo4LevelOfHazardHospital,
      riskInfo4LevelOfHazardHealthHome,
      HospitalsRiskInfoPanelExpanded
    } = this.state

    return (
      <div>


        <Grid container spacing={3}>
          <Grid item xs={24}>
            <ProvinceRiskInfoPages
              provinceRiskInfo4Hospital={provinceRiskInfo4Hospital}
              provinceRiskInfo4HealthHouse={provinceRiskInfo4HealthHouse}
              provinceRiskInfo4HospitalGeojson={provinceRiskInfo4HospitalGeojson}
              provinceRiskInfo4HealthHouseGeojson={provinceRiskInfo4HealthHouseGeojson}
            >
            </ProvinceRiskInfoPages>
          </Grid>

          <Grid xs={24}>
            <RiskInfoByLevelOfImportance
              riskInfo4LevelOfImportanceHospital={riskInfo4LevelOfImportanceHospital}
              riskInfo4LevelOfImportanceHealthHome={riskInfo4LevelOfImportanceHealthHome}
            ></RiskInfoByLevelOfImportance>
          </Grid>

          <Grid xs={24}>
            <RiskInfoByLevelOfHazard
              riskInfo4LevelOfHazardHospital={riskInfo4LevelOfHazardHospital}
              riskInfo4LevelOfHazardHealthHome={riskInfo4LevelOfHazardHealthHome}
            ></RiskInfoByLevelOfHazard>
          </Grid>

          <Grid item xs={24}>






          </Grid>
        </Grid>
        <ExpansionPanel style={{ marginRight: "10px", marginLeft: "10px" }} expanded={HospitalsRiskInfoPanelExpanded} onChange={this.handleHospitalsRiskInfoPanelExpansion}>
          <ExpansionPanelSummary
            expandIcon={<ExpandMoreIcon />}
            aria-controls="panel1bh-content"
            id="panel1bh-header"
          >
            <Typography className={classes.heading}><Translate id="Hospitals"></Translate></Typography>
          </ExpansionPanelSummary>
          <ExpansionPanelDetails>
            {
              HospitalsRiskInfoPanelExpanded ? <HospitalRiskInfoGrid /> : null
            }

          </ExpansionPanelDetails>
        </ExpansionPanel>

      </div >
    )
  }
}

Dashboard.propTypes = {
  classes: PropTypes.object.isRequired,
  theme: PropTypes.object.isRequired,
};
export default withStyles(styles, { withTheme: true })(withLocalize(withSnackbar(Dashboard)));


