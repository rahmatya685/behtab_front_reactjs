import React, { Component } from 'react';
import { Translate, withLocalize } from 'react-localize-redux';
import globalTranslations from "../translations/global.json";
import { withStyles } from '@material-ui/core/styles';
import PropTypes from 'prop-types';
import Cookies from 'universal-cookie';
import CustomizedDialog from '../components/Dialog';
import { Grid } from '@material-ui/core';
import TextField from '@material-ui/core/TextField';
import { withSnackbar } from 'notistack';
import classNames from 'classnames';
import ImageUploader from 'react-images-upload';
import CircularProgress from '@material-ui/core/CircularProgress';
import LinearProgress from '@material-ui/core/LinearProgress';
import MenuItem from '@material-ui/core/MenuItem';


const styles = theme => ({
    root: {
        flexGrow: 1,
    },
    textField: {
        marginLeft: theme.spacing.unit,
        marginRight: theme.spacing.unit,
        width: '100%',
    },
    menu: {
        width: 400,
    },
    container: {
        display: 'flex',
        flexWrap: 'wrap',
    },

    dense: {
        marginTop: theme.spacing.unit * 2,
    },
});

export const LevelOfHazard = [
    {
        value: '1',
        label: 'L1-Low seismicity (0<PGA<0.2g)',
    },
    {
        value: '2',
        label: 'L2-Medium seismicity (0.2g<PGA<0.25g)',
    },
    {
        value: '3',
        label: 'L3-High seismicity (0.25g<PGA<0.3g)	0.75',
    },
    {
        value: '4',
        label: 'L4-Very high seismicity (PGA>0.3g)',
    }
]

export const LevelOfImportance = [
    {
        value: '1',
        label: 'L1-up to 32 No. Beds',
    },
    {
        value: '2',
        label: 'L2-up to 96 No. Beds',
    },
    {
        value: '3',
        label: 'L3-up to 300 No. Beds',
    },
    {
        value: '4',
        label: 'L4-up to 600 No. Beds',
    },
    {
        value: '5',
        label: 'L5-up to 800 No. Beds',
    },
    {
        value: '6',
        label: 'L6-more than 800 No. Beds',
    }
];


class HospitalAnalyticalReportDetailDialog extends Component {

    constructor(props) {
        super(props)

        this.state = {
            Hospital: this.props.hospital,
            picture: undefined
        }

        this.handleOkClicked = this.handleOkClicked.bind(this);
        this.handleChange = this.handleChange.bind(this);
        this.getToken = this.getToken.bind(this)
        this.onDialogClosed = this.onDialogClosed.bind(this)
        this.onChangeImageUpload = this.onChangeImageUpload.bind(this);
        this.uuidv4 = this.uuidv4.bind(this);
        this.postImage = this.postImage.bind(this);
    }

    onDialogClosed(hospital) {
        this.props.onDialogClosed(hospital)
    }

    getToken() {
        const cookies = new Cookies();
        return cookies.get('token') || ''
    }


    handleOkClicked() {
        const { picture } = this.state

        if (picture === undefined) {
            this.updateComments()
        } else {
            this.postImage()
        }
    }

    uuidv4() {
        return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
            var r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
            return v.toString(16);
        });
    }

    updateComments() {
        const { Hospital } = this.state

        try {
            fetch(`${process.env.BASE_URL}/protected/hospitals/update`, {
                method: "POST", // *GET, POST, PUT, DELETE, etc.   
                headers: {
                    "Content-Type": "application/json",
                    "Authorization": this.getToken()
                },
                body: JSON.stringify(Hospital)
            })
                .then(data => {
                    if (data.ok) {
                        data.json().then((resp) => {
                            if (resp === "User not found") {
                                this.props.enqueueSnackbar(<Translate id="UserNotfound" />, {
                                    variant: 'error',
                                });
                            } else if (resp.localizedMessage === "Action not allowed") {
                                this.props.enqueueSnackbar(<Translate id="ActionNotAllowed" />, {
                                    variant: 'error',
                                });
                            } else {
                                this.props.enqueueSnackbar(<Translate id="OperationDoneSuccessfully" />, {
                                    variant: 'success',
                                });
                                this.onDialogClosed(Hospital)
                            }
                        });
                    } else {
                        if (data.status === 500) {
                            this.props.enqueueSnackbar(<Translate id="ServerError" />, {
                                variant: 'error',
                            });
                        } else if (data.status === 401) {
                            this.props.enqueueSnackbar(<Translate id="UserValidationError" />, {
                                variant: 'error',
                            });
                        } else {
                            this.props.enqueueSnackbar(data.toString(), {
                                variant: 'error',
                            });
                        }
                    }
                })
                .catch(error => this.props.enqueueSnackbar(error.toString(), {
                    variant: 'error'
                }));
        } catch (e) {
            this.props.enqueueSnackbar(e.toString(), {
                variant: 'error'
            })
        }
    }
    postImage() {

        const { Hospital, picture } = this.state


        if (picture === undefined)
            return;
  
        var formData = new FormData();
        formData.append('file', picture);
        formData.append('category', "REPORT");
        formData.append('linkedId', Hospital.id)
        formData.append('RowGUID', this.uuidv4())
        formData.append('comment', 'comment')
        formData.append('extension', 'extension')


        try {
            fetch(`${process.env.BASE_URL}/protected/attachments/uploadFile`, {
                method: "POST", // *GET, POST, PUT, DELETE, etc.   
                headers: {
                    "Authorization": this.getToken()
                },
                body: formData
            })
                .then(data => {
                    if (data.ok) {
                        data.json().then((resp) => {
                            if (resp === "User not found") {
                                this.props.enqueueSnackbar(<Translate id="UserNotfound" />, {
                                    variant: 'error',
                                });
                            } else {

                                this.updateComments()

                            }
                        });
                    } else {
                        if (data.status === 500) {
                            this.props.enqueueSnackbar(<Translate id="ServerError" />, {
                                variant: 'error',
                            });
                        } else if (data.status === 401) {
                            this.props.enqueueSnackbar(<Translate id="UserValidationError" />, {
                                variant: 'error',
                            });
                        } else {
                            this.props.enqueueSnackbar(data.toString(), {
                                variant: 'error',
                            });
                        }
                    }
                })
                .catch(error => this.props.enqueueSnackbar(error.toString(), {
                    variant: 'error'
                }));
        } catch (e) {
            this.props.enqueueSnackbar(e.toString(), {
                variant: 'error'
            })
        }
    }

    handleChange = name => event => {
        const val = event.target.value
        const { Hospital } = this.state
        Hospital[name] = val
        this.setState((state, props) => ({
            Hospital: Hospital
        }));
    };



    validateForm() {

        const { Hospital } = this.state

        let formIsValid = true

        Object.entries(Hospital).forEach((key, value) => {

            if (key[0] === "executiveSummary" || key[0] == "observations4StructuralAssessment" || key[0] === "observations4NonStructuralAssessment" || key[0] === "observations4OrganizationalAssessment" || key[0] === "recommendations4StructuralAssessment" || key[0] === "recommendations4NonStructuralAssessment" || key[0] === "recommendations4OrganizationalAssessment" || key[0] === "overallRecommendations") {
                if (!(key[1] !== null && key[1].length > 0)) {
                    formIsValid = false
                }
            }

        })



        return formIsValid
    }
    onChangeImageUpload(pictureFiles, pictureDataURLs) {
        const firtstImage = pictureFiles[0]
        if (pictureFiles.length > 1)
            pictureFiles.splice(1, pictureFiles.length);
        this.setState({
            picture: firtstImage
        });
    }

    render() {

        const { classes, hospital, activeLanguage } = this.props


        let titile = activeLanguage.code === "en" ? `Observations and Recommendation for ${hospital.name}` : `مشاهدات و پیشنهادات فنی برای   ${hospital.name} `


        return (
            <div>
                <CustomizedDialog saveBtnEnable={!this.validateForm()} customTitle={titile} okBtnTitleId="save" onDialogClosed={this.onDialogClosed} handleOkClicked={this.handleOkClicked}>
                    <form className={classes.root}   >

                        <Grid container spacing={24}>

                            <Grid item xs={6}>
                                <TextField
                                    InputLabelProps={{ shrink: true }}
                                    id="outlined-select-currency"
                                    select
                                    label={<Translate id="LevelOfImportance" />}
                                    className={classes.textField}
                                    value={hospital.levelOfImportance}
                                    onChange={this.handleChange('levelOfImportance')}
                                    SelectProps={{
                                        MenuProps: {
                                            className: classes.menu,
                                        },
                                    }}
                                    helperText=""
                                    margin="normal"
                                    variant="outlined"
                                    required
                                >
                                    {LevelOfImportance.map(option => (
                                        <MenuItem key={option.value} value={option.value}>
                                            {option.label}
                                        </MenuItem>
                                    ))}
                                </TextField>
                            </Grid>
                            <Grid item xs={6}>
                                <TextField
                                    InputLabelProps={{ shrink: true }}
                                    id="outlined-select-ccurrency"
                                    select
                                    label={<Translate id="HazardLevels" />}
                                    className={classes.textField}
                                    value={hospital.hazardLevels}
                                    onChange={this.handleChange('hazardLevels')}
                                    SelectProps={{
                                        MenuProps: {
                                            className: classes.menu,
                                        },
                                    }}
                                    helperText="Hazard Levels (Standard No. 2800/UN-Habitat)-PGA"
                                    margin="normal"
                                    variant="outlined"
                                    required
                                >
                                    {LevelOfHazard.map(option => (
                                        <MenuItem key={option.value} value={option.value}>
                                            {option.label}
                                        </MenuItem>
                                    ))}
                                </TextField>
                            </Grid>



                            <Grid item xs={12}>
                                <TextField
                                    id="executiveSummary"
                                    label={<Translate id="executiveSummary" />}
                                    className={classes.textField}
                                    value={hospital.executiveSummary}
                                    onChange={this.handleChange('executiveSummary')}
                                    margin="normal"
                                    required
                                    multiline
                                    fullWidth
                                    variant="outlined"
                                />
                            </Grid>
                            <Grid item xs={12}>
                                <TextField
                                    id="observations4StructuralAssessment"
                                    label={<Translate id="observations4StructuralAssessment" />}
                                    className={classes.textField}
                                    value={hospital.observations4StructuralAssessment}
                                    onChange={this.handleChange('observations4StructuralAssessment')}
                                    margin="normal"
                                    required
                                    multiline
                                    fullWidth
                                    variant="outlined"
                                />
                            </Grid>

                            <Grid item xs={12}>
                                <TextField
                                    id="observations4NonStructuralAssessment"
                                    label={<Translate id="observations4NonStructuralAssessment" />}
                                    className={classes.textField}
                                    value={hospital.observations4NonStructuralAssessment}
                                    type="observations4NonStructuralAssessment"
                                    onChange={this.handleChange('observations4NonStructuralAssessment')}
                                    margin="normal"
                                    required
                                    multiline
                                    fullWidth
                                    variant="outlined"
                                />
                            </Grid>

                            <Grid item xs={12}>
                                <TextField
                                    id="observations4OrganizationalAssessment"
                                    label={<Translate id="observations4OrganizationalAssessment" />}
                                    className={classes.textField}
                                    value={hospital.observations4OrganizationalAssessment}
                                    type="observations4OrganizationalAssessment"
                                    onChange={this.handleChange('observations4OrganizationalAssessment')}
                                    margin="normal"
                                    required
                                    multiline
                                    fullWidth
                                    variant="outlined"
                                />
                            </Grid>
                            <Grid item xs={12}>
                                <TextField
                                    id="recommendations4StructuralAssessment"
                                    label={<Translate id="recommendations4StructuralAssessment" />}
                                    className={classes.textField}
                                    value={hospital.recommendations4StructuralAssessment}
                                    type="recommendations4StructuralAssessment"
                                    onChange={this.handleChange('recommendations4StructuralAssessment')}
                                    margin="normal"
                                    required
                                    multiline
                                    fullWidth
                                    variant="outlined"
                                />
                            </Grid>

                            <Grid item xs={12}>
                                <TextField
                                    id="recommendations4NonStructuralAssessment"
                                    label={<Translate id="recommendations4NonStructuralAssessment" />}
                                    className={classes.textField}
                                    value={hospital.recommendations4NonStructuralAssessment}
                                    type="recommendations4NonStructuralAssessment"
                                    onChange={this.handleChange('recommendations4NonStructuralAssessment')}
                                    margin="normal"
                                    required
                                    multiline
                                    fullWidth
                                    variant="outlined"
                                />
                            </Grid>

                            <Grid item xs={12}>
                                <TextField
                                    id="recommendations4OrganizationalAssessment"
                                    label={<Translate id="recommendations4OrganizationalAssessment" />}
                                    className={classes.textField}
                                    value={hospital.recommendations4OrganizationalAssessment}
                                    type="recommendations4OrganizationalAssessment"
                                    onChange={this.handleChange('recommendations4OrganizationalAssessment')}
                                    margin="normal"
                                    required
                                    multiline
                                    fullWidth
                                    variant="outlined"
                                />
                            </Grid>

                            <Grid item xs={12}>
                                <TextField
                                    id="overallRecommendations"
                                    label={<Translate id="overallRecommendations" />}
                                    className={classes.textField}
                                    value={hospital.overallRecommendations}
                                    type="overallRecommendations"
                                    onChange={this.handleChange('overallRecommendations')}
                                    margin="normal"
                                    required
                                    multiline
                                    fullWidth
                                    variant="outlined"
                                />
                            </Grid>
                            <Grid item xs={12}>
                                <ImageUploader
                                    withIcon={false}
                                    withPreview={true}
                                    withLabel={true}
                                    label={activeLanguage.code === "en" ? `Upload only one image` : `تنها یک فایل بارگذاری نمایید`}
                                    buttonText={activeLanguage.code === "en" ? `Upload Plan/Site Plan Image` : `بارگذاری عکس سایت پلان`}
                                    onChange={this.onChangeImageUpload}
                                    imgExtension={['.jpg', '.gif', '.png', '.gif']}
                                    maxFileSize={10242880}
                                />

                            </Grid>

                        </Grid>
                    </form>
                </CustomizedDialog>
            </div>
        )
    }
}

HospitalAnalyticalReportDetailDialog.propTypes = {
    classes: PropTypes.object.isRequired,
    theme: PropTypes.object.isRequired,
};
export default withStyles(styles, { withTheme: true })(withLocalize(withSnackbar(HospitalAnalyticalReportDetailDialog)));