import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import Panel from "../components/Panel";
import Cookies from 'universal-cookie';
import InputAdornment from '@material-ui/core/InputAdornment';
import Button from '@material-ui/core/Button';
import LocationOn from '@material-ui/icons/LocationOn';
import Select from '@material-ui/core/Select';
import { Translate, withLocalize } from 'react-localize-redux';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import { withSnackbar } from 'notistack';
import Radio from '@material-ui/core/Radio';
import RadioGroup from '@material-ui/core/RadioGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormLabel from '@material-ui/core/FormLabel';

const styles = theme => ({
  container: {
    display: 'flex',
    flexWrap: 'wrap',
  },
  textField: {
    marginLeft: theme.spacing.unit,
    marginRight: theme.spacing.unit,
    marginTop: theme.spacing.unit * 2,
    width: '100%',
  },
  dense: {
    marginTop: 19,
  },
  menu: {
    width: 200,
  },
  closeButton: {
    position: 'absolute',
    right: theme.spacing.unit,
    top: theme.spacing.unit,
    color: theme.palette.grey[500],
  },
  submit: {
    marginTop: theme.spacing.unit * 3,
  },
  selectEmpty: {
    marginTop: theme.spacing.unit * 2,
  },
});

class Hospital {
  constructor() {
    this.id = 0;
    this.name = "";
    this.address = "";
    this.phone = "";
    this.surveyorUserId = "";
    this.evaluatorUserId = "";
    this.centerPoint = { "type": "Point", "coordinates": Array.of(0, 0) };
    this.healthLocationType = 1;
    this.webSite = "";
    this.numTotalApprovedBeds = 0;
    this.bedOccupancyNormalRate = 0;
    this.clinicalStaffCount = 0;
    this.nonClinicalStaffCount = 0;
    this.serviceType = 0;
    this.organizationalAffiliation = 0;
  }
}
const serviceTypes = [
  {
    value: 0,
    label: <Translate id="ServiceType_General" />

  }, {
    value: 1,
    label: <Translate id="ServiceType_Single_Specialty" />,
  }, {
    value: 2,
    label: <Translate id="ServiceType_Treasury" />,
  }
];

const organizationalAffiliationTypes = [
  {
    value: 0,
    label: <Translate id="organizationalAffiliation_Governmental" />
  }, {
    value: 1,
    label: <Translate id="organizationalAffiliation_Private_Sector" />
  }, {
    value: 2,
    label: <Translate id="organizationalAffiliation_Academic" />
  }, {
    value: 3,
    label: <Translate id="organizationalAffiliation_Charity" />
  }, {
    value: 4,
    label: <Translate id="organizationalAffiliation_Military" />
  }, {
    value: 5,
    label: <Translate id="organizationalAffiliation_Social_Security_Organization" />
  }
];




class HospitalEdit extends React.Component {

  constructor(props) {
    super(props)
    this.onBtnSaveClicked = this.onBtnSaveClicked.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.validateForm = this.validateForm.bind(this);
    this.getUsers = this.getUsers.bind(this);
    this.centerPointChangedFromUi = false
  }
  state = {
    evaluators: Array.of(),
    surveyors: Array.of(),
    hospital: this.props.hospital === undefined ? new Hospital() : JSON.parse(JSON.stringify(this.props.hospital)),
    closePanel: false
  }

  componentDidMount() {
    this.getUsers('evaluators')
    this.getUsers('surveyors')
  }

  getUsers(type) {
    const cookies = new Cookies();
    let token = cookies.get('token') || ''

    try {
      fetch(`${process.env.BASE_URL}/protected/users/${type}`, {
        method: "POST", // *GET, POST, PUT, DELETE, etc.   
        headers: {
          "Content-Type": "application/json",
          "Authorization": token
        },
      })
        .then(data => {
          data.json().then((resp) => {

            if (resp === "User not found") {
              this.props.enqueueSnackbar(<Translate id="UserNotfound" />, {
                variant: 'error',
              });
            } else {
              let users = Array.of()
              resp.map(item => {
                users.push(item)
                //users.push({ "id": item.id, "name": item.name })
              })

              this.setState({
                [type]: users
              })
            }
          });
        })
        .catch(error => this.props.enqueueSnackbar(error.toString(), {
          variant: 'error'
        }));
    } catch (e) {
      this.props.enqueueSnackbar(e.toString(), {
        variant: 'error'
      })
    }
  }


  handleChange = name => event => {

    let { hospital, evaluators, surveyors } = this.state

    let coordsArray = Array.of()
    let text = event.target.value

    if (name === 'centerPoint') {
      this.centerPointChangedFromUi = true
      if (text.length !== 0) {
        let coordsStringArray = Array.of(text.split(",")[1], text.split(",")[0])
        coordsArray = Array.of(parseFloat(coordsStringArray[0]), parseFloat(coordsStringArray[1]))
        hospital[name] = text
      } else {
        hospital[name] = ""
      }

      this.props.onLocationChanged(coordsArray)

    } else if (name === "healthLocationType") {
      hospital[name] = parseInt(text)
    } else if (name === "evaluatorUserId") {
      hospital[name] = text
      evaluators.forEach(item => {
        if (item.id == text) {
          hospital["evaluatorUser"] = item
        }
      })

    } else if (name === "surveyorUserId") {
      hospital[name] = text
      surveyors.forEach(item => {
        if (item.id == text) {
          hospital["surveyorUser"] = item
        }
      })
    } else {
      hospital[name] = text
    }

    this.setState({ hospital: hospital });
  };

  onBtnSaveClicked() {

    let hospital = this.state.hospital
    let text = hospital.centerPoint

    if (typeof (text) === 'string' || text instanceof String) {
      let coordsStringArray = Array.of(text.split(",")[1], text.split(",")[0])

      let coordsArray = Array.of(0, 0)

      if (coordsStringArray[0] !== undefined && coordsStringArray[0] !== "" && coordsStringArray[1] !== undefined && coordsStringArray[1] !== "") {
        coordsArray = Array.of(parseFloat(coordsStringArray[0]), parseFloat(coordsStringArray[1]))
      }

      hospital.centerPoint = { 'type': 'Point', 'coordinates': coordsArray }
    }
 
    const cookies = new Cookies();
    let token = cookies.get('token') || ''

    let editAction = ""

    if (this.props.hospital === undefined) {
      editAction = "add";

      hospital.centerPoint.coordinates = this.props.location

    } else {
      editAction = "update";
    }

    try {
      fetch(`${process.env.BASE_URL}/protected/hospitals/${editAction}`, {
        method: "POST", // *GET, POST, PUT, DELETE, etc.   
        headers: {
          "Content-Type": "application/json",
          "Authorization": token,
        },
        body: JSON.stringify(hospital)
      })
        .then(data => {
          if (data.ok) {
            data.json().then((resp) => {
              if (resp == true) {
                this.props.enqueueSnackbar(<Translate id="hospitalEditSuccess" />, {
                  variant: 'success'
                })
                this.setState({
                  closePanel: true
                })
              } else if (resp.message !== undefined) {
                if (resp.message === "User not found") {
                  this.props.enqueueSnackbar(<Translate id="UserNotfound" />, {
                    variant: 'error',
                  });
                } else if (resp.localizedMessage === "HospitalNameShouldBeUnique") {
                  this.props.enqueueSnackbar(<Translate id="HospitalNameShouldBeUnique" />, {
                    variant: 'error',
                  });
                } else if (resp.localizedMessage === "Action not allowed") {
                  this.props.enqueueSnackbar(<Translate id="ActionNotAllowed" />, {
                    variant: 'error',
                  });
                }
              } else {
                this.props.enqueueSnackbar(resp.toString(), {
                  variant: 'error'
                })
              }
            });
          } else {
            data.text().then((body) => {
              this.props.enqueueSnackbar(body, {
                variant: 'error'
              })
            })
          }

        })
        .catch(error => this.props.enqueueSnackbar(error.toString(), {
          variant: 'error'
        }));
    } catch (e) {
      this.props.enqueueSnackbar(e.toString(), {
        variant: 'error'
      })
    }
  }
  validateForm() {

    const properites = ['name', 'address', 'phone', 'surveyorUserId', 'evaluatorUserId']

    let formIsValid = true

    properites.forEach((key) => {

      let value = this.state.hospital[key]

      if (value === null || value === undefined)
        formIsValid = false
      else if (typeof (value) === 'string' && value.length === undefined)
        formIsValid = false
      else if (typeof (value) === 'string' && value.length === 0)
        formIsValid = false
    })

    return formIsValid
  }

  render() {

    const { classes, location } = this.props;

    const { name, address, phone, surveyorUserId, evaluatorUserId, centerPoint, healthLocationType, webSite, numTotalApprovedBeds, bedOccupancyNormalRate, clinicalStaffCount, nonClinicalStaffCount, serviceType, organizationalAffiliation } = this.state.hospital

    let locationTemp = Array.of(0, 0)

    if (this.centerPointChangedFromUi) {
      locationTemp = centerPoint
      this.centerPointChangedFromUi = false
    } else {
      locationTemp = Array.of(location[1], location[0])
    }

    const { closePanel } = this.state

    let panelTitleId = "edit Hospital"
    if (this.props.hospital === undefined)
      panelTitleId = "add Hospital"

    const position = {
      my: "right-center",
      at: "right-center",
      offsetX: 5,
      offsetY: 5
    }

    return (
      <div> <Panel position={position} titleId={panelTitleId} onPanelClosed={this.props.onPanelClosed} closePanel={closePanel}>
        <form className={classes.container}   >

          <RadioGroup aria-label="position" name="healthLocationType" value={healthLocationType} onChange={this.handleChange('healthLocationType')} row>
            <FormControlLabel
              value={1}
              control={<Radio color="primary" />}
              label={<Translate id="Hospital" />}
              labelPlacement="start"
            />
            <FormControlLabel
              value={2}
              control={<Radio color="primary" />}
              label={<Translate id="HealthHome" />}
              labelPlacement="start"
            />
          </RadioGroup>

          <TextField
            id="name"
            label={<Translate id="name" />}
            className={classes.textField}
            value={name}
            name="name"
            onChange={this.handleChange('name')}
            margin="normal"
            required
          />

          <TextField
            id="Phone"
            label={<Translate id="phone" />}
            className={classes.textField}
            value={phone}
            onChange={this.handleChange('phone')}
            margin="normal"
            name="phone"
            required
          />

          <TextField
            id="address"
            label={<Translate id="address" />}
            className={classes.textField}
            value={address}
            onChange={this.handleChange('address')}
            margin="normal"
            name="address"
            required
          />
          <TextField
            className={classes.textField}
            id="centerPoint"
            label={<Translate id="location" />}
            value={locationTemp}
            onChange={this.handleChange('centerPoint')}
            name="centerPoint"
            required
            InputProps={{
              startAdornment: (
                <InputAdornment position="start">
                  <LocationOn />
                </InputAdornment>
              ),
            }}
          />

          <FormControl className={classes.textField}>
            <InputLabel htmlFor="age-simple"><Translate id="evaluatorUser" /> </InputLabel>
            <Select
              value={evaluatorUserId}
              onChange={this.handleChange('evaluatorUserId')}
              inputProps={{
                name: 'id',
                id: 'name',
              }}
              required
            >
              {this.state.evaluators.map(item => (
                <MenuItem key={item.id} value={item.id}  >
                  {item.name}
                </MenuItem>
              ))}
            </Select>
          </FormControl>
          <FormControl className={classes.textField}>
            <InputLabel htmlFor="age-simple"><Translate id="surveyUser" /> </InputLabel>
            <Select
              value={surveyorUserId}
              onChange={this.handleChange('surveyorUserId')}
              inputProps={{
                name: 'id',
                id: 'name',
              }}
              required
            >
              {this.state.surveyors.map(item => (
                <MenuItem key={item.id} value={item.id}  >
                  {item.name}
                </MenuItem>
              ))}
            </Select>
          </FormControl>



          <TextField
            id="webSite"
            label={<Translate id="webSite" />}
            className={classes.textField}
            value={webSite}
            onChange={this.handleChange('webSite')}
            margin="normal"
            name="webSite"
          />


          <TextField
            id="numTotalApprovedBeds"
            label={<Translate id="numTotalApprovedBeds" />}
            className={classes.textField}
            value={numTotalApprovedBeds}
            onChange={this.handleChange('numTotalApprovedBeds')}
            margin="normal"
            type="number"
            name="numTotalApprovedBeds"
          />



          <TextField
            id="bedOccupancyNormalRate"
            label={<Translate id="bedOccupancyNormalRate" />}
            className={classes.textField}
            value={bedOccupancyNormalRate}
            onChange={this.handleChange('bedOccupancyNormalRate')}
            margin="normal"
            type="number"
            name="bedOccupancyNormalRate"
          />

          <TextField
            id="clinicalStaffCount"
            label={<Translate id="clinicalStaffCount" />}
            className={classes.textField}
            value={clinicalStaffCount}
            onChange={this.handleChange('clinicalStaffCount')}
            margin="normal"
            type="number"
            name="clinicalStaffCount"
          />



          <TextField
            id="nonClinicalStaffCount"
            label={<Translate id="nonClinicalStaffCount" />}
            className={classes.textField}
            value={nonClinicalStaffCount}
            onChange={this.handleChange('nonClinicalStaffCount')}
            margin="normal"
            type="number"
            name="nonClinicalStaffCount"
          />


          <FormControl className={classes.textField}>
            <InputLabel htmlFor="age-simple"><Translate id="organizationalAffiliation" /> </InputLabel>
            <Select
              value={organizationalAffiliation}
              onChange={this.handleChange('organizationalAffiliation')}
              inputProps={{
                name: 'value',
                id: 'label',
              }}
              required
            >
              {organizationalAffiliationTypes.map(item => (
                <MenuItem key={item.value} value={item.value}  >
                  {item.label}
                </MenuItem>
              ))}
            </Select>
          </FormControl>

          <FormControl className={classes.textField}>
            <InputLabel htmlFor="age-simple"><Translate id="serviceType" /> </InputLabel>
            <Select
              value={serviceType}
              onChange={this.handleChange('serviceType')}
              inputProps={{
                name: 'value',
                id: 'label',
              }}
              required
            >
              {serviceTypes.map(item => (
                <MenuItem key={item.value} value={item.value}  >
                  {item.label}
                </MenuItem>
              ))}
            </Select>
          </FormControl>



          <Button
            fullWidth
            variant="contained"
            color="primary"
            onClick={this.onBtnSaveClicked}
            disabled={!this.validateForm()}
            className={classes.submit}  >

            <Translate id="save" />
          </Button>
        </form>
      </Panel></div>
    )
  }
}


export default withStyles(styles)(withLocalize(withSnackbar(HospitalEdit)))
