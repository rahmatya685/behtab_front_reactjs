import React, { createElement } from 'react'
import { Redirect } from 'react-router-dom'
import Cookies from 'universal-cookie';
import globalTranslations from "../translations/global.json";
import { renderToStaticMarkup } from "react-dom/server";
import { LocalizeProvider, Translate, withLocalize } from "react-localize-redux";
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import MenuAppBar from '../components/MenuAppBar'
import MapView from '../components/MapView'
import CssBaseline from '@material-ui/core/CssBaseline';
import Drawer from '@material-ui/core/Drawer';

import ChevronLeftIcon from '@material-ui/icons/ChevronLeft';
import MainListItems, { ListItems } from '../components/MainListItems';
import Divider from '@material-ui/core/Divider';
import classNames from 'classnames';
import IconButton from '@material-ui/core/IconButton';

import { MuiThemeProvider, createMuiTheme } from '@material-ui/core/styles';
import MobileSetting from './MobileSetting.js';
import WebSettings from './WebSettings'
import Questions from './Questions'
import { ca } from 'date-fns/esm/locale';
import Dashboard from '../pages/Dashboard';
import UsersGrid from '../components/UsersGrid.js';
import HospitalsGrid from '../components/HospitalsGrid.js';

const drawerWidth = 240;


const themeRtl = createMuiTheme({
  // direction: 'rtl', //uncomment it for gird bottom btns to display in ltr mode
  typography: {
    fontFamily: '"Vazir", sans-serif',
    useNextVariants: true,
  },
});

const styles = theme => ({
  root: {
    display: 'flex',
  },
  toolbar: {
    paddingRight: 24, // keep right padding when drawer closed
  },
  toolbarIcon: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-end',
    padding: '0 8px',
    ...theme.mixins.toolbar,
  },
  drawerPaper: {
    position: 'relative',
    whiteSpace: 'nowrap',
    width: drawerWidth,
    transition: theme.transitions.create('width', {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen,
    }),
  },
  drawerPaperClose: {
    overflowX: 'hidden',
    transition: theme.transitions.create('width', {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
    width: theme.spacing.unit * 7,
    [theme.breakpoints.up('sm')]: {
      width: theme.spacing.unit * 9,
    },
  },
  appBarSpacer: theme.mixins.toolbar,
  content: {
    paddingTop: theme.spacing.unit * 9,
    padding: theme.spacing.unit,
    flexGrow: 1,
    height: '100vh',
    overflow: 'auto',
  },
  h5: {
    marginBottom: theme.spacing.unit * 2,
  },
  mainListItems: {
    marginTop: '50px',
  }
});

class MainPage extends React.Component {
  constructor(props) {
    super(props)

  
    const cookies = new Cookies();
    const lang = cookies.get('languageCode') || "en";
    this.props.initialize({
      languages: [
        { name: "English", code: "en" },
        { name: "Farsi", code: "fa" }
      ],
      translation: globalTranslations,
      options: {
        renderToStaticMarkup,
        renderInnerHtml: true,
        defaultLanguage: lang
      }
    });



    this.state = {
      open: false,
      showMobileSetting: false,
      ViewType: "",
      SetType: "",
      showWebSettings: false,
      showQuestions: false
    };

    this.handleDrawerOpen = this.handleDrawerOpen.bind(this);
    this.handleMainListItemClick = this.handleMainListItemClick.bind(this);
    this.onBottomSheetChanged = this.onBottomSheetChanged.bind(this);
    this.onMobileSettingDialogClosed = this.onMobileSettingDialogClosed.bind(this);
    this.getToken = this.getToken.bind(this)
    this.logout = this.logout.bind(this);
    this.onWebSettingsClosed = this.onWebSettingsClosed.bind(this);
    this.onQuestionsDialogClosed = this.onQuestionsDialogClosed.bind(this);
  }


  handleDrawerOpen = () => {
    this.setState({ open: true });
  };

  handleDrawerClose = () => {
    this.setState({ open: false });
  };

  handleMainListItemClick(action) {
    switch (action) {
      case ListItems.UserManage:
      case ListItems.Hospitals:
      case ListItems.Layers:
      case ListItems.Dashboard:
        this.setState({
          ViewType: action
        });
        break;
      case ListItems.MobileSettings:
        this.setState({
          showMobileSetting: true,
        });
        break;
      case ListItems.ExposureIndex:
        this.setState({
          showWebSettings: true,
          SetType: action
        });
        break;
      case ListItems.LevelOfHazard:
        this.setState({
          showWebSettings: true,
          SetType: action
        });
        break;
      case ListItems.RelativeWeight:
        this.setState({
          showWebSettings: true,
          SetType: action
        });
        break;
      case ListItems.Questions:
        this.setState({
          showQuestions: true,
          SetType: action
        })
        break;
      default:
        break;
    }
  }

  getToken() {
    const cookies = new Cookies();
    return cookies.get('token') || ''
  }

  onWebSettingsClosed() {
    this.setState({
      showWebSettings: false
    })
  }

  onQuestionsDialogClosed() {
    this.setState({
      showQuestions: false
    })
  }


  onMobileSettingDialogClosed() {
    this.setState({
      showMobileSetting: false
    })
  }


  onBottomSheetChanged(opened) {
    this.setState({
      showBottomView: opened
    })
  }

  logout() {
    const cookies = new Cookies();
    cookies.remove('token', { path: '/' })
    this.setState({
      ViewType: ""
    })
  }


  render() {


    let token = this.getToken()
    const { classes } = this.props;

    if (token === undefined || token.length === 0) {
      return <Redirect to="/login" />
    }



    const { ViewType, showMobileSetting, showWebSettings, showQuestions, SetType } = this.state

    const vType = (this.props.location.state !== undefined && this.props.location.state.ViewType !== undefined) ? this.props.location.state.ViewType : ""

    let viewType = vType === "" ? ViewType : vType

    console.log(viewType)

    let bottomView = <Dashboard />
    switch (viewType) {
      case ListItems.Layers:
        bottomView = <MapView />
        break;
      case ListItems.UserManage:
        bottomView = <UsersGrid />
        break;
      case ListItems.Hospitals:
        {
          bottomView = <HospitalsGrid />
        }
        break;
      case ListItems.Dashboard:
        bottomView = <Dashboard />
        break;
      default:
        bottomView = <MapView />
        break;
    }

    return (

      <MuiThemeProvider theme={themeRtl}>
        <LocalizeProvider>
          {/* //  <div  dir="rtl" className={classes.root}> */}
          <div className={classes.root}>
            <CssBaseline />
            <MenuAppBar drawerIsOpen={this.state.open}
              handleDrawerOpen={this.handleDrawerOpen} logout={this.logout} />

            <Drawer
              variant="permanent"
              classes={{
                paper: classNames(classes.drawerPaper, !this.state.open && classes.drawerPaperClose),
              }}
              open={this.state.open}  >
              <div className={classes.toolbarIcon}>
                <IconButton onClick={this.handleDrawerClose}>
                  <ChevronLeftIcon />
                </IconButton>
              </div>
              <Divider />
              <MainListItems handleClick={this.handleMainListItemClick} className={classes.mainListItems} />
            </Drawer>
            <main className={classes.content}>

              {
                bottomView
              }
              {
                showMobileSetting ? <MobileSetting onDialogClosed={this.onMobileSettingDialogClosed}></MobileSetting> : null
              }
              {
                showWebSettings ? <WebSettings setType={SetType} onDialogClosed={this.onWebSettingsClosed}></WebSettings> : null
              }
              {
                showQuestions ? <Questions onPanelClosed={this.onQuestionsDialogClosed} ></Questions> : null
              }
            </main>
          </div>
        </LocalizeProvider>
      </MuiThemeProvider>
    )
  }
}
MainPage.propTypes = {
  classes: PropTypes.object.isRequired,
  theme: PropTypes.object.isRequired,
};
export default withStyles(styles)(withLocalize(MainPage))

