import React, { Component } from 'react';
import { Translate, withLocalize } from 'react-localize-redux';
import globalTranslations from "../translations/global.json";
import { withStyles } from '@material-ui/core/styles';
import PropTypes from 'prop-types';
import Cookies from 'universal-cookie';
import CustomizedDialog from '../components/Dialog.js';
import { Grid } from '@material-ui/core';
import TextField from '@material-ui/core/TextField';
import { withSnackbar } from 'notistack';
import FormLabel from '@material-ui/core/FormLabel';
import FormControl from '@material-ui/core/FormControl';
import FormGroup from '@material-ui/core/FormGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormHelperText from '@material-ui/core/FormHelperText';
import Switch from '@material-ui/core/Switch'

const styles = theme => ({
    root: {
        flexGrow: 1,
    }, textField: {
        marginLeft: theme.spacing.unit,
        marginRight: theme.spacing.unit,
        marginTop: theme.spacing.unit * 2,
        width: '100%',
    }
});

const SettingsKeys = {
    shouldCheckLocation: "5a13ac64-1971-4c9a-bf0c-c9e887713e1fdf5",
    UserDist2HospitalEntrance: "5a13ac64-1971-4c9a-bf0c-c9e887713e15"
}


class MobileSetting extends Component {
    constructor(props) {
        super(props)

        this.state = {
            settings: Array.of()
        }
        this.onBtnSaveClicked = this.onBtnSaveClicked.bind(this);
        this.getToken = this.getToken.bind(this)
        this.onDialogClosed = this.onDialogClosed.bind(this)
        this.handleChange = this.handleChange.bind(this);
        this.validateFor = this.validateForm.bind(this);
    }
    getToken() {
        const cookies = new Cookies();
        return cookies.get('token') || ''
    }


    componentDidMount() {
        fetch(`${process.env.BASE_URL}/protected/setting/getAllMobileAppSetting`, {
            method: "POST", // *GET, POST, PUT, DELETE, etc.   
            headers: {
                "Content-Type": "application/json",
                "Authorization": this.getToken()
            },
        })
            .then(data => {
                data.json().then((resp) => {

                    if (resp === "User not found") {
                        this.props.enqueueSnackbar(<Translate id="UserNotfound" />, {
                            variant: 'error',
                        });
                    } else {
                        let settings = Array.of()
                        resp.map(item => {
                            settings.push(item)
                        })

                        this.setState({
                            settings: settings
                        })
                    }
                });
            })
            .catch(error => this.props.enqueueSnackbar(error.toString(), {
                variant: 'error'
            }));

    }

    onDialogClosed() {
        this.props.onDialogClosed()
    }

    onBtnSaveClicked() {

        const { settings } = this.state

        fetch(`${process.env.BASE_URL}/protected/setting/updateMobileAppSetting`, {
            method: "POST", // *GET, POST, PUT, DELETE, etc.   
            headers: {
                "Content-Type": "application/json",
                "Authorization": this.getToken()
            },
            body: JSON.stringify(settings)
        })
            .then(data => {
                data.json().then((resp) => {

                    if (resp === "User not found") {
                        this.props.enqueueSnackbar(<Translate id="UserNotfound" />, {
                            variant: 'error',
                        });
                    } else {
                        this.props.enqueueSnackbar(<Translate id="OperationDoneSuccessfully" />, {
                            variant: 'success'
                        })
                        this.props.onDialogClosed()
                    }
                });
            })
            .catch(error => this.props.enqueueSnackbar(error.toString(), {
                variant: 'error'
            }));
    }

    handleChange = name => event => {
        const { settings } = this.state

        settings.forEach(item => {

            if (item.rowGuid === name) {
                if (item.rowGuid === SettingsKeys.shouldCheckLocation) {
                    item.value = event.target.checked === true ?  "true":"false"
                }
                if (item.rowGuid === SettingsKeys.UserDist2HospitalEntrance) {
                    let val = event.target.value
                    if (!isNaN(val) && val.length < 6)
                        item.value = val
                }
            }

        })

        this.setState({ settings: settings });
    };


    validateForm() {
        let formIsValid = false
        const { settings } = this.state


        let shouldCheckDistance = true

        settings.forEach(item => {
            if (item.rowGuid === SettingsKeys.shouldCheckLocation) {
                shouldCheckDistance = item.value
            }
        })

        settings.forEach(item => {
            if (item.rowGuid === SettingsKeys.UserDist2HospitalEntrance) {
                if (shouldCheckDistance) {
                    formIsValid = item.value !== undefined && item.value.length > 0
                } else {
                    formIsValid = true
                }
            }

        })

        return formIsValid
    }


    render() {
        const { classes } = this.props;

        const { settings } = this.state

        let shouldCheckDistance = "true"
        let userDist2HospitalEntrance = 100

        settings.forEach(item => {
            if (item.rowGuid === SettingsKeys.shouldCheckLocation) {
                shouldCheckDistance = item.value
            } else if (item.rowGuid === SettingsKeys.UserDist2HospitalEntrance) {
                userDist2HospitalEntrance = item.value
            }
        })
        console.log(settings)

        return (
            <div>
                <CustomizedDialog saveBtnEnable={!this.validateForm()} titleId="MobileSettings" okBtnTitleId="save" onDialogClosed={this.onDialogClosed} handleOkClicked={this.onBtnSaveClicked}>
                    <form className={classes.root}   >
                        <Grid container spacing={24}>
                            <Grid item xs={12}>
                                <FormControlLabel
                                    control={
                                        <Switch
                                            checked={shouldCheckDistance==="true"}
                                            onChange={this.handleChange(SettingsKeys.shouldCheckLocation)}
                                            value={shouldCheckDistance==="true"}
                                        />
                                    }
                                    label={<Translate id="ShouldCheckDistance" />}
                                />
                            </Grid>
                            <Grid item xs={12}>
                                <TextField
                                    id="UserDist2HospitalEntrance"
                                    label={<Translate id="UserDist2HospitalEntrance" />}
                                    className={classes.textField}
                                    onChange={this.handleChange(SettingsKeys.UserDist2HospitalEntrance)}
                                    margin="normal"
                                    disabled={shouldCheckDistance!=="true"}
                                    value={userDist2HospitalEntrance}
                                    required
                                />
                            </Grid>

                        </Grid>
                    </form>
                </CustomizedDialog>
            </div>
        )

    }


}


MobileSetting.propTypes = {
    classes: PropTypes.object.isRequired,
    theme: PropTypes.object.isRequired,
};
export default withStyles(styles, { withTheme: true })(withLocalize(withSnackbar(MobileSetting)));