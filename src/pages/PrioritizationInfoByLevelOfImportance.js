import React, { Component } from 'react';
import globalTranslations from "../translations/global.json";
import { withStyles } from '@material-ui/core/styles';
import { Translate, withLocalize } from 'react-localize-redux';
import { withSnackbar } from 'notistack';
import PropTypes from 'prop-types';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import Cookies from 'universal-cookie';
import gridLocalization from "../components/GridLocalization";
import CustomizedAxisTick from "../components/CustomizedAxisTick";
import { LineChart, Line, XAxis, YAxis, CartesianGrid, Tooltip, Legend, ResponsiveContainer } from 'recharts';

const styles = theme => ({
    container: {
        display: 'flex',
        flexWrap: 'wrap',
    },
    paper: {
        margin: theme.spacing.unit,
    },
    root: {
        width: '100%',
        maxWidth: 360,
        backgroundColor: theme.palette.background.paper,
    }
});



class PrioritizationInfoByLevelOfImportance extends Component {

    constructor(props) {
        super(props)

        this.props.addTranslation(globalTranslations);

        this.state = {
            width: 0, height: 0,

        }
        this.updateWindowDimensions = this.updateWindowDimensions.bind(this);
    }
    updateWindowDimensions() {
        this.setState({ width: window.innerWidth, height: window.innerHeight });
    }
    componentWillUnmount() {
        window.removeEventListener('resize', this.updateWindowDimensions);
    }
    componentDidMount() {
        this.updateWindowDimensions();
        window.addEventListener('resize', this.updateWindowDimensions);
    }

    render() {

        const { classes, theme, activeLanguage, riskInfo4LevelOfImportance } = this.props;

        const { width } = this.state;

        const cookies = new Cookies();
        
        const lang = cookies.get('languageCode') || "en";

        let dataKeyLevelOfImportance = lang === "en" ? "level_of_importance_en" : "level_of_importance_fa";

        return (
            <div>
                <Card className={classes.paper} >
                    <CardContent>
                        <Typography color="textSecondary" gutterBottom>
                            <Translate id="PrioritizationIndexLevelOfImportance"></Translate>
                        </Typography>

                        <ResponsiveContainer width={(width / 2) - 75} height={400}>
                            <LineChart data={riskInfo4LevelOfImportance}
                                margin={{ top: 5, right: 60, left: 0, bottom: 5 }}>
                                <XAxis dataKey={dataKeyLevelOfImportance}  interval="preserveStartEnd" tick={<CustomizedAxisTick />} interval={0} height={80} />
                                <YAxis width={30} />

                                <CartesianGrid strokeDasharray="3 3" />
                                <Tooltip />
                                <Legend verticalAlign="top" height={36} iconSize={16} />
                                {/* <Line type="monotone" name={<Translate id="RiskIndexStructural"></Translate>} dataKey="risk_index_structural" stroke="#e91e63" strokeWidth={2} />
                                <Line type="monotone" name={<Translate id="riskIndexNonStructural"></Translate>} dataKey="risk_index_non_structural" stroke="#3f51b5" strokeWidth={2} />
                                <Line type="monotone" name={<Translate id="riskIndexOrganizational"></Translate>} dataKey="risk_index_organizational" stroke="#ffc107" strokeWidth={2} /> */}
                                <Line type="monotone" name={<Translate id="prioritizationIndex"></Translate>} dataKey="prioritization_index" stroke="#4caf50" strokeWidth={2} activeDot={{ r: 8 }} />

                            </LineChart>
                        </ResponsiveContainer>

                    </CardContent>
                </Card>
            </div>
        )
    }
}

PrioritizationInfoByLevelOfImportance.propTypes = {
    classes: PropTypes.object.isRequired,
    theme: PropTypes.object.isRequired,
};
export default withStyles(styles, { withTheme: true })(withLocalize(withSnackbar(PrioritizationInfoByLevelOfImportance)));

