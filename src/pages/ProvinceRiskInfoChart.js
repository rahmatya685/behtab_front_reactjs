import React, { Component } from 'react';
import globalTranslations from "../translations/global.json";
import { withStyles } from '@material-ui/core/styles';
import { Translate, withLocalize } from 'react-localize-redux';
import { withSnackbar } from 'notistack';
import PropTypes from 'prop-types';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardHeader from '@material-ui/core/CardHeader';
import CardContent from '@material-ui/core/CardContent';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import Cookies from 'universal-cookie';
import gridLocalization from "../components/GridLocalization"
import CustomizedAxisTick from "../components/CustomizedAxisTick";
import ChartSymbolsCircle from "../components/ChartSymbolsCircle"
import ChartSymbolsRectangle from "../components/ChartSymbolsRectangle"
import ChartSymbolsTriangle from "../components/ChartSymbolsTriangle"
import { LineChart, Brush, Line, XAxis, YAxis, CartesianGrid, Tooltip, Legend, ResponsiveContainer } from 'recharts';
import Divider from '@material-ui/core/Divider';
import IconButton from '@material-ui/core/IconButton';
import { Grid } from '@material-ui/core';


const styles = theme => ({
    container: {
        display: 'flex',
        flexWrap: 'wrap',
    },
    paper: {
    },
    button: {
        margin: theme.spacing(1),
    },
    leftIcon: {

        marginRight: theme.spacing(1),
    },
    rightIcon: {
        marginLeft: theme.spacing(1),
    },
    root: {
        width: '100%',
        maxWidth: 360,
        backgroundColor: theme.palette.background.paper,

    }
});



class ProvinceRiskInfoChart extends Component {

    constructor(props) {
        super(props)

        this.props.addTranslation(globalTranslations);

        this.state = {
            width: 0, height: 0,
            healthcareLocationType: "Hospital",
            ShowMap: false,

        }

        this.updateWindowDimensions = this.updateWindowDimensions.bind(this);
    }
    updateWindowDimensions() {
        this.setState({ width: window.innerWidth, height: window.innerHeight });
    }
    componentWillUnmount() {
        window.removeEventListener('resize', this.updateWindowDimensions);
    }
    componentDidMount() {
        this.updateWindowDimensions();
        window.addEventListener('resize', this.updateWindowDimensions);
    }

    render() {

        const { classes, theme, activeLanguage, healthcareLocationType, provinceRiskInfo4Hospital, provinceRiskInfo4HealthHouse } = this.props;

        const { width } = this.state;

        const cookies = new Cookies();
        const lang = cookies.get('languageCode') || "en";
        let nameProvince = lang === "en" ? "ename" : "name";

        let provinceRiskInfo = provinceRiskInfo4Hospital
        if (healthcareLocationType === "HealthHome") {
            provinceRiskInfo = provinceRiskInfo4HealthHouse
        }


        let max = 0;
        let maxPrioritizationIndex = 0
        if (provinceRiskInfo != undefined) {

            for (let i = 0; i < provinceRiskInfo.length; i++) {
                const fieldVal = provinceRiskInfo[i].prioritizationIndex
                maxPrioritizationIndex++
                if (fieldVal === 0)
                    break;
            }

        }

        // console.log(provinceRiskInfo)
        // console.log(maxPrioritizationIndex)

        return (
            <div>


                <Grid container spacing={3}>
                    <Grid item xs={24}>

                        <ResponsiveContainer width={width - 190} height={350}>
                            <LineChart data={provinceRiskInfo}
                                margin={{ top: 0, right: 20, left: 0, bottom: 0 }}>
                                <XAxis dataKey={nameProvince} interval="preserveStartEnd" tick={<CustomizedAxisTick />} interval={0} height={40} />

                                <YAxis width={30} />
                                <CartesianGrid strokeDasharray="3 3" />
                                <Tooltip />
                                <Legend verticalAlign="top" height={30} iconSize={10}
                                    payload={
                                        [
                                            { id: 'pv', value: <Translate id="prioritizationIndex" />, type: 'circle', color: '#4caf50' },
                                        ]
                                    } />
                                <Brush dataKey={nameProvince} height={30} stroke="#8884d8" startIndex={0} endIndex={maxPrioritizationIndex} />
                                <Line type="monotone" name={<Translate id="prioritizationIndex"></Translate>} dataKey="prioritizationIndex" stroke="#4caf50" strokeWidth={2} dot={<ChartSymbolsCircle fill={"#4caf50"} />} />

                            </LineChart>
                        </ResponsiveContainer>

                    </Grid>
                    <Grid item xs={24}>


                        <ResponsiveContainer width={width - 190} height={350}>
                            <LineChart data={provinceRiskInfo}
                                margin={{ top: 0, right: 20, left: 0, bottom: 0 }}>
                                <XAxis dataKey={nameProvince} interval="preserveStartEnd" tick={<CustomizedAxisTick />} interval={0} height={40} />

                                <YAxis width={30} />
                                <CartesianGrid strokeDasharray="3 3" />
                                <Tooltip />
                                <Legend verticalAlign="top" height={30} iconSize={10}
                                    payload={
                                        [
                                            { id: 'pv', value: <Translate id="RiskIndexStructural" />, type: 'circle', color: '#e91e63' },
                                            { id: 'pv', value: <Translate id="riskIndexNonStructural" />, type: 'square', color: '#3f51b5' },
                                            { id: 'pv', value: <Translate id="riskIndexOrganizational" />, type: 'triangle', color: '#ffc107' },
                                        ]
                                    } />
                                <Brush dataKey={nameProvince} height={30} stroke="#8884d8" startIndex={0} endIndex={maxPrioritizationIndex} />
                                <Line type="monotone" name={<Translate id="RiskIndexStructural"></Translate>} dataKey="riskIndexStructural" stroke="#e91e63" strokeWidth={2} dot={<ChartSymbolsCircle fill={"#e91e63"} />} />
                                <Line type="monotone" name={<Translate id="riskIndexNonStructural"></Translate>} dataKey="riskIndexNonStructural" stroke="#3f51b5" strokeWidth={2} dot={<ChartSymbolsRectangle />} />
                                <Line type="monotone" name={<Translate id="riskIndexOrganizational"></Translate>} dataKey="riskIndexOrganizational" stroke="#ffc107" strokeWidth={2} dot={<ChartSymbolsTriangle />} />

                            </LineChart>
                        </ResponsiveContainer>
                    </Grid>
                </Grid>
            </div>
        )
    }
}

ProvinceRiskInfoChart.propTypes = {
    classes: PropTypes.object.isRequired,
    theme: PropTypes.object.isRequired,
};
export default withStyles(styles, { withTheme: true })(withLocalize(withSnackbar(ProvinceRiskInfoChart)));

