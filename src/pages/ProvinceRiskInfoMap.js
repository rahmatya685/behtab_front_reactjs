import React, { Component } from 'react';
import globalTranslations from "../translations/global.json";
import { withStyles } from '@material-ui/core/styles';
import { Translate, withLocalize } from 'react-localize-redux';
import { withSnackbar } from 'notistack';
import PropTypes from 'prop-types';
import Cookies from 'universal-cookie';
import VectorSource from 'ol/source/Vector';
import Map from 'ol/Map';
import View from 'ol/View';
import Select from 'ol/interaction/Select';
import OMSLayer from 'ol/source/OSM';
import { fromLonLat, transform } from 'ol/proj';
import { defaults } from 'ol/control'
import Feature from 'ol/Feature.js';
import Point from 'ol/geom/Point.js';
import Overlay from 'ol/Overlay.js';
import { toStringHDMS } from 'ol/coordinate.js';
import { toLonLat } from 'ol/proj.js';
import { ImageWMS } from 'ol/source';
import { Tile, Vector as VectorLayer, Image as ImageLayer } from 'ol/layer.js';
import { WFS, GeoJSON } from 'ol/format';
import { Icon, Style, Stroke, Text, Fill, Circle as CircleStyle, RegularShape } from 'ol/style.js';
import { defaults as defaultControls, Control } from 'ol/control.js';

import ScaleLine from 'ol/control/ScaleLine';
import jsPDF from 'jspdf';
import ZoomSlider from 'ol/control/ZoomSlider';
import MousePosition from 'ol/control/MousePosition';
import Tooltip from '@material-ui/core/Tooltip';
import Box from '@material-ui/core/Box';
import Legend from 'ol-ext/control/Legend'
import 'ol-ext/control/Legend.css';
import '../stylesheets/MapView.css';
import '../stylesheets/RiskMapLegend.css';
import Print from 'ol-ext/control/Print'
import 'ol-ext/control/Print.css';
import Circle from 'ol/geom/Circle.js';
import SearchNominatim from 'ol-ext/control/SearchNominatim';
import 'ol-ext/control/Search.css';
import 'ol-ext/dist/ol-ext.css'
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormHelperText from '@material-ui/core/FormHelperText';
import FormControl from '@material-ui/core/FormControl';
import Button from '@material-ui/core/Button';
import ButtonGroup from '@material-ui/core/ButtonGroup';
import red from '@material-ui/core/colors/red';
import Grid from '@material-ui/core/Grid';
 


const styles = theme => ({
    paper: { 
    },
    button: {
        margin: theme.spacing(1),
    },
    leftIcon: {
        marginRight: theme.spacing(1),
    },
    formControl: {
        margin: theme.spacing(1),
        minWidth: 120,
    },
    selectEmpty: {
        marginTop: theme.spacing(2),
    },
    rightIcon: {
        marginLeft: theme.spacing(1),
    },
    btnGroup: {

        position: 'relative',
        // marginLeft: '50px',
        // marginTop: '10px',
        marginBottom: '5px',
        zIndex: 20
    },
    legendLabels: {
        color: 'black'
    },
    root: {
        width: '100%',
        maxWidth: 360,
        backgroundColor: theme.palette.background.paper,

    }
});



const SymbologyFieldType = [
    { value: "prioritizationIndex", label: <Translate id="prioritizationIndex"></Translate> },
    { value: "riskIndexStructural", label: <Translate id="RiskIndexStructural"></Translate> },
    { value: "riskIndexNonStructural", label: <Translate id="riskIndexNonStructural"></Translate> },
    { value: "riskIndexOrganizational", label: <Translate id="riskIndexOrganizational"></Translate> },
]




class ProvinceRiskInfoMap extends Component {

    constructor(props) {
        super(props)

        this.props.addTranslation(globalTranslations);

        this.state = {
            width: 0, height: 0,
            map: undefined,
            layerInfos: undefined,
            ShowMap: false,
            SymbologyField: SymbologyFieldType[0].value
        }

        this.updateWindowDimensions = this.updateWindowDimensions.bind(this);
        this.handleChangeSybologyField = this.handleChangeSybologyField.bind(this);
        this.styleFunction = this.styleFunction.bind(this);
        this.getVectorSource = this.getVectorSource.bind(this);

    }
    updateWindowDimensions() {
        this.setState({ width: window.innerWidth, height: window.innerHeight });
    }
    componentWillUnmount() {
        window.removeEventListener('resize', this.updateWindowDimensions);
    }
    componentDidUpdate() {


        const { map, layerInfos } = this.state

        const { healthcareLocationType } = this.props

        if (layerInfos === undefined)
            return

        layerInfos.forEach(layerInfo => {
            layerInfo.layer.visible = false
        })

        layerInfos.forEach(layerInfo => {
            map.removeLayer(layerInfo.layer)
        })

        const currentLayer = this.getCurrentLayer()
        currentLayer.setStyle(this.styleFunction);
        map.addLayer(currentLayer)
    }

    componentDidMount() {

        this.updateWindowDimensions();

        const { activeLanguage } = this.props;

        window.addEventListener('resize', this.updateWindowDimensions);

        const cookies = new Cookies();
        const lang = cookies.get('languageCode') || "en";
        let nameProvince = lang === "en" ? "ename" : "name";

        const refLegendControl = this.refs.legend

        var LegendControl = (function (Control) {
            function LegendControl(opt_options) {
                var options = opt_options || {};

                refLegendControl.className = 'legend-control ol-control';

                Control.call(this, {
                    element: refLegendControl,
                    target: options.target
                });
            }

            if (Control) LegendControl.__proto__ = Control;
            LegendControl.prototype = Object.create(Control && Control.prototype);
            LegendControl.prototype.constructor = LegendControl;

            return LegendControl;
        }(Control));


        const toolTipOverlay = new Overlay({
            element: this.refs.popupContainer,
            autoPan: true,
            autoPanAnimation: {
                duration: 250
            }
        });
        const popupCloserRef = this.refs.popupCloser
        this.refs.popupCloser.onclick = function () {
            toolTipOverlay.setPosition(undefined);
            popupCloserRef.blur();
            return false;
        }


        const osmLayer = new Tile({
            source: new OMSLayer()
        })

        osmLayer.setOpacity(0.4)

        // create map object with feature layer
        var map = new Map({
            target: this.refs.mapContainer,
            controls: defaultControls().extend([
                new LegendControl()
            ]),
            layers:[osmLayer],
            overlays: [toolTipOverlay],
            view: new View({
                projection: 'EPSG:4326',
                center: [51.400261, 35.704338],//Boulder
                zoom: 6,
            })
        });

        map.on('pointermove', (evt) => {
            if (evt.dragging) {
                return;
            }
            // console.log(evt)
            const feature = map.forEachFeatureAtPixel(evt.pixel, (feature) => {

                this.refs.province.innerHTML = feature.getProperties()[nameProvince]
                SymbologyFieldType.forEach(item => {
                    this.refs[item.value].innerHTML = feature.getProperties()[item.value.toLowerCase()]
                })
                toolTipOverlay.setPosition(evt.coordinate);
                return feature;
            });
            if (!feature) {
                toolTipOverlay.setPosition(undefined);
            };
        });




        const layerInfos = []

        const layerNames = ["risk_info_of_hospitals_by_province", "risk_info_of_healthhome_by_province"]

        layerNames.forEach(layerName => {

            const wfsUrl = `${process.env.GEOSERVER_URL}/geoserver//behtab/ows?service=WFS&version=1.0.0&request=GetFeature&typeName=behtab:${layerName}&maxFeatures=50&outputFormat=application/json`;
  
            var vectorLayer = new VectorLayer({
                title: layerName,
                source: new VectorSource({
                    format: new GeoJSON(),
                    url: wfsUrl
                }),
                style: this.styleFunction
            })

            map.addLayer(vectorLayer)

            layerInfos.push({ layerName: layerName, layer: vectorLayer })
        })

        // let currentLayer = undefined;

        // if (this.props.healthcareLocationType === "HealthHome") {
        //     currentLayer = layerInfos[1].layer
        // } else {
        //     currentLayer = layerInfos[0].layer
        // }
        // map.getView().fit(currentLayer.getSource().getExtent(), { padding: [0, 0, 0, 0], nearest: true });

        this.setState({
            map: map,
            layerInfos: layerInfos
        });
    }

    getCurrentLayer() {
        if (this.props.healthcareLocationType === "HealthHome") {
            return this.state.layerInfos[1].layer
        } else {
            return this.state.layerInfos[0].layer
        }
    }

    getVectorSource() {

        const { healthcareLocationType, provinceRiskInfo4HospitalGeojson, provinceRiskInfo4HealthHouseGeojson } = this.props

        let provinceRiskInfo = provinceRiskInfo4HospitalGeojson
        if (healthcareLocationType === "HealthHome") {
            provinceRiskInfo = provinceRiskInfo4HealthHouseGeojson
        }

        const vectorSource = new VectorSource({
            features: (new GeoJSON(
                {
                    projection: 'EPSG:4326'
                }
            )).readFeatures(provinceRiskInfo)
        });
        return vectorSource
    }

    styleFunction = (feature) => {

        const { healthcareLocationType, provinceRiskInfo4HospitalGeojson, provinceRiskInfo4HealthHouseGeojson } = this.props;

        const { SymbologyField } = this.state;

        const cookies = new Cookies();
        const lang = cookies.get('languageCode') || "en";
        let nameProvince = lang === "en" ? "ename" : "name";

        let provinceRiskInfo = provinceRiskInfo4HospitalGeojson
        if (healthcareLocationType == "HealthHome") {
            provinceRiskInfo = provinceRiskInfo4HealthHouseGeojson
        }

        let max = 0;
        provinceRiskInfo.features.forEach(element => {
            const fieldVal = element.properties[SymbologyField]
            if (fieldVal > max)
                max = fieldVal
        });

        var val = feature.getProperties()[SymbologyField.toLowerCase()];

        const colorPercent = 100 - ((val / max) * 100);

        const style = new Style({
            stroke: new Stroke({
                color: "gray",
                width: 1
            }),
            fill: new Fill({
                color: `hsla(360, 100%, ${colorPercent}%, 1)`
                // color: `hsla(360, 100%, ${colorPercent}%, ${1 - (colorPercent / 100)})`
            }),
            text: new Text({
                font: '12px Calibri,sans-serif',
                fill: new Fill({
                    color: '#000'
                }),
                stroke: new Stroke({
                    color: '#fff',
                    width: 3
                })
            })
        })

        style.getText().setText(feature.getProperties()[nameProvince]);

        return style;
    };

    // handleChangeSybologyField = name => event => {
    //     console.log("handleChangeSybologyField")
    //     // this.setState({
    //     //     SymbologyField: event.target.value
    //     // })
    // }
    handleChangeSybologyField(event) {
        this.setState({
            SymbologyField: event.currentTarget.value
        })
    }

    render() {

        const { classes, theme, activeLanguage, healthcareLocationType, provinceRiskInfo4HospitalGeojson, provinceRiskInfo4HealthHouseGeojson } = this.props;

        const { width, SymbologyField } = this.state;

        let gradientVal4Legend = ""
        for (let i = 0; i < 100; i = i + 10) {
            if (gradientVal4Legend.length > 0)
                gradientVal4Legend += ","
            gradientVal4Legend += `hsla(360, 100%, ${i}%, 1)`
        }

        let provinceRiskInfo = provinceRiskInfo4HospitalGeojson
        if (healthcareLocationType === "HealthHome") {
            provinceRiskInfo = provinceRiskInfo4HealthHouseGeojson
        }


        let max = 0;
        if (provinceRiskInfo != undefined) {
            provinceRiskInfo.features.forEach(element => {
                const fieldVal = element.properties[SymbologyField]
                if (fieldVal > max)
                    max = fieldVal
            });
        }

        return (
            <div>
                <div>
                    <ButtonGroup variant="contained"
                        className={classes.btnGroup}
                        size="small"
                        color="secondry"
                        aria-label="small outlined button group">
                        {
                            SymbologyFieldType.map(item => (
                                <Button variant={SymbologyField === item.value ? "contained" : "outlined"} value={item.value} onClick={this.handleChangeSybologyField}>  {item.label}</Button>
                            ))
                        }
                    </ButtonGroup>

                    <div id='mapContainer' className='mapContainer' style={{ width: "87vw", height: "auto", padding: 0 }} ref="mapContainer">
                        <div className="legend-control ol-control" ref="legend">
                            <label>{max} %</label>
                            <Box width={20} height={100} style={{
                                background: `linear-gradient(${gradientVal4Legend})`
                            }}>
                            </Box>
                            <label>0%</label>
                        </div>
                    </div>
                </ div>

                <div id="popup" class="ol-popup" ref="popupContainer">
                    <div id="popup-content" ref="popupContent">
                        <InputLabel className={classes.legendLabels} ref="province">  </InputLabel>
                        {
                            SymbologyFieldType.map(item => (
                                <InputLabel className={classes.legendLabels} >{item.label} :
                                    <label htmlFor="age-simple" ref={item.value}> </label>
                                </InputLabel>

                            ))
                        }
                    </div>
                    <a href="#" id="popup-closer" class="ol-popup-closer" ref="popupCloser"></a>
                </div>

            </div>
        )
    }
}

ProvinceRiskInfoMap.propTypes = {
    classes: PropTypes.object.isRequired,
    theme: PropTypes.object.isRequired,
};
export default withStyles(styles, { withTheme: true })(withLocalize(withSnackbar(ProvinceRiskInfoMap)));

