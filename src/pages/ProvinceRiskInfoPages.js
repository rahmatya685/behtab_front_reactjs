import React, { Component } from 'react';
import globalTranslations from "../translations/global.json";
import { withStyles } from '@material-ui/core/styles';
import { Translate, withLocalize } from 'react-localize-redux';
import { withSnackbar } from 'notistack';
import PropTypes from 'prop-types';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardHeader from '@material-ui/core/CardHeader';
import CardContent from '@material-ui/core/CardContent';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import Cookies from 'universal-cookie';
import gridLocalization from "../components/GridLocalization"
import CustomizedAxisTick from "../components/CustomizedAxisTick";
import ChartSymbolsCircle from "../components/ChartSymbolsCircle"
import ChartSymbolsRectangle from "../components/ChartSymbolsRectangle"
import ChartSymbolsTriangle from "../components/ChartSymbolsTriangle"
import { LineChart, Line, XAxis, YAxis, CartesianGrid, Tooltip, Legend, ResponsiveContainer } from 'recharts';
import Divider from '@material-ui/core/Divider';
import Avatar from '@material-ui/core/Avatar';
import IconButton from '@material-ui/core/IconButton';
import { Grid } from '@material-ui/core';
import Box from '@material-ui/core/Box';

import MoreVertIcon from '@material-ui/icons/MoreVert';
import MapIcon from '@material-ui/icons/Map';
import ChartIcon from '@material-ui/icons/ShowChart';

import { red } from '@material-ui/core/colors';
import ButtonGroup from '@material-ui/core/ButtonGroup';

import ProvinceRiskInfoChart from '../pages/ProvinceRiskInfoChart'
import ProvinceRiskInfoMap from '../pages/ProvinceRiskInfoMap'

const styles = theme => ({
    container: {
        display: 'flex',
        flexWrap: 'wrap',
    },
    paper: {
        marginTop: theme.spacing.unit,
        marginLeft: theme.spacing.unit * 2,
        marginRight: theme.spacing.unit * 2,
    },
    button: {
        margin: theme.spacing(1),
    },
    leftIcon: {

        marginRight: theme.spacing(1),
    },
    rightIcon: {
        marginLeft: theme.spacing(1),
    },
    root: {
        width: '100%',
        maxWidth: 360,
        backgroundColor: theme.palette.background.paper,

    }
});



class ProvinceRiskInfoPages extends Component {

    constructor(props) {
        super(props)

        this.props.addTranslation(globalTranslations);

        this.state = {
            width: 0, height: 0,
            healthcareLocationType: "Hospital",
            ShowMap: false,
            provinceRiskInfo4Hospital: null,
            provinceRiskInfo4HealthHouse: null,
            provinceRiskInfo4HospitalGeojson: {
                "type": "FeatureCollection",
                "features": []
            },
            provinceRiskInfo4HealthHouseGeojson: {
                "type": "FeatureCollection",
                "features": []
            },

        }
        this.doRequest = this.doRequest.bind(this);
        this.updateWindowDimensions = this.updateWindowDimensions.bind(this);
        this.handleBtnReportType = this.handleBtnReportType.bind(this);
        this.handleBtnHealthcareLocationType = this.handleBtnHealthcareLocationType.bind(this);
        this.getPorvinceRiskIndices4Hospital = this.getPorvinceRiskIndices4Hospital.bind(this);
        this.getPorvinceRiskIndices4HealthHouse = this.getPorvinceRiskIndices4HealthHouse.bind(this);
        this.getToken = this.getToken.bind(this);
    }

    getToken() {
        const cookies = new Cookies();
        return cookies.get('token') || ''
    }
    updateWindowDimensions() {
        this.setState({ width: window.innerWidth, height: window.innerHeight });
    }
    componentWillUnmount() {
        window.removeEventListener('resize', this.updateWindowDimensions);
    }
    componentDidMount() {
        this.updateWindowDimensions();
        window.addEventListener('resize', this.updateWindowDimensions);

        const requestQueue = Array.of();

        requestQueue.push(this.doRequest("/protected/province/getanalyticalinfo/1", "GET", (resp) => this.getPorvinceRiskIndices4Hospital(resp, 1)))

        requestQueue.push(this.doRequest("/protected/province/getanalyticalinfo/2", "GET", (resp) => this.getPorvinceRiskIndices4Hospital(resp, 2)))


        Promise.all(requestQueue.reverse())
            .then((value) => {

            })
    }

    doRequest = (url, method, func) => {
        try {
            fetch(`${process.env.BASE_URL}${url}`, {
                method: method,
                headers: {
                    "Content-Type": "application/json",
                    "Authorization": this.getToken()
                },
            }).then(data => {

                if (data.ok) {

                    data.json().then((resp) => {

                        if (resp === "User not found") {
                            this.props.enqueueSnackbar(<Translate id="UserNotfound" />, {
                                variant: 'error',
                            });
                        } else {
                            func(resp)
                        }
                    });
                } else {
                    if (data.status === 500) {
                        this.props.enqueueSnackbar(<Translate id="ServerError" />, {
                            variant: 'error',
                        });
                    } else if (data.status === 401) {
                        this.props.enqueueSnackbar(<Translate id="UserValidationError" />, {
                            variant: 'error',
                        });
                    } else {
                        this.props.enqueueSnackbar(data.toString(), {
                            variant: 'error',
                        });
                    }
                }

            })
                .catch(error => this.props.enqueueSnackbar(error.toString(), {
                    variant: 'error'
                }));
        } catch (e) {
            this.props.enqueueSnackbar(e.toString(), {
                variant: 'error'
            })
        }
    }


    handleBtnReportType(event) {
        this.setState({
            ShowMap: event.currentTarget.value === 'true'
        })
    }
    handleBtnHealthcareLocationType(event) {
        // console.log(event.currentTarget.value)
        this.setState({
            healthcareLocationType: event.currentTarget.value
        })
    }

    getPorvinceRiskIndices4HealthHouse(resp) {

        let provinces = Array.of()

        resp.forEach(element => {
            if (element.prioritizationIndex > 0)
                provinces.push(element)
        });


        this.setState({
            provinceRiskInfo4HealthHouse: provinces
        })
    }
    getPorvinceRiskIndices4Hospital(resp, type) {

        let provinces = Array.of()

        const temGeojson = {
            "type": "FeatureCollection",
            "features": []
        }

        resp.forEach(element => {
            // if (element.prioritizationIndex > 0) {
            //     provinces.push(element);
            // }
            provinces.push(element);
            const featureGeojsonTemp = {
                "type": "Feature",
                "properties": {}
            }
            featureGeojsonTemp.properties["id"] = element.id.toString();
            featureGeojsonTemp.properties["name"] = element.name;
            featureGeojsonTemp.properties["ename"] = element.ename;
            featureGeojsonTemp.properties["riskIndexStructural"] = element.riskIndexStructural;
            featureGeojsonTemp.properties["riskIndexNonStructural"] = element.riskIndexNonStructural;
            featureGeojsonTemp.properties["riskIndexOrganizational"] = element.riskIndexOrganizational;
            featureGeojsonTemp.properties["prioritizationIndex"] = element.prioritizationIndex;

            featureGeojsonTemp["geometry"] = element.geom;
            featureGeojsonTemp["id"] = element.id.toString();


            temGeojson.features.push(featureGeojsonTemp)


        });

        if (type == 1) {
            this.setState({
                provinceRiskInfo4Hospital: provinces,
                provinceRiskInfo4HospitalGeojson: temGeojson
            })
        } else {
            this.setState({
                provinceRiskInfo4HealthHouse: provinces,
                provinceRiskInfo4HealthHouseGeojson: temGeojson
            })
        }

    }



    render() {

        const { classes, theme, activeLanguage, sampleData } = this.props;

        const { width, healthcareLocationType, ShowMap, provinceRiskInfo4Hospital, provinceRiskInfo4HealthHouse, provinceRiskInfo4HospitalGeojson, provinceRiskInfo4HealthHouseGeojson } = this.state;

        const cookies = new Cookies();
        const lang = cookies.get('languageCode') || "en";
        let nameProvince = lang === "en" ? "ename" : "name";

        let provinceRiskInfo = provinceRiskInfo4Hospital
        if (healthcareLocationType === "HealthHome") {
            provinceRiskInfo = provinceRiskInfo4HealthHouse
        }

        return (

            <Card className={classes.paper} >
                <CardHeader
                    // avatar={
                    //     <Avatar aria-label="recipe" className={classes.avatar}>
                    //         P
                    //    </Avatar>
                    // }
                    action={
                        <div>

                            <ButtonGroup color="primary" aria-label="outlined primary button group">
                                <Button value="Hospital" variant={healthcareLocationType === "Hospital" ? "contained" : "outlined"} onClick={this.handleBtnHealthcareLocationType} ><Translate id="Hospital" /></Button>
                                <Button value="HealthHome" variant={healthcareLocationType === "HealthHome" ? "contained" : "outlined"} onClick={this.handleBtnHealthcareLocationType}  ><Translate id="HealthHome" /></Button>
                            </ButtonGroup>

                            <ButtonGroup className={classes.button} color="primary" aria-label="outlined primary button group">
                                <Button value={false} id="Chart" variant={ShowMap ? "outlined" : "contained"} onClick={this.handleBtnReportType}><Translate id="Chart" />
                                    <ChartIcon className={classes.rightIcon} />
                                </Button>
                                <Button value={true} id="Map" variant={ShowMap ? "contained" : "outlined"} onClick={this.handleBtnReportType}><Translate id="Map" />
                                    <MapIcon className={classes.rightIcon} />
                                </Button>
                            </ButtonGroup>

                            {/* <IconButton aria-label="settings">
                                <MoreVertIcon />
                            </IconButton> */}
                        </div>
                    }
                    title={<Translate id="ProvinceRiskInfo" />}
                    titleTypographyProps={{ variant: 'h6' }}
                // subheader=""
                />


                <Divider />
                <CardContent>

                    {ShowMap ?

                        <ProvinceRiskInfoMap sampleData={sampleData} healthcareLocationType={healthcareLocationType} provinceRiskInfo4HospitalGeojson={provinceRiskInfo4HospitalGeojson} provinceRiskInfo4HealthHouseGeojson={provinceRiskInfo4HealthHouseGeojson}></ProvinceRiskInfoMap>

                        :

                        <ProvinceRiskInfoChart healthcareLocationType={healthcareLocationType} provinceRiskInfo4Hospital={provinceRiskInfo4Hospital} provinceRiskInfo4HealthHouse={provinceRiskInfo4HealthHouse}></ProvinceRiskInfoChart>

                    }

                </CardContent>
            </Card>
        )
    }
}

ProvinceRiskInfoPages.propTypes = {
    classes: PropTypes.object.isRequired,
    theme: PropTypes.object.isRequired,
};
export default withStyles(styles, { withTheme: true })(withLocalize(withSnackbar(ProvinceRiskInfoPages)));

