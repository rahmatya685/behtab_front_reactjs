import React, { Component } from 'react';
import MaterialTable from 'material-table'
import { Translate, withLocalize } from 'react-localize-redux';
import globalTranslations from "../translations/global.json";
import { withStyles } from '@material-ui/core/styles';
import PropTypes from 'prop-types';
import Cookies from 'universal-cookie';
import { withSnackbar } from 'notistack';
import gridLocalization from "../components/GridLocalization"
import CustomizedDialog from '../components/Dialog.js';
import Switch from '@material-ui/core/Switch';

const styles = theme => ({
    extendedIcon: {
        marginRight: theme.spacing.unit,
    },
});

class Question {
    constructor() {
        this.categoryId = 0;
        this.questionTitleFa = "";
        this.questionTitleEn = "";
        this.code = "";
        this.helpLink = "";
        this.weight = 0;
        this.briefDescriptionEn = "";
        this.briefDescriptionFa = "";
        this.questionCategoryByCategoryId = null;
        this.rank = "";
    }
}


class Questions extends Component {

    constructor(props) {
        super(props)
        this.props.addTranslation(globalTranslations);

        this.state = {
            height: 0, Categories: Array.of(), categoriesCode: Array.of(), isLoading: false,
            Questions: Array.of()
        };

        this.updateWindowDimensions = this.updateWindowDimensions.bind(this);
        this.getToken = this.getToken.bind(this);
        this.onDialogClosed = this.onDialogClosed.bind(this);
        this.doRuquest = this.doRuquest.bind(this);
        this.getCategories = this.getCategories.bind(this);
        this.enableDisableQuestion = this.enableDisableQuestion.bind(this);
        this.query = undefined;
        this.tableRef = undefined;
    }
    showProgress = () => {
        this.tableRef.setState({ isLoading: true })
    }
    hideProgress = () => {
        this.tableRef.setState({ isLoading: false })
    }
    refreshData = () => {
        this.tableRef.onQueryChange(this.query)
    }

    getCategories() {



        const { activeLanguage } = this.props;

        const cookies = new Cookies();
        let token = cookies.get('token') || ''

        try {
            fetch(`${process.env.BASE_URL}/protected/qcategories`, {
                method: "POST", // *GET, POST, PUT, DELETE, etc.   
                headers: {
                    "Content-Type": "application/json",
                    "Authorization": token
                },
            })
                .then(data => {
                    data.json().then((resp) => {
                        if (resp === "User not found") {
                            this.props.enqueueSnackbar(<Translate id="UserNotfound" />, {
                                variant: 'error',
                            });
                        } else {
                            let cates = Array.of()
                            let cateCodes = {}

                            resp.map(item => {
                                cates.push(item)
                                cateCodes[item.id] = activeLanguage.code === 'en' ? item.categoryTitleEn : item.categoryTitleFa
                            })

                            this.setState({
                                Categories: cates,
                                categoriesCode: cateCodes
                            })
                        }
                    });
                })
                .catch(error => this.props.enqueueSnackbar(error.toString(), {
                    variant: 'error'
                }));
        } catch (e) {
            this.props.enqueueSnackbar(e.toString(), {
                variant: 'error'
            })
        }


    }

    enableDisableQuestion(question) {
        this.showProgress()

        if (question.status === 1) {
            question.status = 2
        } else {
            question.status = 1
        }
        let url = `${process.env.BASE_URL}/protected/questions/update`

        // console.log(question)

        this.doRuquest(this.onEnableDisableResult, this.hideProgress, url, question)
    }

    onEnableDisableResult = () => {
        this.hideProgress()
        this.refreshData()
    }


    getToken() {
        const cookies = new Cookies();
        return cookies.get('token') || ''
    }

    componentWillMount() {
        this.getCategories()
    }

    getData = (query, resolve, reject) => {

        let url = `${process.env.BASE_URL}/protected/questions/page`
        // url += "size=" + query.pageSize
        // url += "&page=" + (query.page)

        fetch(url, {
            method: "GET",
            mode: "cors",
            headers: {
                "Authorization": this.getToken(),
                "content-type": "application/json",
            }
        })
            .then(response => response.clone().json())
            .then(result => {
                resolve({
                    data: result,
                    page: 0,
                    totalCount: result.length,
                })
            }) 

    }

    componentDidMount() {
        this.updateWindowDimensions();
        window.addEventListener('resize', this.updateWindowDimensions);

    }

    componentWillUnmount() {
        window.removeEventListener('resize', this.updateWindowDimensions);
    }

    updateWindowDimensions() {
        this.setState({ height: window.innerHeight });
    }

    onDialogClosed() {
        this.props.onPanelClosed()
    }
    doRuquest(resolve, reject, url, oldData) {
        fetch(url, {
            method: "POST",
            headers: {
                "Authorization": this.getToken(),
                "content-type": "application/json",
            },
            body: JSON.stringify(oldData)
        })
            .then(response => {
                if (response.ok) {
                    response.json().then((resp) => {
                        if (resp === "User not found") {
                            this.props.enqueueSnackbar(<Translate id="UserNotfound" />, {
                                variant: 'error',
                            });
                            reject()
                        } else if (resp.localizedMessage === "Action not allowed") {
                            this.props.enqueueSnackbar(<Translate id="ActionNotAllowed" />, {
                                variant: 'error',
                            });
                            reject()
                        } else {
                            this.props.enqueueSnackbar(<Translate id="OperationDoneSuccessfully" />, {
                                variant: 'success',
                            });
                            resolve()
                        }

                    });
                } else {
                    if (response.status === 500) {
                        this.props.enqueueSnackbar(<Translate id="ServerError" />, {
                            variant: 'error',
                        });
                    } else if (response.status === 401) {
                        this.props.enqueueSnackbar(<Translate id="UserValidationError" />, {
                            variant: 'error',
                        });
                    } else {
                        this.props.enqueueSnackbar(response.toString(), {
                            variant: 'error',
                        });
                    }
                    reject()
                }

            })
            .catch(error => {
                reject()
                this.props.enqueueSnackbar(error.toString(), {
                    variant: 'error',

                })
            });
    }

    render() {

        const { classes, theme, activeLanguage } = this.props;

        const { categoriesCode, Categories, height } = this.state

        const Module = activeLanguage.code === 'en' ? "Module" : "ماژول"
        const questionTitleFa = activeLanguage.code === 'en' ? "Title Persian" : "عنوان فارسی"
        const questionTitleEn = activeLanguage.code === 'en' ? "Title English" : "عنوان انگلیسی"
        const weight = activeLanguage.code === 'en' ? "Weight" : "وزن"
        const briefDescriptionFa = activeLanguage.code === 'en' ? "Brief Description Persian" : "توضیح مختصر فارسی"
        const briefDescriptionEn = activeLanguage.code === 'en' ? "Brief Description Engilish" : "توضیح مختصر انگلیسی"
        const helpLink = activeLanguage.code === 'en' ? "Help Link" : "لینک کمکی"
        const used4Hospital = activeLanguage.code === 'en' ? "Hospital" : "بیمارستان"
        const used4HealthHouse = activeLanguage.code === 'en' ? "Health House" : "خانه بهداشت"

        const category = activeLanguage.code === 'en' ? "Category" : "طبقه بندی"

        const rank = activeLanguage.code === 'en' ? "Row" : "ردیف"

        const isActive = activeLanguage.code === 'en' ? "Is Active" : "فعال"


        return (
            <div>
                <CustomizedDialog titleId="Questions" fullScreen={true} hideActions={true} okBtnTitleId="save" onDialogClosed={this.onDialogClosed} >
                    <MaterialTable ref="table"
                        columns={[
                            {
                                title: rank, field: 'rank', searchable: true,
                                headerStyle: {
                                    fontFamily: '"Vazir", sans-serif'
                                },
                                cellStyle: {
                                    fontFamily: '"Vazir", sans-serif'
                                }
                            },
                            {
                                title: Module, field: 'questionCategoryByCategoryId.moduleName', searchable: true, editable: false,
                                headerStyle: {
                                    fontFamily: '"Vazir", sans-serif'
                                },
                                cellStyle: {
                                    fontFamily: '"Vazir", sans-serif'
                                }
                            },
                            {
                                title: questionTitleFa, field: 'questionTitleFa', searchable: true,
                                headerStyle: {
                                    fontFamily: '"Vazir", sans-serif'
                                },
                                cellStyle: {
                                    fontFamily: '"Vazir", sans-serif'
                                }
                            },
                            {
                                title: questionTitleEn, field: 'questionTitleEn', searchable: true,
                                headerStyle: {
                                    fontFamily: '"Vazir", sans-serif'
                                },
                                cellStyle: {
                                    fontFamily: '"Vazir", sans-serif'
                                }
                            },
                            {
                                title: weight, field: 'weight', searchable: true, type: 'numeric',
                                headerStyle: {
                                    fontFamily: '"Vazir", sans-serif'
                                },
                            },
                            {
                                title: category, field: 'categoryId', searchable: true,
                                headerStyle: {
                                    fontFamily: '"Vazir", sans-serif'
                                },
                                cellStyle: {
                                    fontFamily: '"Vazir", sans-serif'
                                },
                                lookup: categoriesCode

                            },
                            {
                                title: used4Hospital, field: 'used4Hospital', searchable: true, type: 'boolean',
                                headerStyle: {
                                    fontFamily: '"Vazir", sans-serif'
                                },
                            },
                            {
                                title: used4HealthHouse, field: 'used4HealthHouse', searchable: true, type: 'boolean',
                                headerStyle: {
                                    fontFamily: '"Vazir", sans-serif'
                                },
                            }, {
                                title: isActive, field: 'status', searchable: true, type: 'boolean',
                                headerStyle: {
                                    fontFamily: '"Vazir", sans-serif'
                                },
                                render: rowData => <Switch
                                    checked={rowData.status === 2}
                                    onChange={() => this.enableDisableQuestion(rowData)}
                                    value={rowData.status === 2}
                                    inputProps={{ 'aria-label': 'secondary checkbox' }}
                                />,
                            },
                            {
                                title: briefDescriptionFa, field: 'briefDescriptionFa', searchable: true,
                                headerStyle: {
                                    fontFamily: '"Vazir", sans-serif'
                                },
                                cellStyle: {
                                    fontFamily: '"Vazir", sans-serif'
                                },
                                hidden: true
                            },
                            {
                                title: briefDescriptionEn, field: 'briefDescriptionEn', searchable: true,
                                headerStyle: {
                                    fontFamily: '"Vazir", sans-serif'
                                },
                                cellStyle: {
                                    fontFamily: '"Vazir", sans-serif'
                                },
                                hidden: true
                            },
                            {
                                title: helpLink, field: 'helpLink', searchable: true,
                                headerStyle: {
                                    fontFamily: '"Vazir", sans-serif'
                                },
                                hidden: true
                            }

                        ]}
                        data={query => new Promise((resolve, reject) => {
                            this.getData(query, resolve, reject)
                        })}
                        options={{
                            paging: false,
                            exportButton: true,
                            exportAllData: true,
                            maxBodyHeight: height - 150,
                            toolbar: true,
                            actionsColumnIndex: -1,
                            showTitle: false,
                            search: false,
                            filtering: false,
                            columnsButton: true,
                            addRowPosition: "first",
                            rowStyle: {
                                fontFamily: '"Vazir", sans-serif'
                            },
                            actionsCellStyle: {
                                fontFamily: '"Vazir", sans-serif'
                            },
                            filterCellStyle: {
                                fontFamily: '"Vazir", sans-serif'
                            },
                            headerStyle: {
                                fontFamily: '"Vazir", sans-serif'
                            }

                        }} 
                        tableRef={table => this.tableRef = table}
                        editable={{
                            onRowUpdate: oldData =>
                                new Promise((resolve, reject) => {

                                    let url = `${process.env.BASE_URL}/protected/questions/update` 

                                    oldData.status = oldData.status ? 2 : 1

                                    this.doRuquest(resolve, reject, url, oldData)

                                }),
                            // onRowDelete: oldData =>
                            //     new Promise((resolve, reject) => {

                            //         let url = `${process.env.BASE_URL}/protected/questions/delete`

                            //         this.doRuquest(resolve, reject, url, oldData)

                            //     }),
                            onRowAdd: newData =>
                                new Promise((resolve, reject) => {

                                    if (newData.categoryId === undefined || newData.categoryId == null
                                        || newData.weight == undefined || newData.weight === null
                                        || newData.questionTitleEn == undefined || newData.questionTitleEn === null
                                        || newData.questionTitleFa == undefined || newData.questionTitleFa === null
                                        || newData.rank == undefined || newData.rank == null
                                    ) {

                                        this.props.enqueueSnackbar(<Translate id="ErrorMsgFillAll" />, {
                                            variant: 'error',
                                        });

                                        reject()

                                    } else {
                                        let url = `${process.env.BASE_URL}/protected/questions/add`

                                        const question = new Question()
                                        question.categoryId = parseInt(newData.categoryId)
                                        question.code = "F"
                                        question.helpLink = newData.helpLink
                                        question.questionTitleEn = newData.questionTitleEn
                                        question.questionTitleFa = newData.questionTitleFa
                                        question.briefDescriptionEn = newData.briefDescriptionEn
                                        question.briefDescriptionFa = newData.briefDescriptionFa
                                        question.weight = newData.weight
                                        question.used4Hospital = newData.used4Hospital
                                        question.used4HealthHouse = newData.used4HealthHouse
                                        question.rank = newData.rank
                                        question.status = question.status ? 2 : 1

                                        Categories.forEach(item => {
                                            if (item.id == newData.categoryId) {
                                                question.questionCategoryByCategoryId = item
                                            }
                                        })

                                        this.doRuquest(resolve, reject, url, question)
                                    }

                                })
                        }}
                        localization={gridLocalization}

                    />
                </CustomizedDialog>
            </div>

        )
    }
}



Questions.propTypes = {
    classes: PropTypes.object.isRequired,
    theme: PropTypes.object.isRequired,
};
export default withStyles(styles, { withTheme: true })(withLocalize(withSnackbar(Questions)));


