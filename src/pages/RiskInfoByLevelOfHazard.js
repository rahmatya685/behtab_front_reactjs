import React, { Component } from 'react';
import globalTranslations from "../translations/global.json";
import { withStyles } from '@material-ui/core/styles';
import { Translate, withLocalize } from 'react-localize-redux';
import { withSnackbar } from 'notistack';
import PropTypes from 'prop-types';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import Cookies from 'universal-cookie';
import gridLocalization from "../components/GridLocalization"
import CustomizedAxisTick from "../components/CustomizedAxisTick";
import ChartSymbolsCircle from "../components/ChartSymbolsCircle"
import ChartSymbolsRectangle from "../components/ChartSymbolsRectangle"
import ChartSymbolsTriangle from "../components/ChartSymbolsTriangle"
import ButtonGroup from '@material-ui/core/ButtonGroup';
import ChartIcon from '@material-ui/icons/ShowChart';
import CardHeader from '@material-ui/core/CardHeader';
import IconButton from '@material-ui/core/IconButton';
import MoreVertIcon from '@material-ui/icons/MoreVert';
import Divider from '@material-ui/core/Divider';
import Grid from '@material-ui/core/Grid';

import Select from '@material-ui/core/Select';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';

import { LineChart, Line, XAxis, YAxis, CartesianGrid, Tooltip, Legend, ResponsiveContainer } from 'recharts';

const styles = theme => ({
    container: {
        display: 'flex',
        flexWrap: 'wrap',
    },
    paper: {
        marginTop: theme.spacing.unit,
        marginLeft: theme.spacing.unit * 3,
        marginRight: theme.spacing.unit * 3,
    },
    root: {
        width: '100%',
        maxWidth: 360,
        backgroundColor: theme.palette.background.paper,
    }
});


class RiskInfoByLevelOfHazard extends Component {

    constructor(props) {
        super(props)

        this.props.addTranslation(globalTranslations);

        this.state = {
            width: 0, height: 0,
            healthcareLocationType: "Hospital",
        }
        this.handleBtnHealthcareLocationType = this.handleBtnHealthcareLocationType.bind(this);
        this.updateWindowDimensions = this.updateWindowDimensions.bind(this);
    }
    updateWindowDimensions() {
        this.setState({ width: window.innerWidth, height: window.innerHeight });
    }
    componentWillUnmount() {
        window.removeEventListener('resize', this.updateWindowDimensions);
    }
    componentDidMount() {
        this.updateWindowDimensions();
        window.addEventListener('resize', this.updateWindowDimensions);
    }

    handleBtnHealthcareLocationType(event) {
        this.setState({
            healthcareLocationType: event.currentTarget.value
        })
    }

    render() {

        const { classes, theme, activeLanguage, riskInfo4LevelOfHazardHospital,
            riskInfo4LevelOfHazardHealthHome } = this.props;

        const { width, scale, healthcareLocationType } = this.state;

        const cookies = new Cookies();
        const lang = cookies.get('languageCode') || "en";
        let dataKeyLevelOfImportance = lang === "en" ? "hazard_levels_en" : "hazard_levels_fa";

        let riskInfo4LevelOfImportance = riskInfo4LevelOfHazardHospital
        if (healthcareLocationType == "HealthHome")
            riskInfo4LevelOfImportance = riskInfo4LevelOfHazardHealthHome

        return (
            <div>
                <Card className={classes.paper}>
                    <CardHeader
                        // avatar={
                        //     <Avatar aria-label="recipe" className={classes.avatar}>
                        //         P
                        //    </Avatar>
                        // }
                        action={
                            <div>
                                <ButtonGroup color="primary" aria-label="outlined primary button group">
                                    <Button value="Hospital" variant={healthcareLocationType === "Hospital" ? "contained" : "outlined"} onClick={this.handleBtnHealthcareLocationType} ><Translate id="Hospital" /></Button>
                                    <Button value="HealthHome" variant={healthcareLocationType === "HealthHome" ? "contained" : "outlined"} onClick={this.handleBtnHealthcareLocationType}  ><Translate id="HealthHome" /></Button>
                                </ButtonGroup>

                                {/* <IconButton aria-label="settings">
                                    <MoreVertIcon />
                                </IconButton> */}
                            </div>
                        }
                        title={<Translate id="RiskInfoByLevelOfHazard" />}
                        titleTypographyProps={{ variant: 'h6' }}
                    />


                    <Divider />
                    <CardContent>

                        <Grid container spacing={3}>
                            <Grid item xs={6}>


                                <ResponsiveContainer width={(width / 2) - 75} height={400}>
                                    <LineChart data={riskInfo4LevelOfImportance}
                                        margin={{ top: 5, right: 60, left: 0, bottom: 5 }}>
                                        <XAxis dataKey={dataKeyLevelOfImportance} interval="preserveStartEnd" tick={<CustomizedAxisTick />} interval={0} height={80} />
                                        <YAxis width={30} />

                                        <CartesianGrid strokeDasharray="3 3" />
                                        <Tooltip />
                                        <Legend verticalAlign="top" height={36} iconSize={16} />
                                        <Line type="monotone" name={<Translate id="prioritizationIndex"></Translate>} dataKey="prioritization_index" stroke="#4caf50" fill="#4caf50" strokeWidth={2} activeDot={{ r: 8 }} />

                                    </LineChart>
                                </ResponsiveContainer>


                            </Grid>
                            <Grid item xs={6}>


                                <ResponsiveContainer width={(width / 2) - 75} height={400}>
                                    <LineChart data={riskInfo4LevelOfImportance}
                                        margin={{ top: 5, right: 60, left: 0, bottom: 5 }}>

                                        <XAxis dataKey={dataKeyLevelOfImportance} interval="preserveStartEnd" tick={<CustomizedAxisTick />} interval={0} height={80} />

                                        <YAxis width={30} />
                                        <CartesianGrid strokeDasharray="3 3" />
                                        <Tooltip />
                                        <Legend verticalAlign="top" height={30} iconSize={10}
                                            payload={
                                                [
                                                    { id: 'pv', value: <Translate id="RiskIndexStructural" />, type: 'circle', color: '#e91e63' },
                                                    { id: 'pv', value: <Translate id="riskIndexNonStructural" />, type: 'square', color: '#3f51b5' },
                                                    { id: 'pv', value: <Translate id="riskIndexOrganizational" />, type: 'triangle', color: '#ffc107' },
                                                ]
                                            } />
                                        <Line type="monotone" name={<Translate id="RiskIndexStructural" />} dataKey="risk_index_structural" stroke="#e91e63" strokeWidth={2} dot={<ChartSymbolsCircle fill={"#e91e63"} />} />
                                        <Line type="monotone" name={<Translate id="riskIndexNonStructural" />} dataKey="risk_index_non_structural" stroke="#3f51b5" strokeWidth={2} dot={<ChartSymbolsRectangle />} />
                                        <Line type="monotone" name={<Translate id="riskIndexOrganizational" />} dataKey="risk_index_organizational" stroke="#ffc107" strokeWidth={2} dot={<ChartSymbolsTriangle />} />
                                        {/* <Line type="monotone" name={<Translate id="prioritizationIndex"></Translate>} dataKey="prioritization_index" stroke="#4caf50" strokeWidth={2} activeDot={{ r: 8 }} /> */}
                                    </LineChart>
                                </ResponsiveContainer>


                            </Grid>
                        </Grid>


                    </CardContent>
                </Card>
            </div>
        )
    }
}

RiskInfoByLevelOfHazard.propTypes = {
    classes: PropTypes.object.isRequired,
    theme: PropTypes.object.isRequired,
};
export default withStyles(styles, { withTheme: true })(withLocalize(withSnackbar(RiskInfoByLevelOfHazard)));

