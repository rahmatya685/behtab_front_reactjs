import React, { Component } from 'react';
import globalTranslations from "../translations/global.json";
import { withStyles } from '@material-ui/core/styles';
import { Translate, withLocalize } from 'react-localize-redux';
import { withSnackbar } from 'notistack';
import PropTypes from 'prop-types';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import Cookies from 'universal-cookie';
import gridLocalization from "../components/GridLocalization"
import CustomizedAxisTick from "../components/CustomizedAxisTick";
import ChartSymbolsCircle from "../components/ChartSymbolsCircle"
import ChartSymbolsRectangle from "../components/ChartSymbolsRectangle"
import ChartSymbolsTriangle from "../components/ChartSymbolsTriangle"
import ButtonGroup from '@material-ui/core/ButtonGroup';
import ChartIcon from '@material-ui/icons/ShowChart';
import CardHeader from '@material-ui/core/CardHeader';
import IconButton from '@material-ui/core/IconButton';
import MoreVertIcon from '@material-ui/icons/MoreVert';
import Divider from '@material-ui/core/Divider';
import Grid from '@material-ui/core/Grid';

import Select from '@material-ui/core/Select';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';

import { LineChart, Line, XAxis, YAxis, CartesianGrid, Tooltip, Legend, ResponsiveContainer } from 'recharts';

const styles = theme => ({
    container: {
        display: 'flex',
        flexWrap: 'wrap',
    },
    paper: {
        marginTop: theme.spacing.unit,
        marginLeft: theme.spacing.unit*3,
        marginRight: theme.spacing.unit*3,
    },
    root: {
        width: '100%',
        maxWidth: 360,
        backgroundColor: theme.palette.background.paper,
    }
});

const ScaleCountry = { "province_name_en": "Country Scale", "province_name": "مقیاس کشوری" }

class RiskInfoByLevelOfImportance extends Component {

    constructor(props) {
        super(props)

        this.props.addTranslation(globalTranslations);

        this.state = {
            width: 0, height: 0,
            healthcareLocationType: "Hospital",
            scale: ScaleCountry.province_name_en
        }
        this.handleBtnHealthcareLocationType = this.handleBtnHealthcareLocationType.bind(this);
        this.updateWindowDimensions = this.updateWindowDimensions.bind(this);
        this.handleScaleChange = this.handleScaleChange.bind(this);
    }
    updateWindowDimensions() {
        this.setState({ width: window.innerWidth, height: window.innerHeight });
    }
    componentWillUnmount() {
        window.removeEventListener('resize', this.updateWindowDimensions);
    }
    componentDidMount() {
        this.updateWindowDimensions();
        window.addEventListener('resize', this.updateWindowDimensions);
    }

    handleBtnHealthcareLocationType(event) {
         this.setState({
            healthcareLocationType: event.currentTarget.value
        })
    }

    handleScaleChange(event) {
        if (event != null) {
            this.setState({
                scale: event.target.value
            })
        }

    }


    render() {

        const { classes, theme, activeLanguage, riskInfo4LevelOfImportanceHospital, riskInfo4LevelOfImportanceHealthHome } = this.props;

        const { width, scale,healthcareLocationType } = this.state;

        const cookies = new Cookies();
        const lang = cookies.get('languageCode') || "en";
        let dataKeyLevelOfImportance = lang === "en" ? "level_of_importance_en" : "level_of_importance_fa";

        let riskInfo4LevelOfImportance = riskInfo4LevelOfImportanceHospital
        if (healthcareLocationType == "HealthHome")
            riskInfo4LevelOfImportance = riskInfo4LevelOfImportanceHealthHome

        const riskInfos = Array.of()
        const provinces = Array.of()

        provinces.push(ScaleCountry)

        let counter = 0;

        if (riskInfo4LevelOfImportance != null) {

            riskInfo4LevelOfImportance.forEach(item => {

                let foundProvince = undefined;

                provinces.forEach(province => {
                    if (province.province_name_en === item.province_name_en)
                        foundProvince = province
                })

                if (foundProvince === undefined) {
                    provinces.push({ "province_name_en": item.province_name_en, "province_name": item.province_name })
                }
            })



            riskInfo4LevelOfImportance.forEach(item => {

                let foundItem = undefined;

                riskInfos.forEach(riskInfo => {
                    if (riskInfo.level_of_importance_en === item.level_of_importance_en)
                        foundItem = riskInfo
                })

                if (foundItem === undefined) {
                    if (scale === ScaleCountry.province_name_en) {
                        riskInfos.push(item)
                        counter++;
                    } else {
                        if (scale === item.province_name_en) {
                            riskInfos.push(item)
                            counter++;
                        }

                    }
                } else {
                    riskInfos.forEach(riskInfo => {
                        if (riskInfo.level_of_importance_en === item.level_of_importance_en) {

                            if (scale === ScaleCountry.province_name_en) {
                                riskInfo.prioritization_index += item.prioritization_index;
                                riskInfo.risk_index_non_structural += item.risk_index_non_structural;
                                riskInfo.risk_index_organizational += item.risk_index_organizational;
                                riskInfo.risk_index_structural += item.risk_index_structural;
                                counter++;
                            } else {
                                if (scale === riskInfo.province_name_en) {
                                    riskInfo.prioritization_index += item.prioritization_index;
                                    riskInfo.risk_index_non_structural += item.risk_index_non_structural;
                                    riskInfo.risk_index_organizational += item.risk_index_organizational;
                                    riskInfo.risk_index_structural += item.risk_index_structural;
                                    counter++;
                                }

                            }
                        }
                    })
                }
            })

            if (counter > 0) {
                riskInfos.forEach(riskInfo => {
                    riskInfo.prioritization_index = riskInfo.prioritization_index/counter;
                    riskInfo.risk_index_non_structural = riskInfo.risk_index_non_structural/counter;
                    riskInfo.risk_index_organizational = riskInfo.risk_index_organizational/counter;
                    riskInfo.risk_index_structural = riskInfo.risk_index_structural/counter;
                })
            }

            riskInfo4LevelOfImportance = riskInfos
        }


        return (
            <div>
                <Card className={classes.paper}>
                    <CardHeader
                        // avatar={
                        //     <Avatar aria-label="recipe" className={classes.avatar}>
                        //         P
                        //    </Avatar>
                        // }
                        action={
                            <div>

                                <Select style={{marginRight:"5px"}}
                                    value={scale}
                                    onChange={this.handleScaleChange}
                                    inputProps={{
                                        name: 'province_name_en',
                                        id: 'province_name_en',
                                    }}
                                    required
                                >
                                    {provinces.map(item => (
                                        <MenuItem key={item.province_name_en} value={item.province_name_en}  >
                                            {lang === "en" ? item.province_name_en : item.province_name}
                                        </MenuItem>
                                    ))}
                                </Select>

                                <ButtonGroup color="primary" aria-label="outlined primary button group">
                                    <Button value="Hospital" variant={healthcareLocationType === "Hospital" ? "contained" : "outlined"} onClick={this.handleBtnHealthcareLocationType} ><Translate id="Hospital" /></Button>
                                    <Button value="HealthHome" variant={healthcareLocationType === "HealthHome" ? "contained" : "outlined"} onClick={this.handleBtnHealthcareLocationType}  ><Translate id="HealthHome" /></Button>
                                </ButtonGroup>

                                {/* <IconButton aria-label="settings">
                                    <MoreVertIcon />
                                </IconButton> */}
                            </div>
                        }
                        title={<Translate id="RiskInfoByLevelOfImportance" />}
                        titleTypographyProps={{ variant: 'h6' }}
                    />


                    <Divider />
                    <CardContent>

                        <Grid container >
                            <Grid item xs={6}>


                                <ResponsiveContainer width={(width / 2) - 75} height={400}>
                                    <LineChart data={riskInfo4LevelOfImportance}
                                        margin={{ top: 5, right: 60, left: 0, bottom: 5 }}>
                                        <XAxis dataKey={dataKeyLevelOfImportance} interval="preserveStartEnd" tick={<CustomizedAxisTick />} interval={0} height={80} />
                                        <YAxis width={30} />

                                        <CartesianGrid strokeDasharray="3 3" />
                                        <Tooltip />
                                        <Legend verticalAlign="top" height={36} iconSize={16} />
                                        <Line type="monotone" name={<Translate id="prioritizationIndex"></Translate>} dataKey="prioritization_index" stroke="#4caf50" fill="#4caf50" strokeWidth={2} activeDot={{ r: 8 }} />

                                    </LineChart>
                                </ResponsiveContainer>


                            </Grid>
                            <Grid item xs={6}>


                                <ResponsiveContainer width={(width / 2) - 75} height={400}>
                                    <LineChart data={riskInfo4LevelOfImportance}
                                        margin={{ top: 5, right: 60, left: 0, bottom: 5 }}>

                                        <XAxis dataKey={dataKeyLevelOfImportance} interval="preserveStartEnd" tick={<CustomizedAxisTick />} interval={0} height={80} />

                                        <YAxis width={30} />
                                        <CartesianGrid strokeDasharray="3 3" />
                                        <Tooltip />
                                        <Legend verticalAlign="top" height={30} iconSize={10}
                                            payload={
                                                [
                                                    { id: 'pv', value: <Translate id="RiskIndexStructural" />, type: 'circle', color: '#e91e63' },
                                                    { id: 'pv', value: <Translate id="riskIndexNonStructural" />, type: 'square', color: '#3f51b5' },
                                                    { id: 'pv', value: <Translate id="riskIndexOrganizational" />, type: 'triangle', color: '#ffc107' },
                                                ]
                                            } />
                                        <Line type="monotone" name={<Translate id="RiskIndexStructural" />} dataKey="risk_index_structural" stroke="#e91e63" strokeWidth={2} dot={<ChartSymbolsCircle fill={"#e91e63"} />} />
                                        <Line type="monotone" name={<Translate id="riskIndexNonStructural" />} dataKey="risk_index_non_structural" stroke="#3f51b5" strokeWidth={2} dot={<ChartSymbolsRectangle />} />
                                        <Line type="monotone" name={<Translate id="riskIndexOrganizational" />} dataKey="risk_index_organizational" stroke="#ffc107" strokeWidth={2} dot={<ChartSymbolsTriangle />} />
                                        {/* <Line type="monotone" name={<Translate id="prioritizationIndex"></Translate>} dataKey="prioritization_index" stroke="#4caf50" strokeWidth={2} activeDot={{ r: 8 }} /> */}
                                    </LineChart>
                                </ResponsiveContainer>


                            </Grid>
                        </Grid>


                    </CardContent>
                </Card>
            </div>
        )
    }
}

RiskInfoByLevelOfImportance.propTypes = {
    classes: PropTypes.object.isRequired,
    theme: PropTypes.object.isRequired,
};
export default withStyles(styles, { withTheme: true })(withLocalize(withSnackbar(RiskInfoByLevelOfImportance)));

