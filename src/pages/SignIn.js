import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import FormControl from '@material-ui/core/FormControl';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
import Input from '@material-ui/core/Input';
import InputLabel from '@material-ui/core/InputLabel';
import LockOutlinedIcon from '@material-ui/icons/LockOutlined';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import withStyles from '@material-ui/core/styles/withStyles';
import Cookies from 'universal-cookie';
import { Redirect } from 'react-router'
import globalTranslations from "../translations/global.json";
import LanguageToggle from '../components/LanguageToggle'
import { LocalizeProvider, Translate, withLocalize } from "react-localize-redux";
import { withSnackbar } from 'notistack';
import v_behdasht_icon from '../images/v_behdasht_icon.png'

import { MuiThemeProvider, createMuiTheme } from '@material-ui/core/styles';


const themeRtl = createMuiTheme({
    direction: 'rtl',
    typography: {
        fontFamily: '"Vazir", sans-serif',
        useNextVariants: true,
    },
});

const styles = theme => ({
    main: {
        width: 'auto',
        display: 'block', // Fix IE 11 issue.
        marginLeft: theme.spacing.unit * 3,
        marginRight: theme.spacing.unit * 3,
        [theme.breakpoints.up(400 + theme.spacing.unit * 3 * 2)]: {
            width: 400,
            marginLeft: 'auto',
            marginRight: 'auto',
        },
    },
    paper: {
        marginTop: theme.spacing.unit * 8,
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        padding: `${theme.spacing.unit * 2}px ${theme.spacing.unit * 3}px ${theme.spacing.unit * 3}px`,
    },
    avatar: {
        padding: theme.spacing.unit,
        width: theme.spacing(14),
        height: theme.spacing(14),
    },
    form: {
        width: '100%', // Fix IE 11 issue.
        marginTop: theme.spacing.unit,
    },
    submit: {
        marginTop: theme.spacing.unit * 3,
    },
    appTitle: {
        flexGrow: 1,
        align: "center",
        textAlign: "center",
    }
});


class SignIn extends Component {
    constructor(props) {
        super(props)
        this.state = {
            userName: "",
            password: "",
            toDashboard: false
        };
        this.props.addTranslation(globalTranslations);
    }
    validateForm() {
        return this.state.userName.length > 0 && this.state.password.length > 0;
    }
    onBtnLoginClicked = event => {
        event.preventDefault()
        try {
            fetch(`${process.env.BASE_URL}/users/login?userName=${this.state.userName}&password=${this.state.password}`)
                .then(data => {
                    data.text().then((text) => {
                        if (text === "User not found") {
                            this.props.enqueueSnackbar(<Translate id="UserNotfound" />, {
                                variant: 'error',
                            });
                        } else {
                            const cookies = new Cookies();
                            cookies.set('token', text, { path: '/' });

                            this.setState({
                                toDashboard: true
                            })

                            // <Redirect to="/"/>
                            //this.context.router.push('/');
                        }
                    });
                })
                .catch(error => this.props.enqueueSnackbar(error.toString(), {
                    variant: 'error',
                }));
        } catch (e) {
            this.props.enqueueSnackbar(e.toString(), {
                variant: 'error'
            })
        }


    }
    handleChange = event => {
        this.setState({
            [event.target.id]: event.target.value
        });
    }
    handleSubmit = event => {
        event.preventDefault();
    }

    render() {
        const { toDashboard } = this.state
        const { classes } = this.props

        if (toDashboard === true
        ) {
            return <Redirect to="/" />
        } else {

            return (
                <MuiThemeProvider theme={themeRtl}>
                    <main className={classes.main}>

                        <CssBaseline />
                        <Paper className={classes.paper}>

                            <Avatar className={classes.avatar} src={v_behdasht_icon}>
                                {/* <LockOutlinedIcon /> */}
                            </Avatar>
                            {/* <Typography component="h1" variant="h5">
                                    <Translate id="AppName" />
                                </Typography> */}
                            <Typography className={classes.appTitle} component="subtitle1" variant="subtitle1" >
                                <Translate id="AppNameLong" />
                            </Typography>

                            <LanguageToggle />

                            <form className={classes.form}>
                                <FormControl margin="normal" required fullWidth>
                                    <InputLabel htmlFor="userName">
                                        <Translate id="UserName" />
                                    </InputLabel>
                                    <Input id="userName" name="userName" autoFocus
                                        value={this.state.userName}
                                        onChange={this.handleChange} />
                                </FormControl>
                                <FormControl margin="normal" required fullWidth>
                                    <InputLabel htmlFor="password">
                                        <Translate id="Password" />
                                    </InputLabel>
                                    <Input name="password" type="password" id="password" autoComplete="current-password"
                                        value={this.state.password}
                                        onChange={this.handleChange}
                                    />
                                </FormControl>

                                <Button
                                    type="submit"
                                    fullWidth
                                    variant="contained"
                                    color="primary"
                                    className={classes.submit}
                                    disabled={!this.validateForm()}
                                    onClick={this.onBtnLoginClicked}>
                                    <Translate id="login" />

                                </Button>
                            </form>
                        </Paper>
                    </main>
                </MuiThemeProvider>
            );
        }
    }
}


SignIn.propTypes = {
    classes: PropTypes.object.isRequired,
    theme: PropTypes.object.isRequired,
};

export default withStyles(styles)(withLocalize(withSnackbar(SignIn)));