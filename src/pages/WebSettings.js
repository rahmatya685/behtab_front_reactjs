import React, { Component } from 'react';
import MaterialTable from 'material-table'
import { Translate, withLocalize } from 'react-localize-redux';
import globalTranslations from "../translations/global.json";
import { withStyles } from '@material-ui/core/styles';
import PropTypes from 'prop-types';
import Cookies from 'universal-cookie';
import Fab from '@material-ui/core/Fab'
import AddIcon from '@material-ui/icons/Add'
import classNames from 'classnames';
import { withSnackbar } from 'notistack';
import gridLocalization from "../components/GridLocalization"
import CustomizedDialog from '../components/Dialog.js';
import { ListItems } from '../components/MainListItems';


const styles = theme => ({
    extendedIcon: {
        marginRight: theme.spacing.unit,
    },
});

class WebSettings extends Component {
    constructor(props) {
        super(props)
        this.props.addTranslation(globalTranslations);

        this.state = {
            height: 0, buildings: Array.of(), isLoading: false
        };

        this.updateWindowDimensions = this.updateWindowDimensions.bind(this);
        this.getToken = this.getToken.bind(this);
        this.onDialogClosed = this.onDialogClosed.bind(this);
        this.doRuquest = this.doRuquest.bind(this);
        this.query = undefined;
        this.tableRef = undefined

    }



    getToken() {
        const cookies = new Cookies();
        return cookies.get('token') || ''
    }

    componentWillMount() {

    }

    getData = (query, resolve, reject) => {

        let method = ""

        const { setType } = this.props;

        if (setType === ListItems.LevelOfHazard) {
            method = "getHazardLevels"
        } else if (setType === ListItems.ExposureIndex) {
            method = "getExposureIndices"
        } else if (setType === ListItems.RelativeWeight) {
            method = "getRelativeWeights"
        }

        this.query = query

        let url = `${process.env.BASE_URL}/protected/setting/${method}?`
        url += "size=" + query.pageSize
        url += "&page=" + (query.page)

        fetch(url, {
            method: "GET",
            mode: "cors",
            headers: {
                "Authorization": this.getToken(),
                "content-type": "application/json",
            }
        })
            .then(response => response.clone().json())
            .then(result => {
                if (result.content.length === 0) {
                    if (result.totalElements !== 0) {
                        query.page = query.page - 1
                        this.getData(query, resolve, reject)
                    } else {
                        resolve({
                            data: result.content,
                            page: result.number,
                            totalCount: result.totalElements,
                        })
                    }

                } else {
                    resolve({
                        data: result.content,
                        page: result.number,
                        totalCount: result.totalElements,
                    })
                }
            }) 

    }




    componentDidMount() {
        this.updateWindowDimensions();
        window.addEventListener('resize', this.updateWindowDimensions);

    }

    componentWillUnmount() {
        window.removeEventListener('resize', this.updateWindowDimensions);
    }

    updateWindowDimensions() {
        this.setState({ height: window.innerHeight });
    }

    onDialogClosed() {
        this.props.onDialogClosed()
    }
    doRuquest(resolve, reject, url, oldData) {
        fetch(url, {
            method: "POST",
            headers: {
                "Authorization": this.getToken(),
                "content-type": "application/json",
            },
            body: JSON.stringify(oldData)
        })
            .then(response => {
                if (response.ok) {
                    response.json().then((resp) => {
                        this.props.enqueueSnackbar(<Translate id="OperationDoneSuccessfully" />, {
                            variant: 'success',
                        });
                        resolve()
                    });
                } else {
                    if (response.status === 500) {
                        this.props.enqueueSnackbar(<Translate id="ServerError" />, {
                            variant: 'error',
                        });
                    } else if (response.status === 401) {
                        this.props.enqueueSnackbar(<Translate id="UserValidationError" />, {
                            variant: 'error',
                        });
                    } else {
                        this.props.enqueueSnackbar(response.toString(), {
                            variant: 'error',
                        });
                    }
                    reject()
                }

            })
            .catch(error => {
                reject()
                this.props.enqueueSnackbar(error.toString(), {
                    variant: 'error',

                })
            });
    }

    render() {

        const { classes, theme, activeLanguage, setType } = this.props;

        let updateMethod = ""
        let valueColumnId = ""
        let dataFieldName = ""

        if (setType === ListItems.LevelOfHazard) {
            updateMethod = "updateHazardLevel"
            valueColumnId = "HazardLevels"
            dataFieldName = "hazardLevel"
        } else if (setType === ListItems.ExposureIndex) {
            updateMethod = "updateExposureIndice"
            valueColumnId = "ExposureIndex"
            dataFieldName = "exposureIndex"

        } else if (setType === ListItems.RelativeWeight) {
            updateMethod = "updateRelativeWeight"
            valueColumnId = "RelativeWeight"
            dataFieldName = "relativeWeight"
        }

        let name = (<Translate id="Categories" options={{ renderInnerHtml: false }} />)
        const valueColumn = (<Translate id={valueColumnId} options={{ renderInnerHtml: false }} />)

        let nameFieldTranslateId = activeLanguage.code === "en" ? "setNameEn" : "setNameFa";

        return (
            <div>
                <CustomizedDialog titleId={valueColumnId} hideActions={true} okBtnTitleId="save" onDialogClosed={this.onDialogClosed} >
                    <MaterialTable ref="table"
                        columns={[
                            {
                                title: name, field: nameFieldTranslateId, searchable: true, editable: false,
                                headerStyle: {
                                    fontFamily: '"Vazir", sans-serif'
                                },
                                cellStyle: {
                                    fontFamily: '"Vazir", sans-serif'
                                }
                            },
                            {
                                title: valueColumn, field: dataFieldName, searchable: false, type: 'numeric',
                                headerStyle: {
                                    fontFamily: '"Vazir", sans-serif'
                                },
                                cellStyle: {
                                    fontFamily: '"Vazir", sans-serif'
                                }
                            },

                        ]}
                        data={query => new Promise((resolve, reject) => {
                            this.getData(query, resolve, reject)
                        })
                        }
                        options={{
                            paging: true,
                            // maxBodyHeight: this.state.height/3,
                            toolbar: true,
                            actionsColumnIndex: -1,
                            showTitle: false,
                            search: false,
                            filtering: false,
                            headerStyle: {
                                fontFamily: '"Vazir", sans-serif'
                            },
                            doubleHorizontalScroll: true,
                            rowStyle: {
                                fontFamily: '"Vazir", sans-serif'
                            }
                        }}
                     
                        tableRef={table => this.tableRef = table}
                        editable={{
                            onRowUpdate: oldData =>
                                new Promise((resolve, reject) => {
                                    let url = `${process.env.BASE_URL}/protected/setting/${updateMethod}`

                                    oldData.hazardLevel = parseFloat(oldData.hazardLevel)

                                    this.doRuquest(resolve, reject, url, oldData)
                                })
                        }}
                        localization={gridLocalization}

                    />
                </CustomizedDialog>
            </div>

        )
    }
}



WebSettings.propTypes = {
    classes: PropTypes.object.isRequired,
    theme: PropTypes.object.isRequired,
};
export default withStyles(styles, { withTheme: true })(withLocalize(withSnackbar(WebSettings)));


