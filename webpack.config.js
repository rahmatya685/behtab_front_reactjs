var webpaqck = require("webpack"); 
const Dotenv = require('dotenv-webpack');


function findPara(param){
    let result = '';
    process.argv.forEach((argv)=>{
        if(argv.indexOf('--' + param) === -1) return;
        result = argv.split('=')[1];
    });
    return  result;
}

const mode = findPara('mode');


module.exports = {
    plugins: [
        new Dotenv({
            path: `.env.${mode}`, // load this now instead of the ones in '.env'
          })
      ],
    resolve: {
        alias: {
            fs: 'pdfkit/js/virtual-fs.js'
        }
    },
    entry: "./src/index.js",
    output: {
        path: __dirname + "/public/assets",
        filename: "bundle.js",
        publicPath: "assets"
    },
    devServer: {
        inline: true,
        contentBase: "./public",
        port: 3003
    },
    //mode: 'development',
      mode: 'production',
    module: {
        rules: [ 
            {
                test: /\.js$/,
                exclude: /(node_modules)/,
                use: {
                    loader: "babel-loader",
                    options: {
                        presets: ['@babel/preset-env', {
                            "targets": {
                                "node": "current"
                            }
                        }],
                        presets: ["@babel/preset-env", "@babel/preset-react"]
                    }
                }
            },
            {
                test: /\.css$/,
                use: ["style-loader", "css-loader"]
            }, {
                test: /\.less$/,
                use: ['style-loader',
                    'css-loader',
                    'less-loader']
            },
            {
                test: /\.(png|jp(e*)g|svg)$/,
                use: [{
                    loader: 'url-loader',
                    options: { 
                        name: 'images/[hash]-[name].[ext]'
                    }
                }]
            }
            // ,
            // {
            //     test:/\.json$/,
            //     exclude:/(node-modules)/,
            //     use:{
            //         loader:"json-loader" 
            //     }
            // }
        ]
    } 
}
